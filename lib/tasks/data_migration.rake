namespace :data_migration do
  desc "Migration geofence data to geofence collection from geofence groups"
  task :geofence_data => :environment do
    geofence_groups = GeofenceGroup.all
    geofence_groups.each do |geofence_group|
      puts ">>>>> Updating geofences for #{geofence_group.name} <<<<<"
      geofence_collection_params = {
        name: geofence_group.name,
        description: geofence_group.description
      }
      geofence_collection = GeofenceCollection.find_or_create_by geofence_collection_params
      geofences           = Geofence.where(geofence_group_id: geofence_group.id)
      geofences.update_all(geofence_collection_id: geofence_collection.id)
      geofence_group.update_attribute(:geofence_collection_id, geofence_collection.id)
    end
  end
end
