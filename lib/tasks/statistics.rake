require 'statistics_data'

namespace :statistics do

  desc 'Run daily to update the statistics for the week'
  task create_or_update_weekly_data: :environment do
    week_number = Time.now.strftime('%W')
    statistics  = Statistic.find_by week_number: week_number

    client    = StatisticsData.new

    statistic_params = {}
    Statistic.allowed_attributes.each do |attribute|
      statistic_params["#{attribute}_count"] = client.get_panelist_count_by_group(group_type: attribute)
    end

    if statistics
      statistics.update_attributes(statistic_params)
    else
      statistic_params[:week_number] = week_number
      Statistic.create statistic_params
    end
  end
end
