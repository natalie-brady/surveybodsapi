namespace :quick_poll_stats do

  desc "Pull all necessary data for quick poll stats and persist them"
  task pull_data: :environment do
    puts ">>>>> Executing QuickPollStatsJob <<<<<"
    QuickPollStatsJob.perform_later
    puts ">>>>> Done. <<<<<"
  end
end
