require 'questback'

class StatisticsData

  def initialize
    @questback_client = Questback.new ''
  end

  def get_panelist_count_by_filter(filter_type:)
    @questback_client.get_list_by_filter(filters_list[filter_type.to_sym]).uniq.count
  end

  def get_panelist_count_by_group(group_type:)
    @questback_client.get_list_by_group(groups_list[group_type.to_sym]).uniq.count
  end

  private

  def filters_list
    {
      all_active: 1221,
      all_active_four_weeks: 1302,
      all_active_surveybods: 1250,
    }
  end

  def groups_list
    {
      weekly_registrations: 43925,
      total_registrations: 43974,
      weekly_active_panelists: 43926,
      total_active_panelists: 16837,
      total_actively_participating: 16631
    }
  end
end
