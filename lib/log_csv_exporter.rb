class LogCsvExporter

  def initialize(user_id:, type:, options: {})
    @user                   = User.find_by id: user_id
    @type                   = type
    @options                = options
    date_params
  end

  def generate_csv(delayed_job_id)
    delayed_job       = Delayed::Job.find_by(id: delayed_job_id)
    initial_filename  = @type.eql?('panelist') ? 'panelist_geofence_logs' : geofence_group_filename
    logs_to_generate  = @type.eql?('panelist') ? export_for_panelist : export_for_geofence_group
    date_filename     = date_params_present? ? "_#{@start_date.to_date.strftime('%F')}_to_#{@end_date.to_date.strftime('%F')}" :
                          "_#{Time.now.strftime('%F')}"
    filename          = initial_filename + date_filename
    filepath          = "#{Rails.root}/tmp/#{filename}"

    logs_to_generate.find_in_batches(batch_size: 100000).with_index do |logs, index|
      index_count = index + 1
      delayed_job.update_column(:status_text, "Exporting #{logs.count * (index_count)} of #{logs_to_generate.count}")

      file = filepath + "_part_#{(index_count)}.csv"
      CSV.open(file, 'w') do |csv|
        csv << headers
        logs.map{ |log| csv << log.send(:csv_values) }
      end

      file_to_upload  = File.open(file, 'rb+')
      file_md5        = Digest::MD5.hexdigest(file)
      @user.geofence_log_exports.where(file_md5: file_md5).first_or_initialize do |attrib|
        attrib.file_csv = file_to_upload
        attrib.save
      end
    end
  end

  def export_for_panelist
    panelist      = Panelist.find_by id: @options.delete('panelist_id')
    geofence_logs = panelist.geofence_logs.query_by_parameters(@options)
    geofence_logs = geofence_logs.by_date_range(@start_date.to_datetime.to_i, @end_date.to_datetime.to_i) if date_params_present?
    geofence_logs
  end

  def export_for_geofence_group
    geofence_logs   = GeofenceLog.query_by_parameters(@options)
    geofence_logs   = geofence_logs.by_date_range(@start_date.to_datetime.to_i, @end_date.to_datetime.to_i) if date_params_present?
    geofence_logs
  end

  private

  def geofence_group_filename
    @options['geofence_group_id'] ? "geofence_group_geofence_logs_#{@options['geofence_group_id']}" : "geofence_group_geofence_logs"
  end

  def date_params
    @start_date = @options['start_date'] ? @options.delete('start_date') : ''
    @end_date   = @options['end_date'] ? @options.delete('end_date') : ''
    return @start_date, @end_date
  end

  def date_params_present?
    !@start_date.blank? && !@end_date.blank?
  end

  def headers
    ['Panelist Email', 'GeofenceID', 'GeofenceName', 'GeofenceType', 'GeofenceGroupID', 'GeofenceGroupName', 'Action', 'Logged Time',
      'Location Timestamp', 'Location Accuracy', 'Location Latitude', 'Location Longitude', 'Device Type']
  end

end
