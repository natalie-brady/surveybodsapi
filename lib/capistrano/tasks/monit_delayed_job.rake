namespace :monit_delayed_job do

  desc "Stop the delayed_job process"
  task :stop do
    on roles(:app) do
      fetch(:delayed_job_queues).each do |queue|
        begin
          execute :sudo, "monit stop delayed_job_#{queue}"
        rescue
          puts ">>>>> No process for #{queue} <<<<<"
        end
      end
    end
  end

  desc "Start the delayed_job process"
  task :start do
    on roles(:app) do
      fetch(:delayed_job_queues).each do |queue|
        begin
          execute :sudo, "monit start delayed_job_#{queue}"
        rescue
          puts ">>>>> No process for #{queue} <<<<<"
        end
      end
    end
  end

  desc "Restart the delayed_job process"
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      fetch(:delayed_job_queues).each do |queue|
        begin
          execute :sudo, "monit restart delayed_job_#{queue}"
        rescue
          puts ">>>>> No process for #{queue} <<<<<"
        end
      end
    end
  end

end
