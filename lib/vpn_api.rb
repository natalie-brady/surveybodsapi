require 'httparty'

class VpnApi

  def initialize(panelist_id: nil, vpn_study_id: nil, type: nil)
    @host                       = Setting.first.vpn_hostname || 'http://localhost:8080'
    @panelist                   = Panelist.find_by id: panelist_id
    @vpn_study                  = VpnStudy.find_by id: vpn_study_id
    @vpn_study_credential_type  = type
  end

  def create_config
    vpn_api_extension = get_extension
    vpn_api_username  = get_panelist_username(status: 'pending')
    filename          = "#{vpn_api_username}.#{vpn_api_extension}"
    filepath          = "#{Rails.root}/tmp/#{filename}"

    config_params = {
      vpn_study_id: @vpn_study.id,
      username:     vpn_api_username
    }

    api_params  = sanitize_options(user: vpn_api_username)
    file        = File.new(filepath, 'wb+')
    file.write request(type: 'post', api_params: api_params)
    file.close

    config_file                       = File.open(filepath, 'rb+')
    config_params[get_config_type]    = config_file
    vpn_study_credential              = @panelist.vpn_study_credentials.create(config_params)

    create_android_ssl(vpn_study_credential) if @vpn_study_credential_type.eql?('android')

    vpn_study_credential
  end

  def delete_config
    api_params = sanitize_options(user: get_panelist_username(status: 'active'))
    request(type: 'delete', api_params: api_params )
  end

  def sync
    request(type: 'get')
  end

  def disconnect(username: nil)
    api_params = sanitize_options(user: username)
    request(type: 'get', api_params: api_params)
  end

  private

  def request(type:, api_params: nil)
    vpn_api_url = config_url(type: @vpn_study_credential_type)
    url_params  = "?#{api_params}" unless api_params.nil?

    HTTParty.send(type, "#{vpn_api_url}#{url_params}")
  end

  def config_url(type:)
    case type
    when 'ios'
      "#{@host}/5173fa55a3412a77d3cb5198c30c7df3-create-vpn-iphone.php"
    when 'android'
      "#{@host}/cbdbeeb63e9c6787254242900b64b09d-create-vpn-android.php"
    when 'delete'
      "#{@host}/54b2022f0548cab5fb9daecb626aca09-vpn-delete.php"
    when 'route_sync'
      "#{@host}/d41d8cd98f00b204e9800998ecf8427e-route-install.php"
    when 'route_disconnect'
      "#{@host}/d41d8cd98f00b204e9800998ecf8427e-disconnectvpn.php"
    when 'decrypt_host'
      "#{@host}/cad95ed46edf05398022d47e3f5567a9-ssldomain.php"
    end
  end

  def get_panelist_username(status:)
    vpn_study_credential = @panelist.vpn_study_credentials.send(status).where(vpn_study_id: @vpn_study.id).last
    vpn_study_credential.nil? ? SecureRandom.hex(4) : vpn_study_credential.username
  end

  def get_extension
    @vpn_study_credential_type.eql?('ios') ? 'mobileconfig' : 'ovpn'
  end

  def get_config_type
    "#{@vpn_study_credential_type}_config".to_sym
  end

  def create_android_ssl(vpn_study_credential)
    filepath  = "#{Rails.root}/tmp/#{vpn_study_credential.username}_ca.crt"
    file      = File.new(filepath, "wb+")
    file.write HTTParty.get("#{@host}/ca.crt")
    file.close

    crt_file = File.open(filepath, 'rb+')
    vpn_study_credential.update_attribute(:android_ssl_cert, crt_file)
    vpn_study_credential.reload
  end

  def sanitize_options(options = {})
    mapped_options = options.map{ |key, value| "#{key}=#{value}" }
    mapped_options.join("&")
  end
end
