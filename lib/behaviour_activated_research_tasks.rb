class BehaviourActivatedResearchTasks

  def initialize(options={})
    @bart_setting = BartSetting.find_by_id(options[:bart_setting_id])
    @start_time   = options[:start_date]
    @end_time     = options[:end_date]
    if @bart_setting
      @vpn_study    = @bart_setting.vpn_study
      @bart_actions = @bart_setting.bart_actions
    end
  end

  def process_behaviour_activated_research_task
    @bart_actions.each do |bart_action|
      matching_entries = keyword_query(bart_action.search_term)

      if matching_entries.any?
        matching_entries.each do |matching_entry|
          new_audit_log_params = audit_log_params(matching_entry.time_since_epoch, bart_action.search_term)

          panelist = get_panelist_by_username(matching_entry.username)
          if panelist
            panelist_excluded = panelist_is_excluded?(bart_action, panelist)
            new_audit_log_params[:panelist_id] = panelist.id
            new_audit_log_params[:access_log_email_sent] = false if panelist_excluded

            unless panelist_excluded
              send_survey_to_user(bart_action, panelist, matching_entry.id) if @end_time.to_datetime <= @bart_setting.end_activation_period.to_datetime

              new_audit_log_params[:access_log_email_sent]      = true
              new_audit_log_params[:access_log_email_timestamp] = Time.zone.now
            end

            matching_entry.bart_audit_logs.create new_audit_log_params
          end
        end
      end
    end
  end

  def send_survey_to_user(bart_action, panelist, access_log_id)
    bart_action_survey_url  = bart_action.survey_url
    bart_action_survey_url  += "&pcode=#{panelist.external_auth_panelist_code}&a=#{access_log_id}&b=#{panelist.id}"
    bart_action_survey_url  += "&#{bart_action.survey_parameters}" if bart_action.survey_parameters.present?

    BartActionMailer.send_survey_to_panelist(bart_setting_id: @bart_setting.id,
                                             panelist_email: panelist.email,
                                             survey_url: bart_action_survey_url).
                                             deliver_now unless @bart_setting.exclude_all?
  end

  private

  def get_panelist_by_username(username)
    Panelist.joins(:vpn_study_credentials).
      where("vpn_study_credentials.vpn_study_id" => @vpn_study.id).
      find_by("vpn_study_credentials.username" => username)
  end

  def panelist_is_excluded?(bart_action, panelist)
    panelist_exclusion = bart_action.bart_exclusions.find_by_panelist_id panelist.id
    if panelist_exclusion
      exclusion_timeout = panelist_exclusion.created_at + @bart_setting.exclusion_setting.minutes
      if Time.zone.now >= exclusion_timeout
        panelist_exclusion.destroy
        create_bart_action_exclusions(bart_action, panelist)
        return false
      else
        return true
      end
    else
      create_bart_action_exclusions(bart_action, panelist)
      return false
    end
  end

  def create_bart_action_exclusions(bart_action, panelist)
    bart_action.bart_exclusions.create(panelist: panelist)
  end

  def keyword_query(bart_search_term)
    #TODO make this query efficient so we only perform one for all search terms rather than one for each term.
    #Every query takes more than 10s!!
    time_string = "#{@start_time.to_i} AND #{@end_time.to_i}"
    matches = UsageStudyAccessLog.select("access_log.id, user.username, access_log.http_url, access_log.time_since_epoch").joins(:usage_study_user).where("LOWER(access_log.http_url) LIKE '%#{bart_search_term.downcase}%' AND (access_log.time_since_epoch BETWEEN #{time_string})")
    return matches
  end

  def audit_log_params(timestamp, search_term)
    {
      access_log_timestamp: timestamp,
      access_log_search_term: search_term
    }
  end

end
