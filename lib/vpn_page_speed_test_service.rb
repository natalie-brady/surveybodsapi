require 'curb'

class VpnPageSpeedTestService

  def initialize(error_count:)
    @vpn_page_monitoring  = VpnPageMonitoring.first
    @error_count          = error_count
    @vpn_request          = Curl::Easy.new(@vpn_page_monitoring.page_url) do |req|
                              req.interface       = @vpn_page_monitoring.network_interface
                              req.proxy_url       = "http://#{@vpn_page_monitoring.dns_host}:#{@vpn_page_monitoring.dns_port}"
                              req.ssl_verify_peer     = false
                              req.verbose             = true
                              req.connect_timeout_ms  = @vpn_page_monitoring.response_timeout_threshold
                            end
  end

  def perform_test
    @vpn_request.perform
    evaluate_response
  rescue
    @error_count += 1
    log_test_result
    send_email_or_create_delayed_job
  end

  private
  def evaluate_response
    if @vpn_request.body_str.include?(@vpn_page_monitoring.page_success_message)
      total_time_in_milliseconds = @vpn_request.total_time * 1000
      (total_time_in_milliseconds > @vpn_page_monitoring.response_timeout_threshold) ?
        (@error_count += 1) : (@error_count = 0)
    else
      @error_count += 1
    end
    log_test_result
    send_email_or_create_delayed_job
  end

  def send_email_or_create_delayed_job
    @error_count >= @vpn_page_monitoring.error_alert_count ?
      send_email_to_users : create_delayed_job_for_vpn_page_speed_test
  end

  def send_email_to_users
    @error_count = 0
    create_delayed_job_for_vpn_page_speed_test
    VpnPageMonitoringMailer.speed_test_performance_alert.deliver_now
  end

  def create_delayed_job_for_vpn_page_speed_test
    wait_time = @vpn_page_monitoring.frequency_of_calls / 1000
    VpnPageSpeedTestJob.set(wait: wait_time.seconds).perform_later(error_count: @error_count)
  end

  def log_test_result
    test_status         = (@error_count != 0) ? 'fail' : 'success'
    test_result_params  =  {
      start_time: Time.zone.now,
      test_status: test_status,
      ellapsed_time: @vpn_request.total_time * 1000
    }
    VpnPageMonitoringResult.create test_result_params
  end

end
