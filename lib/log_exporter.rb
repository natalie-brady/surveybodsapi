class LogExporter

  def initialize(user_id:, options: {})
    @user         = User.find_by id: user_id
    @export_type  = options.delete('export_type')
    @options      = options
    date_params
  end

  def generate_csv(delayed_job_id)
    delayed_job = Delayed::Job.find_by(id: delayed_job_id)
    filepath    = "#{Rails.root}/tmp/#{export_filename}"

    logs_to_generate.find_in_batches(batch_size: 100000).with_index do |logs, index|
      delayed_job.update_column(:status_text, "Exporting #{logs.count * (index + 1)} of #{logs_to_generate.count}")

      file = filepath + "_part_#{(index + 1)}.csv"

      CSV.open(file, 'w') do |csv|
        csv << csv_headers
        logs.map{ |log| csv << log.send(:csv_values) }
      end

      file_to_upload  = File.open(file, 'rb+')
      file_md5        = Digest::MD5.hexdigest(file)
      @user.log_exports.where(file_md5: file_md5).first_or_initialize do |attrib|
        attrib.file         = file_to_upload
        attrib.export_type  = @export_type
        attrib.save
      end
    end
  end

  def logs_to_generate
    query_logs =
      if date_params_present?
        export_model.query_by_parameters(@options, start_date: @start_date.to_datetime, end_date: @end_date.to_datetime)
      else
        export_model.query_by_parameters(@options)
      end
    query_logs
  end

  private
  def csv_headers
    export_model.send(:csv_attributes)
  end

  def export_filename
    initial_filename  = "#{@export_type}_export"
    date_filename     = date_params_present? ? "_#{@start_date.to_date.strftime('%F')}_to_#{@end_date.to_date.strftime('%F')}" :
                          "_#{Time.now.to_i}"
    initial_filename + user_filename + date_filename
  end

  def date_params
    @start_date = @options['start_date'] ? @options.delete('start_date') : ''
    @end_date   = @options['end_date'] ? @options.delete('end_date') : ''
    return @start_date, @end_date
  end

  def date_params_present?
    !@start_date.blank? && !@end_date.blank?
  end

  def export_model
    @export_type.camelize.constantize
  end

  def user_filename
    if @options['ip_client']
      user      = UsageStudyUser.find_by ip_client: @options['ip_client']
      panelist  = user.vpn_study_credential.panelist if user && user.vpn_study_credential
    elsif @options['panelist_id']
      panelist = Panelist.find_by id: @options['panelist_id']
    end
    userfilename = panelist ? "_#{panelist.email}" : "_no_panelist_record"
    userfilename = "" if @options['ip_client'].blank? && @options['panelist_id'].blank?
    userfilename
  end

end
