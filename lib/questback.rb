require 'httparty'

class Questback

  def initialize(email, username=nil, password=nil, host=nil)
    @email = email
    @host = host == nil ? Setting.first.qb_hostname : host
    @username = username == nil ? Setting.first.qb_api_username : username
    @password = password == nil ? Setting.first.qb_api_password : password
    @api_root = "#{@host}/service/index.php?handler=json"
  end

  def is_valid_login(password)
    params = {
      :account => @email,
      :password => password,
      :loginType => "login_type_email"
    }
    request("panel.panelists.isValidLogin", params)
  end

  def get_bonus_points
    if @get_bonus_points_cached == nil
      params = {
        :identifierType => "u_email",
        :identifierValue => @email
      }
      @get_bonus_points_cached = request("panel.panelists.getBonusPoints", params)
    end
    @get_bonus_points_cached
  end

  def modify_bonus_points(points, reason)
    params = {
      :identifierType => "u_email",
      :identifierValue => @email,
      :pointsValue => points,
      :pointsReason => reason
    }
    request("panel.panelists.modifyPoints", params)
  end

  def get_panelist
    if @get_panelist_cached == nil
      params = {
        :identifierType => "u_email",
        :identifierValue => @email
      }
      @get_panelist_cached = request("panel.panelists.getV1", params)
    end
    @get_panelist_cached
  end

  def get_panelist_masterdata(masterdata_key)
    panelist_data = get_panelist
    return nil if panelist_data.nil?
    panelist_data["masterDataValues"].each do |md|
      if md["key"] == masterdata_key
        return md["value"] == "-77" ? "0" : md["value"]
      end
    end
    nil
  end


  # TODO Needs ID to get lottery by :(
  def get_lottery(lottery_id)
    params = {
      :id => lottery_id
    }
    request("panel.lotteries.get", params)
  end

  def get_surveys
    params = {
      :identifierType => "u_email",
      :identifierValue => @email
    }
    request("panel.surveys.getList", params)
  end

  def get_survey(survey_id)
    params = { 
      :surveyId => survey_id
    }
    request("survey.surveys.get", params)
  end

  def get_completed_surveys
    params = {
      :identifierType => "u_email",
      :identifierValue => @email
    }
    request("panel.surveys.getListCompleted", params)
  end

  def get_all_active_surveys
    request("survey.surveys.getActiveList", {})
  end

  def get_all_surveys
    request("survey.surveys.getList", {})
  end

  def get_masterdata_descriptions
    request("panel.masterdata.getDescriptionList", nil)
  end

  def change_panelist(attributes)
    targets = ["u_firstname", "u_name", "u_email", "u_zip"]
    masterdata_targets = ["m_region", "m_county", "md_tickets", "md_app_login"]

    params = {
      :identifierType => "u_email",
      :identifierValue => @email,
    }
    panelist_data = {}
    panelist_master_data = []

    attributes.each do |attribute|
      if targets.index(attribute[0]) != nil
        panelist_data[attribute[0]] = attribute[1]
      elsif masterdata_targets.index(attribute[0]) != nil
        panelist_master_data << {"key" => attribute[0], "value" => attribute[1]}
      end
    end

    if panelist_master_data.size > 0
      panelist_data["masterDataValues"] = panelist_master_data
    end
    params[:panelistData] = panelist_data

    request("panel.panelists.changeV1", params)
  end

  def register_panelist(attributes, site_id)
    pci                           = Setting.first.pci
    targets                       = ["u_firstname", "u_name", "u_email", "u_passwd"]
    masterdata_targets            = ["m_0001", "m_0002", "m_0003"]
    params                        = {}
    params[:returnIdentifierType] = "u_email"

    # Check all required attributes are present
    (targets + masterdata_targets).each do |target|
      if !attributes.has_key? target
        return nil
      end
    end

    panelist_data = {}
    panelist_master_data = []
    attributes.each do |attribute|
      if targets.index(attribute[0]) != nil
        panelist_data[attribute[0]] = attribute[1]
      elsif masterdata_targets.index(attribute[0]) != nil
        panelist_master_data << {"key" => attribute[0], "value" => attribute[1]}
      end
    end

    panelist_data["masterDataValues"] = panelist_master_data
    panelist_data["site_id"]          = site_id
    panelist_data["pci"]              = pci
    params[:panelistData]             = panelist_data

    request("panel.panelists.addV1", params, true)
  end

  def get_mail_templates(types = [])
      types = ['default_mail', 'invitation_mail', 'reminder_mail', 'survey_ends_mailtype', 'welcome_mail', 'pwrecovery_mail', 'doub_check_mail', 'register_check_mail', 'md_invitation_mail', 'md_reminder_mail', 'winner_mail', 'promotion_mail', 'bonus_mail', 'exchange_mail'] if types.empty?
      params = {
        :mailTemplateTypes => types
      }
      request("efs.mailtemplates.getList", params)
  end

  def tell_a_friend(promotion_id: nil, email: nil, panelist_id: nil)
    params = {
      promotionId: promotion_id,
      recipientEmail: email,
      panelistUid: panelist_id,
      header: 'This is a header',
      footer: 'This is a footer'
    }

    request_response = request("panel.promotions.tellafriend", params)
    request_response
  end

  def get_survey_samples(survey_id)
    params = {
      :surveyId => survey_id
    }
    request("survey.samples.getList", params)
  end

  def get_sample_panelists(sample_id)
    params = {
      :sampleId => sample_id,
      :returnDataFields => ["uid"]
    }
    request("panel.panelists.getListBySample", params)
  end

  def get_samples
    request("survey.samples.getAll", nil)
  end

  def create_external_auth(auth_code)
    params = {
      :externalServiceId => Setting.first.questback_external_service_id,
      :externalAuthenticationValue => auth_code,
      :identifierType => "u_email",
      :identifierValue => @email
    }
    request("panel.panelists.saveExternalAuthentication", params, true)
  end

  def get_external_auth_pcode(auth_code)
    params = {
      :externalServiceId => Setting.first.questback_external_service_id,
      :externalAuthenticationValue => auth_code,
      :identifierReturnType => "panelist_code"
    }
    request("panel.panelists.getExternalAuthentication", params, true)
  end

  def remove_external_auth(auth_code)
    params = {
      :externalServiceId => Setting.first.questback_external_service_id,
      :externalAuthenticationValue => auth_code
    }
    request("panel.panelists.deleteExternalAuthentication", params, true)
  end

  def get_groups
    request("panel.groups.getList", {})
  end

  def get_group_panelist_uids(group_id)
    params = {
      :id => group_id,
      :returnIdentifierType => "uid"
    }
    request("panel.groups.getGroupMembers", params)
  end

  def get_list_by_filter(filter_id)
    params = {
      filterId: filter_id,
      returnDataFields: ['uid']
    }
    request('panel.panelists.getListByFilter', params)
  end

  def get_list_by_group(filter_id)
    params = {
      groupId: filter_id,
      returnDataFields: ['uid']
    }
    request('panel.panelists.getListByGroup', params)
  end

  def request(method, params={}, report_error=false)
    Rails::logger.debug(">>> REQUEST SENT: #{method}")
    Rails::logger.debug(">>> REQUEST PARAMS: #{params}")
    request_data = {
      :id => "",
      :method => method,
      :jsonrpc => "2.0",
      :params => params
    }

    raw_response = HTTParty.post(
      @api_root,
      :body => request_data.to_json,
      :basic_auth => { :username => @username, :password => @password }
    )
    # Rails::logger.debug(">>> RAW_RESPONSE: #{raw_response}")
    # TODO: Place a catcher that will filter error responses from QB
    if raw_response.has_key?("error")
      Rails::logger.debug(">>> ERROR: #{raw_response['error']['message']}")
      if report_error
        return {"error_message" => raw_response['error']['message']}
      else
        return nil
      end
    end

    response = raw_response["result"]["return"]
    Rails::logger.debug(">>> RESPONSE RECEIVED: #{method}")
    # Rails::logger.debug(">>> RESPONSE: #{response}")
    response
  end

end
