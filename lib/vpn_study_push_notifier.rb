class VpnStudyPushNotifier
  attr_accessor :vpn_study

  def initialize(vpn_study_id:, action_type:)
    @vpn_study                = VpnStudy.find_by id: vpn_study_id
    @action_type              = action_type
    @devices_notifier_client  = DevicesNotifier.new device_notifier_params if @vpn_study
  end

  def complete_active_vpn_study_credentials
    active_vpn_study_credentials.each do |vpn_study_credential|
      begin
        vpn_study_credential.complete!
      rescue
        vpn_study_credential.update_attribute(:status, :complete)
      end
    end
  end

  def send_notification_to_devices
    @devices_notifier_client.notify_apple_devices
    @devices_notifier_client.notify_android_devices
  end

  private

  def active_vpn_study_credentials
    @vpn_study.vpn_study_credentials.active
  end

  def vpn_study_panelists
    case @action_type
    when 'connect'    then @vpn_study.send(:panelists_without_active_or_completed_or_cancelled_record)
    when 'disconnect' then all_vpn_study_panelists
    end
  end

  def all_vpn_study_panelists
    active_vpn_study_credentials.map{ |vpn_study_credential| vpn_study_credential.panelist }.flatten
  end

  def notification_message
    case @action_type
    when 'connect'    then 'New usage study'
    when 'disconnect' then "#{@vpn_study.name} has ended. The connection will be terminated automatically."
    end
  end

  def device_notifier_params
    {
      panelists: vpn_study_panelists,
      type: 'vpn_study',
      type_id: @vpn_study.id,
      custom_message: notification_message,
      action: @action_type
    }
  end
end
