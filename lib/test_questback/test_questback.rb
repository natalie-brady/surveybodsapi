#!/usr/bin/env ruby
require './lib/questback'

module Rails
  class Logger
    @@quiet = false
    def info(message)
      if !@@quiet
        puts message
      end
    end
    def debug(message)
      if !@@quiet
        puts message
      end
    end
  end
  def self.logger
    return Logger.new
  end
end


class MockSetting
  def questback_external_service_id
    return 12345
  end
end

class MockSettingResource
  def first
    MockSetting.new
  end
end

Setting = MockSettingResource.new

# qb = ::Questback.new("user@example.com", "user", "password", "http://surveybods.com")
qb = ::Questback.new("mike@terzza.com", "terzza", "sa9RG3M9TilRryn5amm1Yj", "http://surveybods.com")

# puts qb.tell_a_friend()

# puts qb.get_panelist().to_yaml
# puts qb.change_panelist({"m_region" => "5"})

# puts qb.change_panelist({"u_zip" => "QW123ER"})
# puts qb.change_panelist({"site_id" => 13})
# puts qb.is_valid_login("password")
# puts qb.get_bonus_points("mike@terzza.com")
# puts qb.get_panelist.to_yaml

# response = qb.get_all_active_surveys
# puts response.to_yaml
# puts response.size

# puts qb.get_lottery(32964)

# puts qb.get_surveys.to_yaml
# puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
# puts qb.get_panelist_completed_surveys("mike@terzza.com")



# samples = qb.get_survey_samples(35607)
# puts "Samples: #{samples.size}"
# all_panelist = []
# samples.each do |sample|
#   panelists = qb.get_sample_panelists(sample["sampleId"])
#   puts "Sample: #{sample['sampleId']} | #{panelists.size}"
#   cleaned_panelists = panelists.map {|p| p[0]}
#   puts cleaned_panelists.inspect
#   all_panelist += cleaned_panelists
#   puts "AllPanelists: #{all_panelist.uniq.size}"
# end

# puts qb.get_sample_panelists("35608").map {|p| p[0]}

puts qb.test