class AddEmailSubjectToBartSetting < ActiveRecord::Migration
  def change
    add_column :bart_settings, :email_subject, :string
  end
end
