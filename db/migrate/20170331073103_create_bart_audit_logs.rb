class CreateBartAuditLogs < ActiveRecord::Migration
  def change
    create_table :bart_audit_logs do |t|
      t.belongs_to  :panelist
      t.belongs_to  :usage_study_access_log
      t.string      :access_log_timestamp
      t.string      :access_log_search_term
      t.boolean     :access_log_email_sent
      t.datetime    :access_log_email_timestamp

      t.timestamps null: false
    end
    add_index :bart_audit_logs, :usage_study_access_log_id
    add_index :bart_audit_logs, :panelist_id
  end
end
