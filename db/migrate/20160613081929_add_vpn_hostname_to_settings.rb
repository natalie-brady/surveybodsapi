class AddVpnHostnameToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :vpn_hostname, :string
  end
end
