class AddDwellTimeToGeofences < ActiveRecord::Migration
  def change
    add_column :geofences, :dwell_time, :integer
  end
end
