class CreateSurveyCategories < ActiveRecord::Migration
  def change
    create_table :survey_categories do |t|
      t.string :name
      t.text :description
      t.string :color

      t.timestamps null: false
    end
  end
end
