class RemoveActionToGeofencesAndMoveGeofencesGroup < ActiveRecord::Migration
  def change
    remove_column :geofences, :action

    add_column :geofence_groups, :action, :integer
  end
end
