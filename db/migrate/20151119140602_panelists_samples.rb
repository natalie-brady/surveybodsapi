class PanelistsSamples < ActiveRecord::Migration
  def change
    create_table :panelists_samples, id: false do |t|
      t.belongs_to :panelist, index: true
      t.belongs_to :sample, index: true
    end
  end
end
