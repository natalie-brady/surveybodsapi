class AddFileColumnToGeofenceCollection < ActiveRecord::Migration
  def up
    add_attachment :geofence_collections, :file
  end

  def down
    remove_attachment :geofence_collections, :file
  end
end
