class CreateMailTemplates < ActiveRecord::Migration
  def change
    create_table :mail_templates do |t|
      t.string :description
      t.integer :template_id

      t.timestamps null: false
    end
  end
end
