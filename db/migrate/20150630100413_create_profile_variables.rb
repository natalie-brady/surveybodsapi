class CreateProfileVariables < ActiveRecord::Migration
  def change
    create_table :profile_variables do |t|
      t.string :name
      t.text :description
      t.string :value

      t.timestamps null: false
    end
  end
end
