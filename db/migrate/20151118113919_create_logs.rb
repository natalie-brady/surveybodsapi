class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.belongs_to :panelist, index: true
      t.text :data

      t.timestamps null: false
    end
    add_foreign_key :logs, :panelists
  end
end
