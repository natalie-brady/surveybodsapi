class ChangeIosMinimumVersionColumnFromSetting < ActiveRecord::Migration
  def change
    change_column :settings, :ios_minimum_version, :string
  end
end
