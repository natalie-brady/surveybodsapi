class AddIpclientIndexToUsageStudyUser < ActiveRecord::Migration
  def change
    add_index :vpn_study_credentials, :username
    add_index 'ecsc_staging.user', :username
    add_index 'ecsc_staging.access_log', :ip_client
  end
end
