class CreatePanelistSyncLogs < ActiveRecord::Migration
  def change
    create_table :panelist_sync_logs do |t|
      t.belongs_to  :user, index: true
      t.datetime    :started_date
      t.datetime    :ended_date

      t.timestamps null: false
    end
  end
end
