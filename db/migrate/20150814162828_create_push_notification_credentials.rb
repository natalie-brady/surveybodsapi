class CreatePushNotificationCredentials < ActiveRecord::Migration
  def change
    create_table :push_notification_credentials do |t|
      t.string :token
      t.integer :notification_service
      t.belongs_to :panelist, index: true

      t.timestamps null: false
    end
    add_foreign_key :push_notification_credentials, :panelists
  end
end
