class AddRegistrationSurveyToSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :registration_survey, :boolean, default: false
  end
end
