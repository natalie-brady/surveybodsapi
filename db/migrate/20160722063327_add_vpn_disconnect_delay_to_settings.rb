class AddVpnDisconnectDelayToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :vpn_study_disconnect_delay, :integer, default: 3
  end
end
