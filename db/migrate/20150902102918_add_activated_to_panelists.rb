class AddActivatedToPanelists < ActiveRecord::Migration
  def change
    add_column :panelists, :activated, :boolean, :default => true
  end
end
