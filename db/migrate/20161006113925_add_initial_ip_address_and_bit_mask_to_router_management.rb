class AddInitialIpAddressAndBitMaskToRouterManagement < ActiveRecord::Migration
  def change
    add_column :router_managements, :initial_ip_address, :string
    add_column :router_managements, :bit_mask, :string
  end
end
