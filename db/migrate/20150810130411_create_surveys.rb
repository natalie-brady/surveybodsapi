class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.integer :survey_id
      t.string :title
      t.datetime :created_time
      t.datetime :start_time
      t.datetime :end_time
      t.integer :bonus_points
      t.belongs_to :survey_category, index: true
      t.string :duration

      t.timestamps null: false
    end
    add_foreign_key :surveys, :survey_categories
  end
end
