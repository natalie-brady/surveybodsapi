class CreateBartExclusions < ActiveRecord::Migration
  def change
    create_table :bart_exclusions do |t|
      t.belongs_to :bart_action
      t.belongs_to :panelist

      t.timestamps null: false
    end
  end
end
