class AddDeviceTypeToGeofenceLogs < ActiveRecord::Migration
  def change
    add_column :geofence_logs, :device_type, :string
  end
end
