class RemoveColumnMaxDurationFromVpnStudies < ActiveRecord::Migration
  def change
    remove_column :vpn_studies, :max_duration
  end
end
