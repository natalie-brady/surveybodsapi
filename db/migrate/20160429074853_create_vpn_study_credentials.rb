class CreateVpnStudyCredentials < ActiveRecord::Migration
  def change
    create_table :vpn_study_credentials do |t|
      t.belongs_to :panelist, index: true
      t.attachment :ios_config
      t.attachment :android_config
      t.attachment :android_ssl_cert

      t.timestamps null: false
    end
  end
end
