class AddGeofenceToGeofenceGroup < ActiveRecord::Migration
  def change
    add_column :geofences, :geofence_group_id, :integer, index: true
  end
end
