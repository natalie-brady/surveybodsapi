class AddVpnStudyIdToVpnStudyCredentials < ActiveRecord::Migration
  def change
    add_column :vpn_study_credentials, :vpn_study_id, :integer
  end
end
