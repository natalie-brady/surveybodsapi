class AddWeeklyRegistrationsCountAndWeeklyActivesCountToStatistics < ActiveRecord::Migration
  def change
    add_column :statistics, :weekly_registrations_count, :integer, default: 0
    add_column :statistics, :weekly_actives_count, :integer, default: 0
  end
end
