class AddDefaultValueToPciForSettings < ActiveRecord::Migration
  def self.up
    change_column :settings, :pci, :integer, default: 1
  end

  def self.down
    change_column :settings, :pci, :integer, default: nil
  end
end
