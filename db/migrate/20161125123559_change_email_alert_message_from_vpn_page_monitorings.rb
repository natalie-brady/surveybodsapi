class ChangeEmailAlertMessageFromVpnPageMonitorings < ActiveRecord::Migration
  def change
    rename_column :vpn_page_monitorings, :email_alert_message, :email_alert_addresses
  end
end
