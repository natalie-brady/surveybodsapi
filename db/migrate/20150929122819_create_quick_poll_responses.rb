class CreateQuickPollResponses < ActiveRecord::Migration
  def change
    create_table :quick_poll_responses do |t|
      t.integer :panelist_id
      t.integer :choice_id

      t.timestamps null: false
    end
    add_index :quick_poll_responses, :panelist_id
    add_index :quick_poll_responses, :choice_id
  end
end
