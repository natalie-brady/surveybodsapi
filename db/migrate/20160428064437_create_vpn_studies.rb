class CreateVpnStudies < ActiveRecord::Migration
  def change
    create_table :vpn_studies do |t|
      t.belongs_to  :questback_group, index: true
      t.string      :name
      t.text        :description
      t.string      :max_duration
      t.datetime    :end_time
      t.string      :bonus_points,    default: 0
      t.string      :tickets,         default: 0

      t.timestamps null: false
    end
  end
end
