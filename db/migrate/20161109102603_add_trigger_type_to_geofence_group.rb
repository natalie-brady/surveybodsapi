class AddTriggerTypeToGeofenceGroup < ActiveRecord::Migration
  def change
    add_column :geofence_groups, :trigger_type, :integer, default: 0
  end
end
