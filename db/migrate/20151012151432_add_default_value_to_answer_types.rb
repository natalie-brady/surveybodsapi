class AddDefaultValueToAnswerTypes < ActiveRecord::Migration
  def change
    change_column :choices, :answer_type, :integer, default: 0
  end
end
