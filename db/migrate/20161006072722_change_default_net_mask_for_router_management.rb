class ChangeDefaultNetMaskForRouterManagement < ActiveRecord::Migration
  def up
    change_column :router_managements, :net_mask, :string, default: nil
  end

  def down
    change_column :router_managements, :net_mask, :string, default: "255.255.255.255"
  end
end
