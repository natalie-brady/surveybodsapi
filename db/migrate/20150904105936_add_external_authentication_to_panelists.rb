class AddExternalAuthenticationToPanelists < ActiveRecord::Migration
  def change
    add_column :panelists, :external_auth_code, :string
    add_column :panelists, :external_auth_panelist_code, :string
  end
end
