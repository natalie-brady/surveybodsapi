class AddAnchoredToChoices < ActiveRecord::Migration
  def change
    add_column :choices, :anchored, :boolean, :default => false
  end
end
