class AddAttributesToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :site_id, :integer
    add_column :settings, :mail_template_id, :integer
  end
end
