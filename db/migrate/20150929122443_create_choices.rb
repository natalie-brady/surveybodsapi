class CreateChoices < ActiveRecord::Migration
  def change
    create_table :choices do |t|
      t.string :answer
      t.integer :quick_poll_question_id

      t.timestamps null: false
    end
    add_index :choices, :quick_poll_question_id
  end
end
