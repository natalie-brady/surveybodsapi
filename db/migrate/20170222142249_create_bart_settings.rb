class CreateBartSettings < ActiveRecord::Migration
  def change
    create_table :bart_settings do |t|
      t.belongs_to  :vpn_study, index: true
      t.datetime    :start_activation_period
      t.datetime    :end_activation_period
      t.integer     :frequency_checker
      t.integer     :exclusion_setting
      t.boolean     :exclude_all
      t.attachment  :action_csv_file

      t.timestamps null: false
    end
  end
end
