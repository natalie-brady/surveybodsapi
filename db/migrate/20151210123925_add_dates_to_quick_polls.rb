class AddDatesToQuickPolls < ActiveRecord::Migration
  def change
    add_column :quick_polls, :start_time, :datetime
    add_column :quick_polls, :end_time, :datetime
  end
end
