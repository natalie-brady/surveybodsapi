class AddApnCertToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :apn_cert, :text
    add_column :settings, :apn_passphrase, :string
  end
end
