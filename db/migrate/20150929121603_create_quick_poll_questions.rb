class CreateQuickPollQuestions < ActiveRecord::Migration
  def change
    create_table :quick_poll_questions do |t|
      t.string :type
      t.string :question
      t.integer :quick_poll_id

      t.timestamps null: false
    end
    add_index :quick_poll_questions, :quick_poll_id
  end
end
