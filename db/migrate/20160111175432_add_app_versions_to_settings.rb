class AddAppVersionsToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :android_minimum_version, :integer, :default => 0
    add_column :settings, :ios_minimum_version, :integer, :default => 0
  end
end
