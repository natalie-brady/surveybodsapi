class CreateVpnPageMonitoringResults < ActiveRecord::Migration
  def change
    create_table :vpn_page_monitoring_results do |t|
      t.datetime  :start_time
      t.integer   :test_status
      t.string   :ellapsed_time

      t.timestamps null: false
    end
  end
end
