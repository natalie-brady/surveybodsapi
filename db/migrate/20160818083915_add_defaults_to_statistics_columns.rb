class AddDefaultsToStatisticsColumns < ActiveRecord::Migration
  def change
    change_column :statistics, :signups_count, :integer, default: 0
    change_column :statistics, :active_panelists_count, :integer, default: 0
    change_column :statistics, :response_rate_count, :integer, default: 0
  end
end
