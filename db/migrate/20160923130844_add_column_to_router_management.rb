class AddColumnToRouterManagement < ActiveRecord::Migration
  def change
    add_column :router_managements, :net_mask, :string, default: "255.255.255.255"
    add_reference :router_managements, :vpnroute, index: true
  end
end
