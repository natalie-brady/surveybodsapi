class CreateRouterManagements < ActiveRecord::Migration
  def change
    create_table :router_managements do |t|
      t.string  :name
      t.text    :description
      t.string  :ip_address_range
      t.integer :status

      t.timestamps null: false
    end
  end
end
