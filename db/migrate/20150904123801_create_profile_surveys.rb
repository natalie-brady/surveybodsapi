class CreateProfileSurveys < ActiveRecord::Migration
  def change
    create_table :profile_surveys do |t|
      t.string :name
      t.belongs_to :survey, index: true

      t.timestamps null: false
    end
    add_foreign_key :profile_surveys, :surveys
  end
end
