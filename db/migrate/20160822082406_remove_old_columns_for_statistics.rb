class RemoveOldColumnsForStatistics < ActiveRecord::Migration
  def change
    remove_column :statistics, :signups_count
    remove_column :statistics, :active_panelists_count
    remove_column :statistics, :response_rate_count
    remove_column :statistics, :weekly_registrations_count
    remove_column :statistics, :weekly_actives_count
  end
end
