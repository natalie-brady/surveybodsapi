class RemoveSurveyQuickPollAndMessageToGeofenceAndMoveToGeofenceGroup < ActiveRecord::Migration
  def change
    remove_foreign_key  :geofences, :survey
    remove_foreign_key  :geofences, :quick_poll
    remove_reference    :geofences, :survey, index: true
    remove_reference    :geofences, :quick_poll, index: true
    remove_column       :geofences, :message

    add_reference :geofence_groups, :survey, index: true, foreign_key: true
    add_reference :geofence_groups, :quick_poll, index: true, foreign_key: true
    add_column    :geofence_groups, :message, :string
  end
end
