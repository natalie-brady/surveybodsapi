class CreateGeofenceLogs < ActiveRecord::Migration
  def change
    create_table :geofence_logs do |t|
      t.belongs_to :panelist, index: true
      t.belongs_to :geofence, index: true
      t.string     :logged_time

      t.timestamps null: false
    end
  end
end
