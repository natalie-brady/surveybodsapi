class AddColumnLastStartTimeOnVpnStudies < ActiveRecord::Migration
  def change
    add_column :vpn_studies, :last_start_time, :datetime
  end
end
