class CreatePanelists < ActiveRecord::Migration
  def change
    create_table :panelists do |t|
      t.string :access_token
      t.string :email
      t.datetime :last_accessed
      t.integer :uid
      t.string :password_digest

      t.timestamps null: false
    end
  end
end
