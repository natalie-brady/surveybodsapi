class MoveRandomizeOrderToQuickPollQuestion < ActiveRecord::Migration
  def change
    remove_column :quick_polls, :randomize_order, :boolean
    add_column :quick_poll_questions, :randomize_order, :boolean, :default => false
  end
end
