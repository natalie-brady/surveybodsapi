class CreateBartActions < ActiveRecord::Migration
  def change
    create_table :bart_actions do |t|
      t.belongs_to  :bart_setting
      t.text        :search_term
      t.string      :survey_url
      t.string      :survey_parameters

      t.timestamps null: false
    end
  end
end
