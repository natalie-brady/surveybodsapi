class ChangeStatusColumnDefaultForVpnStudyCredentials < ActiveRecord::Migration
  def change
    change_column :vpn_study_credentials, :status, :integer, default: 'pending'
  end
end
