class AddGcmKeyToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :gcm_key, :string
  end
end
