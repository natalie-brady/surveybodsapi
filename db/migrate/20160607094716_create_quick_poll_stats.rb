class CreateQuickPollStats < ActiveRecord::Migration
  def change
    create_table :quick_poll_stats do |t|
      t.belongs_to  :quick_poll, index: true
      t.integer     :completed_panelists_count
      t.integer     :questback_group_panelists_count
      t.boolean     :responded_to

      t.timestamps null: false
    end

    add_foreign_key :quick_poll_stats, :quick_polls
  end
end
