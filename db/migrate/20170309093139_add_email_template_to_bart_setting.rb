class AddEmailTemplateToBartSetting < ActiveRecord::Migration
  def change
    change_table :bart_settings do |t|
      t.attachment :email_template
    end
  end
end
