class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :qb_hostname
      t.string :qb_api_username
      t.string :qb_api_password
      t.integer :auth_timeout_seconds

      t.timestamps null: false
    end
  end
end
