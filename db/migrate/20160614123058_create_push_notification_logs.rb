class CreatePushNotificationLogs < ActiveRecord::Migration
  def change
    create_table :push_notification_logs do |t|
      t.belongs_to  :user
      t.integer     :survey_id
      t.integer     :quick_poll_id
      t.integer     :questback_group_id
      t.text        :sample_ids

      t.timestamps null: false
    end
  end
end
