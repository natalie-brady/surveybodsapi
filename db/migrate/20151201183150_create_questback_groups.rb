class CreateQuestbackGroups < ActiveRecord::Migration
  def change
    create_table :questback_groups do |t|
      t.integer :group_id
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
