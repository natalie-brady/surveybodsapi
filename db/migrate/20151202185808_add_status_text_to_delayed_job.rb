class AddStatusTextToDelayedJob < ActiveRecord::Migration
  def change
    add_column :delayed_jobs, :status_text, :text
  end
end
