class ChangeColumnTypeOfMaxDurationFromVpnStudy < ActiveRecord::Migration
  def change
    change_column :vpn_studies, :max_duration, :integer
  end
end
