class CreateDeviceMetadata < ActiveRecord::Migration
  def change
    create_table :device_metadata do |t|
      t.belongs_to  :panelist, index: true
      t.string      :network_provider
      t.string      :device_model
      t.string      :device_manufacturer
      t.text        :more_info

      t.timestamps null: false
    end
  end
end
