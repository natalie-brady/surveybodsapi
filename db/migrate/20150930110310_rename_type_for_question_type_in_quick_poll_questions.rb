class RenameTypeForQuestionTypeInQuickPollQuestions < ActiveRecord::Migration
  def change
     rename_column :quick_poll_questions, :type, :question_type
  end
end
