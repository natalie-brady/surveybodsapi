class AddAvatarToPanelist < ActiveRecord::Migration
  def up
    add_attachment :panelists, :avatar
  end

  def down
    remove_attachment :panelists, :avatar
  end
end
