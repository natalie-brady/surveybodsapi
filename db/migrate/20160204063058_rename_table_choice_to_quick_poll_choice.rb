class RenameTableChoiceToQuickPollChoice < ActiveRecord::Migration
  def change
    remove_index :quick_poll_responses, :choice_id
    remove_index :choices, :quick_poll_question_id

    rename_table :choices, :quick_poll_choices

    rename_column :quick_poll_responses, :choice_id, :quick_poll_choice_id
    rename_column :quick_poll_responses, :choice_text, :quick_poll_choice_text

    add_index :quick_poll_responses, :quick_poll_choice_id
    add_index :quick_poll_choices, :quick_poll_question_id
  end
end
