class AddTicketsToQuickPolls < ActiveRecord::Migration
  def change
    add_column :quick_polls, :tickets, :integer, default: 0
  end
end
