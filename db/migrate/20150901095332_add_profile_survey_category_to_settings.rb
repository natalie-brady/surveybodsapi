class AddProfileSurveyCategoryToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :survey_category_id, :integer, index: true
  end
end
