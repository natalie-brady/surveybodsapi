class CreateLogExports < ActiveRecord::Migration
  def change
    create_table :log_exports do |t|
      t.belongs_to  :user, index: true
      t.attachment  :file
      t.string      :file_md5
      t.string      :export_type

      t.timestamps null: false
    end
  end
end
