class RemoveGeofenceTypeAndDwellTimeFromGeofencesAndAddToGeofenceGroup < ActiveRecord::Migration
  def change
    remove_column :geofences, :geofence_type
    remove_column :geofences, :dwell_time

    add_column :geofence_groups, :geofence_type, :integer
    add_column :geofence_groups, :dwell_time, :integer
  end
end
