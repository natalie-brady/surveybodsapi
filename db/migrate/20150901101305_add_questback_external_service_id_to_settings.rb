class AddQuestbackExternalServiceIdToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :questback_external_service_id, :integer
  end
end
