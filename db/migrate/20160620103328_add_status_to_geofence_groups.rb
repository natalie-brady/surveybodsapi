class AddStatusToGeofenceGroups < ActiveRecord::Migration
  def change
    add_column :geofence_groups, :status, :integer, default: 1
  end
end
