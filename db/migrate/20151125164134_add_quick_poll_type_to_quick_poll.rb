class AddQuickPollTypeToQuickPoll < ActiveRecord::Migration
  def change
    add_column :quick_polls, :quick_poll_type, :integer, :default => 0
  end
end
