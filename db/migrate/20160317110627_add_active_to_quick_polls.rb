class AddActiveToQuickPolls < ActiveRecord::Migration
  def change
    add_column :quick_polls, :active, :boolean, default: false
  end
end
