class AddLocationTimestampLocationAccuracyLocationLatitudeAndLocationLongitudeToGeofenceLogs < ActiveRecord::Migration
  def change
    add_column :geofence_logs, :location_timestamp ,:string
    add_column :geofence_logs, :location_accuracy ,:string
    add_column :geofence_logs, :location_latitude ,:string
    add_column :geofence_logs, :location_longitude ,:string
  end
end
