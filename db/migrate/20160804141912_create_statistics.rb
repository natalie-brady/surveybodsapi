class CreateStatistics < ActiveRecord::Migration
  def change
    create_table :statistics do |t|
      t.integer :week_number
      t.integer :signups_count
      t.integer :active_panelists_count
      t.integer :response_rate_count

      t.timestamps null: false
    end
  end
end
