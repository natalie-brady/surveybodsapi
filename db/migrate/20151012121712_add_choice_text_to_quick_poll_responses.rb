class AddChoiceTextToQuickPollResponses < ActiveRecord::Migration
  def change
    add_column :quick_poll_responses, :choice_text, :string
  end

  def down
    remove_column :quick_poll_responses, :choice_text
  end

end
