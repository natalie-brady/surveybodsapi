class CreateSamples < ActiveRecord::Migration
  def change
    create_table :samples do |t|
      t.integer :sample_id
      t.string :title
      t.string :description
      t.integer :survey_id

      t.timestamps null: false
    end
  end
end
