class CreateQuickPolls < ActiveRecord::Migration
  def change
    create_table :quick_polls do |t|
      t.string :title
      t.text :description
      t.string :duration
      t.integer :bonus_points

      t.timestamps null: false
    end
  end
end
