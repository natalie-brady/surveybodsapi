class AddPciColumnToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :pci, :integer
  end
end
