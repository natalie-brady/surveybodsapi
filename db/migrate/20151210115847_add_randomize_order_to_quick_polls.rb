class AddRandomizeOrderToQuickPolls < ActiveRecord::Migration
  def change
    add_column :quick_polls, :randomize_order, :boolean
  end
end
