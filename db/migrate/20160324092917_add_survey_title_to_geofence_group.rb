class AddSurveyTitleToGeofenceGroup < ActiveRecord::Migration
  def change
    add_column :geofence_groups, :survey_title, :string
  end
end
