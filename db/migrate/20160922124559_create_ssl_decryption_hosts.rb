class CreateSslDecryptionHosts < ActiveRecord::Migration
  def change
    create_table :ssl_decryption_hosts do |t|
      t.string  :name
      t.text    :description
      t.string  :host_name
      t.integer :status

      t.timestamps null: false
    end
  end
end
