class AddNewColumnsToStatistics < ActiveRecord::Migration
  def change
    add_column :statistics, :weekly_registrations_count,          :integer, default: 0
    add_column :statistics, :total_registrations_count,           :integer, default: 0
    add_column :statistics, :weekly_active_panelists_count,       :integer, default: 0
    add_column :statistics, :total_active_panelists_count,        :integer, default: 0
    add_column :statistics, :total_actively_participating_count,  :integer, default: 0
  end
end
