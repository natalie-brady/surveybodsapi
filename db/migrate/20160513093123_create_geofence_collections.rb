class CreateGeofenceCollections < ActiveRecord::Migration
  def change
    create_table :geofence_collections do |t|
      t.string      :name
      t.text        :description

      t.timestamps null: false
    end

    add_column :geofence_groups, :geofence_collection_id, :integer
    add_column :geofences, :geofence_collection_id, :integer
  end
end
