class CreateAccessKeys < ActiveRecord::Migration
  def change
    create_table :access_keys do |t|
      t.string :access_token
      t.string :email
      t.datetime :expiry

      t.timestamps null: false
    end
  end
end
