class CreateVpnPageMonitorings < ActiveRecord::Migration
  def change
    create_table :vpn_page_monitorings do |t|
      t.integer :frequency_of_calls,          default: 300000
      t.string  :page_url
      t.string  :page_success_message
      t.integer :response_timeout_threshold,  default: 800
      t.integer :error_alert_count,           default: 3
      t.text    :email_alert_message
      t.string  :network_interface,           default: 'tun0'
      t.string  :dns_host,                    default: '100.100.100.10'
      t.string  :dns_port,                    default: '3128'

      t.timestamps null: false
    end
  end
end
