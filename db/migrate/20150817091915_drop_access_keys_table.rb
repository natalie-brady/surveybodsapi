class DropAccessKeysTable < ActiveRecord::Migration
  def change
    drop_table :access_keys
  end
end
