class AddIndexesForQuestbackGroupQueries < ActiveRecord::Migration
  def change
    add_index :questback_group_users, :uid,                name: 'questback_group_users_uid'
    add_index :questback_group_users, :questback_group_id, name: 'questback_group_users_questback_group_id'
    add_index :geofence_groups,       :questback_group_id, name: 'geofence_groups_questback_group_id'
    add_index :geofences,             :geofence_group_id,  name: 'geofences_geofence_group_id'
  end
end
