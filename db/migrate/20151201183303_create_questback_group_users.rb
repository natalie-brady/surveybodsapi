class CreateQuestbackGroupUsers < ActiveRecord::Migration
  def change
    create_table :questback_group_users do |t|
      t.belongs_to :questback_group, index: true
      t.integer :uid

      t.timestamps null: false
    end
    add_foreign_key :questback_group_users, :questback_groups
  end
end
