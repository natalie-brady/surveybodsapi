class CreateMasterDataDescriptions < ActiveRecord::Migration
  def change
    create_table :master_data_descriptions do |t|
      t.string :varname
      t.string :label
      t.string :datatype
      t.text :categories

      t.timestamps null: false
    end
  end
end
