class CreateGeofenceLogExports < ActiveRecord::Migration
  def change
    create_table :geofence_log_exports do |t|
      t.belongs_to  :user, index: true
      t.attachment  :file_csv
      t.string      :file_md5
      t.timestamps null: false
    end
  end
end
