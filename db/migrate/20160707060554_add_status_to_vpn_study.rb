class AddStatusToVpnStudy < ActiveRecord::Migration
  def change
    add_column :vpn_studies, :active, :boolean, default: true
  end
end
