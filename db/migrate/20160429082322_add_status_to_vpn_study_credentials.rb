class AddStatusToVpnStudyCredentials < ActiveRecord::Migration
  def change
    add_column :vpn_study_credentials, :status, :integer, default: 0
  end
end
