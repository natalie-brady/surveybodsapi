class AddColumnToSslDecryptionHost < ActiveRecord::Migration
  def change
    add_reference :ssl_decryption_hosts, :decrypthosts, index: true
  end
end
