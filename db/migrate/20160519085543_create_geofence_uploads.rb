class CreateGeofenceUploads < ActiveRecord::Migration
  def change
    create_table :geofence_uploads do |t|
      t.belongs_to  :geofence_collection, index: true
      t.string      :name
      t.float       :latitude
      t.float       :longitude
      t.integer     :radius

      t.timestamps null: false
    end

    remove_column :geofences, :geofence_collection_id
  end
end
