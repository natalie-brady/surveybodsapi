class AddUsernameToVpnStudyCredential < ActiveRecord::Migration
  def change
    add_column :vpn_study_credentials, :username, :string
  end
end
