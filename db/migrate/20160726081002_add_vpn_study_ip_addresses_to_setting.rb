class AddVpnStudyIpAddressesToSetting < ActiveRecord::Migration
  def change
    add_column :settings, :vpn_study_ip_addresses, :text
  end
end
