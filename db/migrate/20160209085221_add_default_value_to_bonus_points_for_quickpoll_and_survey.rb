class AddDefaultValueToBonusPointsForQuickpollAndSurvey < ActiveRecord::Migration
  def self.up
    change_column :surveys, :bonus_points, :integer, default: 0
    change_column :quick_polls, :bonus_points, :integer, default: 0
  end

  def self.down
    change_column :surveys, :bonus_points, :integer, default: nil
    change_column :quick_polls, :bonus_points, :integer, default: nil
  end
end
