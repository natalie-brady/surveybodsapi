class AddMaxDurationToVpnStudies < ActiveRecord::Migration
  def change
    add_column :vpn_studies, :max_duration, :integer, default: 0
  end
end
