class AddReferPromotionIdToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :refer_promotion_id, :string
  end
end
