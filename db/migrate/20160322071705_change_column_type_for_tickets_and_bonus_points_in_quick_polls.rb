class ChangeColumnTypeForTicketsAndBonusPointsInQuickPolls < ActiveRecord::Migration
  def change
    change_column :quick_polls, :tickets, :string
    change_column :quick_polls, :bonus_points, :string
  end
end
