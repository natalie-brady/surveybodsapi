class ChangeGeofencePrecision < ActiveRecord::Migration
  def change
    change_column :geofences, :latitude, :decimal, precision: 30, scale: 15
    change_column :geofences, :longitude, :decimal, precision: 30, scale: 15
  end
end
