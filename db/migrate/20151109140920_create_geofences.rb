class CreateGeofences < ActiveRecord::Migration
  def change
    create_table :geofences do |t|
      t.string :name
      t.string :description
      t.float :latitude
      t.float :longitude
      t.integer :radius
      t.integer :geofence_type
      t.integer :action
      t.belongs_to :survey, index: true
      t.belongs_to :quick_poll, index: true
      t.integer :delay
      t.string :message

      t.timestamps null: false
    end
    add_foreign_key :geofences, :surveys
    add_foreign_key :geofences, :quick_polls
  end
end
