# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170331073103) do

  create_table "bart_actions", force: :cascade do |t|
    t.integer  "bart_setting_id",   limit: 4
    t.text     "search_term",       limit: 65535
    t.string   "survey_url",        limit: 255
    t.string   "survey_parameters", limit: 255
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "bart_audit_logs", force: :cascade do |t|
    t.integer  "panelist_id",                limit: 4
    t.integer  "usage_study_access_log_id",  limit: 4
    t.string   "access_log_timestamp",       limit: 255
    t.string   "access_log_search_term",     limit: 255
    t.boolean  "access_log_email_sent",      limit: 1
    t.datetime "access_log_email_timestamp"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "bart_audit_logs", ["panelist_id"], name: "index_bart_audit_logs_on_panelist_id", using: :btree
  add_index "bart_audit_logs", ["usage_study_access_log_id"], name: "index_bart_audit_logs_on_usage_study_access_log_id", using: :btree

  create_table "bart_exclusions", force: :cascade do |t|
    t.integer  "bart_action_id", limit: 4
    t.integer  "panelist_id",    limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "bart_settings", force: :cascade do |t|
    t.integer  "vpn_study_id",                 limit: 4
    t.datetime "start_activation_period"
    t.datetime "end_activation_period"
    t.integer  "frequency_checker",            limit: 4
    t.integer  "exclusion_setting",            limit: 4
    t.boolean  "exclude_all",                  limit: 1
    t.string   "action_csv_file_file_name",    limit: 255
    t.string   "action_csv_file_content_type", limit: 255
    t.integer  "action_csv_file_file_size",    limit: 4
    t.datetime "action_csv_file_updated_at"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "email_template_file_name",     limit: 255
    t.string   "email_template_content_type",  limit: 255
    t.integer  "email_template_file_size",     limit: 4
    t.datetime "email_template_updated_at"
    t.string   "email_subject",                limit: 255
  end

  add_index "bart_settings", ["vpn_study_id"], name: "index_bart_settings_on_vpn_study_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",    limit: 4,     default: 0, null: false
    t.integer  "attempts",    limit: 4,     default: 0, null: false
    t.text     "handler",     limit: 65535,             null: false
    t.text     "last_error",  limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",   limit: 255
    t.string   "queue",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "status_text", limit: 65535
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "device_metadata", force: :cascade do |t|
    t.integer  "panelist_id",         limit: 4
    t.string   "network_provider",    limit: 255
    t.string   "device_model",        limit: 255
    t.string   "device_manufacturer", limit: 255
    t.text     "more_info",           limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "device_metadata", ["panelist_id"], name: "index_device_metadata_on_panelist_id", using: :btree

  create_table "faqs", force: :cascade do |t|
    t.string   "question",   limit: 255
    t.text     "answer",     limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "geofence_collections", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.text     "description",       limit: 65535
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
  end

  create_table "geofence_groups", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.text     "description",            limit: 65535
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "questback_group_id",     limit: 4
    t.integer  "geofence_type",          limit: 4
    t.integer  "dwell_time",             limit: 4
    t.integer  "survey_id",              limit: 4
    t.integer  "quick_poll_id",          limit: 4
    t.string   "message",                limit: 255
    t.integer  "action",                 limit: 4
    t.string   "survey_title",           limit: 255
    t.integer  "geofence_collection_id", limit: 4
    t.integer  "status",                 limit: 4,     default: 1
    t.integer  "trigger_type",           limit: 4,     default: 0
  end

  add_index "geofence_groups", ["questback_group_id"], name: "geofence_groups_questback_group_id", using: :btree
  add_index "geofence_groups", ["quick_poll_id"], name: "index_geofence_groups_on_quick_poll_id", using: :btree
  add_index "geofence_groups", ["survey_id"], name: "index_geofence_groups_on_survey_id", using: :btree

  create_table "geofence_log_exports", force: :cascade do |t|
    t.integer  "user_id",               limit: 4
    t.string   "file_csv_file_name",    limit: 255
    t.string   "file_csv_content_type", limit: 255
    t.integer  "file_csv_file_size",    limit: 4
    t.datetime "file_csv_updated_at"
    t.string   "file_md5",              limit: 255
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "geofence_log_exports", ["user_id"], name: "index_geofence_log_exports_on_user_id", using: :btree

  create_table "geofence_logs", force: :cascade do |t|
    t.integer  "panelist_id",        limit: 4
    t.integer  "geofence_id",        limit: 4
    t.string   "logged_time",        limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "location_timestamp", limit: 255
    t.string   "location_accuracy",  limit: 255
    t.string   "location_latitude",  limit: 255
    t.string   "location_longitude", limit: 255
    t.string   "device_type",        limit: 255
  end

  add_index "geofence_logs", ["geofence_id"], name: "index_geofence_logs_on_geofence_id", using: :btree
  add_index "geofence_logs", ["panelist_id"], name: "index_geofence_logs_on_panelist_id", using: :btree

  create_table "geofence_uploads", force: :cascade do |t|
    t.integer  "geofence_collection_id", limit: 4
    t.string   "name",                   limit: 255
    t.float    "latitude",               limit: 24
    t.float    "longitude",              limit: 24
    t.integer  "radius",                 limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "geofence_uploads", ["geofence_collection_id"], name: "index_geofence_uploads_on_geofence_collection_id", using: :btree

  create_table "geofences", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.string   "description",       limit: 255
    t.decimal  "latitude",                      precision: 30, scale: 15
    t.decimal  "longitude",                     precision: 30, scale: 15
    t.integer  "radius",            limit: 4
    t.integer  "delay",             limit: 4
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.integer  "geofence_group_id", limit: 4
  end

  add_index "geofences", ["geofence_group_id"], name: "geofences_geofence_group_id", using: :btree

  create_table "log_exports", force: :cascade do |t|
    t.integer  "user_id",           limit: 4
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
    t.string   "file_md5",          limit: 255
    t.string   "export_type",       limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "log_exports", ["user_id"], name: "index_log_exports_on_user_id", using: :btree

  create_table "logs", force: :cascade do |t|
    t.integer  "panelist_id", limit: 4
    t.text     "data",        limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "logs", ["panelist_id"], name: "index_logs_on_panelist_id", using: :btree

  create_table "mail_templates", force: :cascade do |t|
    t.string   "description", limit: 255
    t.integer  "template_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "master_data_descriptions", force: :cascade do |t|
    t.string   "varname",    limit: 255
    t.string   "label",      limit: 255
    t.string   "datatype",   limit: 255
    t.text     "categories", limit: 16777215
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "panelist_sync_logs", force: :cascade do |t|
    t.integer  "user_id",      limit: 4
    t.datetime "started_date"
    t.datetime "ended_date"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "panelist_sync_logs", ["user_id"], name: "index_panelist_sync_logs_on_user_id", using: :btree

  create_table "panelists", force: :cascade do |t|
    t.string   "access_token",                limit: 255
    t.string   "email",                       limit: 255
    t.datetime "last_accessed"
    t.integer  "uid",                         limit: 4
    t.string   "password_digest",             limit: 255
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "avatar_file_name",            limit: 255
    t.string   "avatar_content_type",         limit: 255
    t.integer  "avatar_file_size",            limit: 4
    t.datetime "avatar_updated_at"
    t.boolean  "activated",                   limit: 1,   default: true
    t.string   "external_auth_code",          limit: 255
    t.string   "external_auth_panelist_code", limit: 255
  end

  create_table "panelists_samples", id: false, force: :cascade do |t|
    t.integer "panelist_id", limit: 4
    t.integer "sample_id",   limit: 4
  end

  add_index "panelists_samples", ["panelist_id"], name: "index_panelists_samples_on_panelist_id", using: :btree
  add_index "panelists_samples", ["sample_id"], name: "index_panelists_samples_on_sample_id", using: :btree

  create_table "profile_surveys", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "survey_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "profile_surveys", ["survey_id"], name: "index_profile_surveys_on_survey_id", using: :btree

  create_table "profile_variables", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "value",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "push_notification_credentials", force: :cascade do |t|
    t.string   "token",                limit: 255
    t.integer  "notification_service", limit: 4
    t.integer  "panelist_id",          limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "push_notification_credentials", ["panelist_id"], name: "index_push_notification_credentials_on_panelist_id", using: :btree

  create_table "push_notification_logs", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.integer  "survey_id",          limit: 4
    t.integer  "quick_poll_id",      limit: 4
    t.integer  "questback_group_id", limit: 4
    t.text     "sample_ids",         limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  create_table "questback_group_users", force: :cascade do |t|
    t.integer  "questback_group_id", limit: 4
    t.integer  "uid",                limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "questback_group_users", ["questback_group_id"], name: "index_questback_group_users_on_questback_group_id", using: :btree
  add_index "questback_group_users", ["questback_group_id"], name: "questback_group_users_questback_group_id", using: :btree
  add_index "questback_group_users", ["uid"], name: "questback_group_users_uid", using: :btree

  create_table "questback_groups", force: :cascade do |t|
    t.integer  "group_id",    limit: 4
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "quick_poll_choices", force: :cascade do |t|
    t.string   "answer",                 limit: 255
    t.integer  "quick_poll_question_id", limit: 4
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "answer_type",            limit: 4,   default: 0
    t.boolean  "anchored",               limit: 1,   default: false
  end

  add_index "quick_poll_choices", ["quick_poll_question_id"], name: "index_quick_poll_choices_on_quick_poll_question_id", using: :btree

  create_table "quick_poll_questions", force: :cascade do |t|
    t.string   "question_type",   limit: 255
    t.string   "question",        limit: 255
    t.integer  "quick_poll_id",   limit: 4
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.boolean  "randomize_order", limit: 1,   default: false
  end

  add_index "quick_poll_questions", ["quick_poll_id"], name: "index_quick_poll_questions_on_quick_poll_id", using: :btree

  create_table "quick_poll_responses", force: :cascade do |t|
    t.integer  "panelist_id",            limit: 4
    t.integer  "quick_poll_choice_id",   limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "quick_poll_choice_text", limit: 255
  end

  add_index "quick_poll_responses", ["panelist_id"], name: "index_quick_poll_responses_on_panelist_id", using: :btree
  add_index "quick_poll_responses", ["quick_poll_choice_id"], name: "index_quick_poll_responses_on_quick_poll_choice_id", using: :btree

  create_table "quick_poll_stats", force: :cascade do |t|
    t.integer  "quick_poll_id",                   limit: 4
    t.integer  "completed_panelists_count",       limit: 4
    t.integer  "questback_group_panelists_count", limit: 4
    t.boolean  "responded_to",                    limit: 1
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "quick_poll_stats", ["quick_poll_id"], name: "index_quick_poll_stats_on_quick_poll_id", using: :btree

  create_table "quick_polls", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.text     "description",        limit: 65535
    t.string   "duration",           limit: 255
    t.string   "bonus_points",       limit: 255,   default: "0"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "quick_poll_type",    limit: 4,     default: 0
    t.integer  "questback_group_id", limit: 4
    t.datetime "start_time"
    t.datetime "end_time"
    t.boolean  "active",             limit: 1,     default: false
    t.string   "tickets",            limit: 255,   default: "0"
  end

  create_table "router_managements", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.text     "description",        limit: 65535
    t.string   "ip_address_range",   limit: 255
    t.integer  "status",             limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "net_mask",           limit: 255
    t.integer  "vpnroute_id",        limit: 4
    t.string   "initial_ip_address", limit: 255
    t.string   "bit_mask",           limit: 255
  end

  add_index "router_managements", ["vpnroute_id"], name: "index_router_managements_on_vpnroute_id", using: :btree

  create_table "samples", force: :cascade do |t|
    t.integer  "sample_id",   limit: 4
    t.string   "title",       limit: 255
    t.string   "description", limit: 255
    t.integer  "survey_id",   limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string   "qb_hostname",                   limit: 255
    t.string   "qb_api_username",               limit: 255
    t.string   "qb_api_password",               limit: 255
    t.integer  "auth_timeout_seconds",          limit: 4
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.integer  "site_id",                       limit: 4
    t.integer  "mail_template_id",              limit: 4
    t.text     "apn_cert",                      limit: 65535
    t.string   "apn_passphrase",                limit: 255
    t.integer  "survey_category_id",            limit: 4
    t.integer  "questback_external_service_id", limit: 4
    t.string   "gcm_key",                       limit: 255
    t.integer  "android_minimum_version",       limit: 4,     default: 0
    t.string   "ios_minimum_version",           limit: 255,   default: "0"
    t.string   "refer_promotion_id",            limit: 255
    t.integer  "pci",                           limit: 4,     default: 1
    t.string   "vpn_hostname",                  limit: 255
    t.integer  "vpn_study_disconnect_delay",    limit: 4,     default: 3
    t.text     "vpn_study_ip_addresses",        limit: 65535
  end

  create_table "ssl_decryption_hosts", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.text     "description",     limit: 65535
    t.string   "host_name",       limit: 255
    t.integer  "status",          limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "decrypthosts_id", limit: 4
  end

  add_index "ssl_decryption_hosts", ["decrypthosts_id"], name: "index_ssl_decryption_hosts_on_decrypthosts_id", using: :btree

  create_table "statistics", force: :cascade do |t|
    t.integer  "week_number",                        limit: 4
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.integer  "weekly_registrations_count",         limit: 4, default: 0
    t.integer  "total_registrations_count",          limit: 4, default: 0
    t.integer  "weekly_active_panelists_count",      limit: 4, default: 0
    t.integer  "total_active_panelists_count",       limit: 4, default: 0
    t.integer  "total_actively_participating_count", limit: 4, default: 0
  end

  create_table "survey_categories", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "color",       limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "surveys", force: :cascade do |t|
    t.integer  "survey_id",           limit: 4
    t.string   "title",               limit: 255
    t.datetime "created_time"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "bonus_points",        limit: 4,   default: 0
    t.integer  "survey_category_id",  limit: 4
    t.string   "duration",            limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "active",              limit: 1,   default: false
    t.string   "questback_status",    limit: 255
    t.boolean  "registration_survey", limit: 1,   default: false
    t.string   "tickets",             limit: 255, default: "0"
  end

  add_index "surveys", ["survey_category_id"], name: "index_surveys_on_survey_category_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vpn_page_monitoring_results", force: :cascade do |t|
    t.datetime "start_time"
    t.integer  "test_status",   limit: 4
    t.string   "ellapsed_time", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "vpn_page_monitorings", force: :cascade do |t|
    t.integer  "frequency_of_calls",         limit: 4,     default: 300000
    t.string   "page_url",                   limit: 255
    t.string   "page_success_message",       limit: 255
    t.integer  "response_timeout_threshold", limit: 4,     default: 800
    t.integer  "error_alert_count",          limit: 4,     default: 3
    t.text     "email_alert_addresses",      limit: 65535
    t.string   "network_interface",          limit: 255,   default: "tun0"
    t.string   "dns_host",                   limit: 255,   default: "100.100.100.10"
    t.string   "dns_port",                   limit: 255,   default: "3128"
    t.datetime "created_at",                                                          null: false
    t.datetime "updated_at",                                                          null: false
  end

  create_table "vpn_studies", force: :cascade do |t|
    t.integer  "questback_group_id", limit: 4
    t.string   "name",               limit: 255
    t.text     "description",        limit: 65535
    t.datetime "end_time"
    t.string   "bonus_points",       limit: 255,   default: "0"
    t.string   "tickets",            limit: 255,   default: "0"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "active",             limit: 1,     default: true
    t.datetime "last_start_time"
    t.integer  "max_duration",       limit: 4,     default: 0
  end

  add_index "vpn_studies", ["questback_group_id"], name: "index_vpn_studies_on_questback_group_id", using: :btree

  create_table "vpn_study_credentials", force: :cascade do |t|
    t.integer  "panelist_id",                   limit: 4
    t.string   "ios_config_file_name",          limit: 255
    t.string   "ios_config_content_type",       limit: 255
    t.integer  "ios_config_file_size",          limit: 4
    t.datetime "ios_config_updated_at"
    t.string   "android_config_file_name",      limit: 255
    t.string   "android_config_content_type",   limit: 255
    t.integer  "android_config_file_size",      limit: 4
    t.datetime "android_config_updated_at"
    t.string   "android_ssl_cert_file_name",    limit: 255
    t.string   "android_ssl_cert_content_type", limit: 255
    t.integer  "android_ssl_cert_file_size",    limit: 4
    t.datetime "android_ssl_cert_updated_at"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.integer  "status",                        limit: 4,   default: 0
    t.integer  "vpn_study_id",                  limit: 4
    t.string   "username",                      limit: 255
  end

  add_index "vpn_study_credentials", ["panelist_id"], name: "index_vpn_study_credentials_on_panelist_id", using: :btree
  add_index "vpn_study_credentials", ["username"], name: "index_vpn_study_credentials_on_username", length: {"username"=>191}, using: :btree

  add_foreign_key "logs", "panelists"
  add_foreign_key "profile_surveys", "surveys"
  add_foreign_key "push_notification_credentials", "panelists"
  add_foreign_key "questback_group_users", "questback_groups"
  add_foreign_key "quick_poll_stats", "quick_polls"
  add_foreign_key "surveys", "survey_categories"
end
