class DevicesNotifier
  include Notifier

  def initialize(panelists:, type:, type_id:, custom_message: nil, action: nil)
    @panelists            = panelists
    @notification_type    = type
    @notification_message = custom_message || notification_message
    @notification_type_id = type_id
    @action_type          = action.nil? ? 'new' : action
  end

  def notify_apple_devices
    push_notification_client  = PushNotification.new

    @panelists.each do |panelist|
      push_notification_credentials = panelist.try(:push_notification_credentials).try(:apple_devices)
      push_notification_credentials.each do |push_notification_credential|
        push_notification_client.send(push_notification_credential.token, @notification_message, notification_data[:ios])
      end
    end
  end

  def notify_android_devices
    gcm         = GoogleCloudMessage.new
    gcm_tokens  = @panelists.map { |panelist| panelist.push_notification_credentials.android_devices.map(&:token) }.flatten

    if gcm_tokens.any?
      gcm_tokens.each_slice(1000) do |tokens|
        gcm.send(tokens, notification_data[:android])
      end
    end
  end

  private

  def notification_data
    {
      ios: {
        @notification_type => {
          id: @notification_type_id,
          action: @action_type
        }
      },
      android: {
        action: @action_type,
        message: @notification_message,
        "#{@notification_type}_id" => @notification_type_id
      }
    }.deep_symbolize_keys
  end

  def notification_message
    "New #{@notification_type.humanize.downcase}!"
  end

end
