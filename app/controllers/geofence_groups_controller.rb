class GeofenceGroupsController < ApplicationController
  include CsvExport

  autocomplete :geofence_group, :name, extra_data: [:id], full: true, limit: 50

  before_action :authenticate_user!
  before_action :find_geofence_group, only: [:show, :edit, :update, :status_update, :map]
  before_action :check_all_params, only: [:import]
  before_action :verify_valid_file, only: [:import]
  before_action :verify_filter_params, only: [:index]

  def index
    @geofence_groups    = GeofenceGroup.search(params[:search]).send(@filter_status)

    @reload_in_progress = Delayed::Job.where("handler like ?", "%#{GeofenceAppsReloadJob.to_s}%").size != 0

    respond_to do |format|
      format.html
      format.js

    @geofence_group_active = GeofenceGroup.where(status: 1)
    @geofence_group_inactive = GeofenceGroup.where(status: 0)
    end
  end

  def show
  end

  def new
    @geofence_group = GeofenceGroup.new
  end

  def map
    @map_data = get_map_data(GeofenceGroup.find(params[:id]).geofences, false)
  end

  def create
    @geofence_group = GeofenceGroup.new(geofence_group_params)
    if @geofence_group.save
      redirect_to geofence_group_path(@geofence_group)
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @geofence_group.update(geofence_group_params)
      redirect_to geofence_group_path
    else
      render 'edit'
    end
  end

  def status_update
    @geofence_group.send(params[:status])
    @filter_status = params[:filter_status]
    respond_to do |format|
      format.html { (redirect_to geofence_group_path(@geofence_group), notice: "You've successfully #{params[:status]}d #{@geofence_group.to_s}") }
      format.js
    end
  end

  def reload_apps
    GeofenceAppsReloadJob.perform_later
    respond_to do |format|
      format.js
    end
  end

  def export
    export_params = {
      records:  GeofenceGroup.all,
      header:   GeofenceGroup.csv_attributes
    }
    send_data generate_csv_file(export_params), :filename => "geofence_group_export.csv", :type => "text/plain"
  end

  private
    def geofence_group_params
      params.require(:geofence_group).permit(
        :name,
        :description,
        :questback_group_id,
        :message,
        :survey_id,
        :quick_poll_id,
        :action,
        :geofence_type,
        :dwell_time,
        :survey_title,
        :geofence_collection_id,
        :status,
        :trigger_type
      )
    end

    def get_map_data(background_fences, editable)
      background_fences_data = []
      background_fences.each do |fence|
        background_fences_data << {
          :icon => ActionController::Base.helpers.asset_path("rb_icon.png"),
          :latitude => fence.latitude,
          :longitude => fence.longitude,
          :radius => fence.radius,
          :active => false,
          :editable => false,
          :name => fence.name,
          :description => fence.description,
          :link => geofence_path(fence)
        }
      end

      return {
        :active_fence => nil,
        :background_fences => background_fences_data
      }
    end

    def find_geofence_group
      @geofence_group = GeofenceGroup.find(params[:id])
    end

    def check_all_params
      unless params[:file] && params[:geofence_group_id]
        redirect_to upload_geofence_groups_path, alert: 'Please select a Geofence Group or a file to upload.'
      end
    end

    def verify_valid_file
      unless File.extname(params[:file].original_filename).eql?('.csv')
        redirect_to upload_geofence_groups_path, alert: 'Please use a valid .csv file.'
      end
    end

    def verify_filter_params
      @filter_status = params[:filter] ? params[:filter][:status].to_sym : :all
    end

end
