class VpnBandwidthUsagesController < ApplicationController
  include CsvExport

  before_action :authenticate_user!
  before_action :find_vpn_bandwidth_usages_and_filename, only: [:export]

  def index
    @vpn_bandwidth_usages = VpnBandwidthUsage.all.paginate(page: params[:page])
  end

  def export
    export_params = {
      records: @vpn_bandwidth_usages,
      header: VpnBandwidthUsage.csv_headers
    }
    send_data generate_csv_file(export_params), filename: @filename, type: 'text/plain'
  end

  private

  def find_vpn_bandwidth_usages_and_filename
    @vpn_bandwidth_usages = VpnBandwidthUsage.all
    @filename             = 'VpnBandwidthUsage.csv'
  end
end
