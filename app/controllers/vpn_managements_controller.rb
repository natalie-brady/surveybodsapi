class VpnManagementsController < ApplicationController
  before_action :authenticate_user!, except: [:vpn_speed_test]

  def vpn_speed_test
    @vpn_page_monitoring = VpnPageMonitoring.first
  end
end
