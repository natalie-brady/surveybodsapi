class SurveyCategoriesController < ApplicationController

  before_action :authenticate_user!
  before_action :find_survey_category, only: [:show, :edit, :update, :destroy]

  def index
    @survey_categories = SurveyCategory.all
  end

  def show
  end

  def new
    @survey_category = SurveyCategory.new
  end

  def create
    @survey_category = SurveyCategory.new(survey_categories_params)
    if @survey_category.save
      redirect_to @survey_category
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @survey_category.update(survey_categories_params)
      redirect_to @survey_category
    else
      render 'edit'
    end
  end

  def destroy
    if @survey_category.destroy
      redirect_to survey_categories_path
    else
      redirect_to @survey_category
    end
  end

  private

    def survey_categories_params
      params.require(:survey_category).permit(
        :name,
        :description,
        :color
      )
    end

    def find_survey_category
      @survey_category = SurveyCategory.find(params[:id])
    end

end
