class VpnRoutesController < ApplicationController
  before_action :authenticate_user!

  def index
    @vpn_routes = VpnRoute.all.paginate(page: params[:page])
  end
end
