class VpnStudyCredentialsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_panelist, only: [:new, :create]
  before_action :find_vpn_study_credential, only: [:show, :edit, :update, :destroy]

  def index
        @vpn_study_credentials  = VpnStudyCredential.search(params[:search])
                      .ordered
                      .paginate(page: params[:page])

    @vpn_pending = VpnStudyCredential.where(status: 0)
    @vpn_cancelled = VpnStudyCredential.where(status: 3)
    @vpn_active = VpnStudyCredential.where(status: 1)
    @vpn_complete = VpnStudyCredential.where(status: 2)
  end

  def new
    @vpn_study_credential = @panelist.vpn_study_credentials.new
  end

  def create
    @vpn_study_credential = @panelist.vpn_study_credentials.new(vpn_study_credential_params)
    @vpn_study_credential.save ? redirect_to(vpn_study_credentials_path, notice: "You have successfully created a vpn study credential for #{@panelist.to_s}.") : render("new")
  end

  def show
  end

  def edit
  end

  def update
    @vpn_study_credential.update_attributes(vpn_study_credential_params) ? redirect_to(vpn_study_credentials_path, notice: "You have successfully updated vpn study credential") : render("edit")
  end

  def destroy
    @vpn_study_credential.complete!
    redirect_to vpn_study_credentials_path
  end

  private

  def vpn_study_credential_params
    params.require(:vpn_study_credential).permit(
      :ios_config,
      :android_config,
      :android_ssl_cert,
      :status,
      :vpn_study_id
    )
  end

  def find_panelist
    @panelist = Panelist.find_by(access_token: params[:access_token])
  end

  def find_vpn_study_credential
    @vpn_study_credential = VpnStudyCredential.find params[:id]
  end
end
