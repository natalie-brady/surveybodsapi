class SurveysController < ApplicationController

  before_action :authenticate_user!
  before_action :find_delayed_job, only: [:index]
  before_action :find_survey, except: [:index, :reload]
  autocomplete :survey, :title, extra_data: [:id], full: true, limit: 100

  def index
    @surveys = Survey.search(params[:search]).ordered.paginate(page: params[:page])
    respond_to do |format|
      format.html
      format.js
    @reload_in_progress = @delayed_job.size != 0
    @reload_status      = @reload_in_progress ? @delayed_job.first.status_text : ""

    @survey_active = Survey.where(questback_status: "ACTIVE")
    @survey_generated = Survey.where(questback_status: "GENERATED")
    @survey_archived = Survey.where(questback_status: "ARCHIVED")
    @survey_closing = Survey.where(questback_status: "CLOSING")
    @survey_finished = Survey.where(questback_status: "FINISHED")
  end
end

  def show
  end

  def edit
  end

  def update
    if @survey.update(surveys_params)
      redirect_to @survey
    else
      render 'edit'
    end
  end

  def reload
    params[:active].eql?("true") ? SurveysReloadJob.perform_later(active: true) : SurveysReloadJob.perform_later
    respond_to do |format|
      format.html { redirect_to setting_path(Setting.first) }
      format.js
    end
  end

  def send_notifications
    if @survey
      params[:sample_ids].each do |sample_id|
        SurveySendNotificationsJob.perform_later(survey_id: @survey.survey_id, sample_id: sample_id)
      end
      create_survey_send_notification_log
    end
    redirect_to surveys_path, notice: "You've successfully sent push notification."
  end

  def show_push_notification_form
    @samples = @survey.samples
    render(partial: 'send_push_notification_form', locals: { survey: @survey }, layout: false)
  end

  private

    def surveys_params
      params.require(:survey).permit(
        :survey_id,
        :survey_category_id,
        :duration,
        :active,
        :registration_survey
      )
    end

    def find_delayed_job
      @delayed_job = Delayed::Job.where("handler like ?", "%#{SurveysReloadJob.to_s}%")
    end

    def find_survey
      @survey = Survey.find(params[:id])
    end

    def create_survey_send_notification_log
      push_notification_log_params = {
        survey_id: params[:id],
        sample_ids: params[:sample_ids]
      }
      current_user.push_notification_logs.create(push_notification_log_params)
    end

    def verify_filter_params
      @filter_status = params[:filter] ? params[:filter][:status].to_sym : :all
    end

end
