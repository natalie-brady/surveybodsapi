class ProfileSurveysController < ApplicationController
  before_action :authenticate_user!
  before_action :set_profile_survey, only: [:show, :edit, :update, :destroy]

  def index
    @profile_surveys = ProfileSurvey.all
  end

  def show
  end

  def new
    @profile_survey = ProfileSurvey.new
  end

  def create
    @profile_survey = ProfileSurvey.new(profile_surveys_params)
    if @profile_survey.save
      redirect_to @profile_survey
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @profile_survey.update(profile_surveys_params)
      redirect_to @profile_survey
    else
      render 'edit'
    end
  end

  def destroy
    if @profile_survey.destroy
      redirect_to profile_surveys_path
    else
      redirect_to @profile_survey
    end
  end

  private

    def profile_surveys_params
      params.require(:profile_survey).permit(
        :name,
        :survey_id
      )
    end

    def set_profile_survey
      @profile_survey = ProfileSurvey.find(params[:id])
    end

end
