class GeofenceCollectionsController < ApplicationController
  before_action :authenticate_user!

  before_action :find_geofence_collection, only: [:show, :edit, :update, :destroy]
  after_action  :process_file_csv, only: [:create], if: -> { @geofence_collection.save }

  def index
    @geofence_collections = GeofenceCollection.search(params[:search])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @geofence_collection = GeofenceCollection.new
  end

  def create
    @geofence_collection = GeofenceCollection.new(geofence_collection_param)
    @geofence_collection.save ? redirect_to(geofence_collections_path, notice: create_success_message) : render("new")
  end

  def show
  end

  def upload
    @geofence_collections = GeofenceCollection.all
  end

  def import
    geofence_collection  = GeofenceCollection.find_by(id: params[:geofence_collection_id])
    if params[:file].present?
      geofence_collection.import(params[:file])
      message       = { notice: "Successfully imported geofences for #{geofence_collection.name}." }
      redirect_path = geofence_collection_path(geofence_collection)
    else
      message       = { alert: "No file found. Please attach the file to upload." }
      redirect_path = upload_geofence_collections_path(geofence_collection_id: geofence_collection.id)
    end

    redirect_to redirect_path, message
  end

  private

  def geofence_collection_param
    params.require(:geofence_collection).permit(
      :name,
      :description,
      :file
    )
  end

  def find_geofence_collection
    @geofence_collection = GeofenceCollection.find params[:id]
  end

  def process_file_csv
    GeofenceCollectionProcessCsvJob.perform_later(geofence_collection_id: @geofence_collection.id) if @geofence_collection.file.exists?
  end

  def create_success_message
    'Successfully created Geofence Collection. Your uploaded file will be processed in the background'
  end
end
