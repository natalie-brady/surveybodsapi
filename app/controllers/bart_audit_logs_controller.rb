class BartAuditLogsController < ApplicationController
  before_action :authenticate_user!
  before_action :sanitize_params

  def index
    @bart_audit_logs = BartAuditLog.search(params[:search]).ordered.paginate(page: params[:page])
    respond_to do |format|
      format.html
      format.js
    end
  end

  private

  def sanitize_params
    params[:search][:panelist_id] = params[:search].delete(:id) if params[:search] && params[:search][:id]
  end

end
