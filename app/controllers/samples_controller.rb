class SamplesController < ApplicationController

  before_action :authenticate_user!
  before_action :find_delayed_job, only: [:index]

  def index
    @samples = Sample.search(params[:search]).ordered.paginate(page: params[:page])
    respond_to do |format|
      format.html
      format.js
    @reload_in_progress = @delayed_job.size != 0
    @reload_status      = @reload_in_progress ? @delayed_job.first.status_text : ""
  end
end

  def show
    @sample = Sample.find(params[:id])
  end

  def reload
    params[:active] ? ActiveSamplesReloadJob.perform_later : SamplesReloadJob.perform_later
    respond_to do |format|
      format.js
    end
  end

  private

  def find_delayed_job
    @delayed_job =  params[:active] ? Delayed::Job.where("handler like ?", "%#{ActiveSamplesReloadJob.to_s}%") : Delayed::Job.where("handler like ?", "%#{SamplesReloadJob.to_s}%")
  end

end
