class DeviceMetadataController < ApplicationController
  before_action :authenticate_user!
  before_action :find_panelist_from_params, only: [:index]

  def index
    @device_metadata  = DeviceMetadatum.search(params[:search]).ordered.paginate(page: params[:page])
  end

  private

  def find_panelist_from_params
    @panelist = Panelist.find_by(id: params[:id]) if params[:id].present?
  end

end
