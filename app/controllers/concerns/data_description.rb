module DataDescription
  extend ActiveSupport::Concern

  def survey_data(survey, completed: false, qb_surveys: nil)
    questback_survey  = qb_surveys.nil? ? nil : qb_surveys.select{ |qb_survey| qb_survey if qb_survey['id'].eql?(survey.survey_id) }.first
    survey_title      = questback_survey.nil? ? survey.title : questback_survey['title']
    {
      title:                survey_title,
      id:                   survey.survey_id,
      duration:             survey.duration.nil? ? '' : survey.duration,
      start_time:           survey.start_time.to_i,
      end_time:             survey.end_time.to_i,
      bonus_points:         survey.bonus_points,
      registration_survey:  survey.registration_survey,
      url:                  completed ? '' : survey.get_panelist_url(@panelist),
      category_name:        survey.survey_category.nil? ? '' : survey.survey_category.name,
      category_color:       survey.survey_category.nil? ? '' : survey.survey_category.color
    }
  end

  def geofence_data(geofence)
    {
      id:           geofence.id,
      latitude:     geofence.latitude.to_f,
      longitude:    geofence.longitude.to_f,
      radius:       geofence.radius,
      delay:        geofence.delay.nil? ? 0 : geofence.delay,
      message:      geofence.geofence_group.message,
      type:         geofence.geofence_group.geofence_type.blank? ? '' : geofence.geofence_group.geofence_type,
      action:       geofence.geofence_group.action,
      trigger_type: geofence.geofence_group.trigger_type
    }
  end

  def profile_survey_data(profile_survey)
    {
      title:                profile_survey.name,
      id:                   profile_survey.survey.survey_id,
      duration:             profile_survey.survey.duration.nil? ? '' : profile_survey.survey.duration,
      start_time:           profile_survey.survey.start_time.to_i,
      end_time:             profile_survey.survey.end_time.to_i,
      bonus_points:         profile_survey.survey.bonus_points,
      registration_survey:  profile_survey.survey.registration_survey,
      url:                  profile_survey.survey.get_panelist_url(@panelist),
      category_name:        profile_survey.survey.survey_category.nil? ? '' : profile_survey.survey.survey_category.name,
      category_color:       profile_survey.survey.survey_category.nil? ? '' : profile_survey.survey.survey_category.color
    }
  end

  def vpn_study_credential_data(vpn_study_credential)
    {
      id:               vpn_study_credential.id,
      ios_config:       vpn_study_credential.ios_config.exists? ? "#{root_url}#{vpn_study_credential.ios_config.url}" : "",
      android_config:   vpn_study_credential.android_config.exists? ? "#{root_url}#{vpn_study_credential.android_config.url}" : "",
      android_ssl_cert: vpn_study_credential.android_ssl_cert.exists? ? "#{root_url}#{vpn_study_credential.android_ssl_cert.url}" : "",
      vpn_study_id:     vpn_study_credential.vpn_study_id,
      username:         vpn_study_credential.username
    }
  end

end
