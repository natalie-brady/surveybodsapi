module AppVersionHeader
  extend ActiveSupport::Concern

  def add_app_version_header
    response.headers['X-Android-Min-Version'] = Setting.first.android_minimum_version.to_s
    response.headers['X-Ios-Min-Version'] = Setting.first.ios_minimum_version.to_s
  end

end