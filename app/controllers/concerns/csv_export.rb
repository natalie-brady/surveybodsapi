module CsvExport
  extend ActiveSupport::Concern

  def generate_csv_file(records:, header:)
    CSV.generate do |csv|
      csv << header
      records.map{ |record| csv << record.send(:csv_values) }
    end
  end

  def start_date
    parse_date(params[:start_date]).beginning_of_day
  end

  def end_date
    parse_date(params[:end_date]).end_of_day
  end

  def parse_date(date)
    date.map{ |key, value| value}.join('-').to_datetime
  end

  def valid_date_params?
    !params[:start_date].has_value?('') && !params[:end_date].has_value?('')
  end
end
