class BartSettingsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_bart_setting

  def show
  end

  def export_csv_file
    send_file export_file,
              filename: export_filename,
              disposition: 'attachment',
              x_sendfile: true
  end

  private

  def find_bart_setting
    @bart_setting = BartSetting.find_by_id(params[:id]) if params[:id]
  end

  def export_file
    file = if @bart_setting
             params[:filename].eql?('email') ? @bart_setting.email_template.path : @bart_setting.action_csv_file.path
           else
             @filename = params[:filename].eql?('email') ? 'BARTSettingEmailTemplate.html' : 'BARTActionTableTemplate.csv'
             Rails.root.join('public/templates', @filename)
           end
    file
  end

  def export_filename
    file_name = if @bart_setting
                  params[:filename].eql?('email') ? @bart_setting.email_template.original_filename : @bart_setting.action_csv_file.original_filename
                else
                  @filename
                end
    file_name
  end
end
