class SslDecryptionHostsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_ssl_decryption_hosts, only: [:update, :edit]

  def index
    @ssl_decryption_hosts = SslDecryptionHost.all.paginate(page: params[:page])
  end

  def new
    @ssl_decryption_host = SslDecryptionHost.new
  end

  def create
    @ssl_decryption_host = SslDecryptionHost.new ssl_decryption_host_params
    @ssl_decryption_host.save ?
      redirect_to(ssl_decryption_hosts_path, notice: 'Successfully created SSL Decryption Host') :
      render(:new)
  end

  def edit
  end

  def update
    @ssl_decryption_host.update_attributes(ssl_decryption_host_params) ?
      redirect_to(ssl_decryption_hosts_path, notice: "Successfully updated #{@ssl_decryption_host.name}.") :
      render(:edit)
  end

  def sync
    SslDecryptionHostJob.perform_later(params)
    redirect_to ssl_decryption_hosts_path, notice: "Successfully executed sync."
  end

  private

  def ssl_decryption_host_params
    params.require(:ssl_decryption_host).permit(
      :name,
      :description,
      :host_name,
      :status
    )
  end

  def find_ssl_decryption_hosts
    @ssl_decryption_host = SslDecryptionHost.find params[:id]
  end
end
