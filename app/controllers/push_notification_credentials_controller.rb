class PushNotificationCredentialsController < ApplicationController

  before_action :authenticate_user!
  before_action :find_push_notification_credential

  def show
  end

  def test
    @push_notification_credential.test(params[:data])
    head(200)
  end

  def destroy
    if @push_notification_credential.destroy
      redirect_to panelist_path(Panelist.find(params[:panelist_id]))
    else
      render show
    end
  end

  private

  def find_push_notification_credential
    @push_notification_credential = PushNotificationCredential.find(params[:id])
  end

end
