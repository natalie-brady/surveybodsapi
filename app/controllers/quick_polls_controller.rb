class QuickPollsController < ApplicationController
  autocomplete :quick_poll, :title, extra_data: [:id], full: true

  before_action :authenticate_user!
  before_action :get_quick_poll, only: [:show, :edit, :update, :destroy, :send_notifications]
  before_action :find_delayed_job, only: [:index]

  def index
    @quick_polls = QuickPoll.search(params[:search]).ordered.paginate(page: params[:page])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
  end

  def new
    @quick_poll = QuickPoll.new(:start_time => Time.now, :end_time => Time.now + 7.days)
  end

  def edit
  end

  def update
    if @quick_poll.update_attributes(quick_poll_params)
       redirect_to quick_poll_path
    else
      render 'edit'
    end
  end

  def send_notifications
    if @quick_poll.questback_group
      message = { notice: "You've successfully sent the notification to #{@quick_poll.questback_group.to_s}." }
      @quick_poll.notify
      create_send_notification_log
    else
      message = { alert: "Failed to send notification. No Questback Group set on #{@quick_poll.to_s}." }
    end

    redirect_to quick_polls_path, message
  end

  def create
    @quick_poll = QuickPoll.new(quick_poll_params)
    if @quick_poll.save
       redirect_to quick_polls_path
    else
       render 'new'
    end
  end

  private

  def quick_poll_params
    params.require(:quick_poll).permit(
      :title,
      :description,
      :duration,
      :bonus_points,
      :tickets,
      :quick_poll_type,
      :questback_group_id,
      :start_time,
      :end_time,
      :active,
      :quick_poll_questions_attributes => [
        :id,
        :question,
        :question_type,
        :randomize_order,
        :_destroy,
        :quick_poll_choices_attributes => [
          :id,
          :answer,
          :answer_type,
          :anchored,
          :_destroy
        ]
      ]
    )
  end

  def get_quick_poll
    @quick_poll = QuickPoll.find_by_id params[:id]
  end

  def find_delayed_job
    @delayed_job = Delayed::Job.where("handler like ?", "%#{QuickPollStatsJob.to_s}%")
  end

  def create_send_notification_log
    push_notification_log_params = {
      quick_poll_id: params[:id],
      questback_group_id: @quick_poll.questback_group_id
    }

    current_user.push_notification_logs.create(push_notification_log_params)
  end

end
