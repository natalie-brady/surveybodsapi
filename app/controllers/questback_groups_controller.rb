class QuestbackGroupsController < ApplicationController
  autocomplete :questback_group, :name, extra_data: [:id], limit: 100, full: true

  before_action :find_delayed_job, only: [:index]

  def index
    @questback_groups   = QuestbackGroup.search(params[:search]).ordered_by_name.paginate(:page => params[:page])
    @reload_in_progress = @delayed_job.size != 0
    @reload_status      = @reload_in_progress ? @delayed_job.first.status_text : ""

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @questback_group = QuestbackGroup.find(params[:id])
  end

  def reload
    QuestbackGroupsReloadJob.perform_later
    respond_to do |format|
      format.js
    end
  end

  private

  def find_delayed_job
    @delayed_job = Delayed::Job.where("handler like ?", "%#{QuestbackGroupsReloadJob.to_s}%")
  end

end
