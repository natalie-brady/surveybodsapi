class LogExportsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_log_export, except: [:index]
  before_action :find_delayed_job, only: [:index]
  before_action :initialize_export_params, only: [:export]
  before_action :include_additional_export_params, only: [:export]

  def index
    @log_exports = params[:export_type].present? ? LogExport.send(params[:export_type]) : LogExport.all
    @log_exports = @log_exports.paginate(page: params[:page])

    @export_in_progress   = @delayed_job.size != 0
    @export_status        = @export_in_progress ? @delayed_job.first.status_text : ""
  end

  def download
    send_file @log_export.file.path,
              filename: @log_export.file.original_filename,
              type: 'application/octet-stream',
              disposition: 'attachment',
              x_sendfile: true
  end

  def destroy
    @log_export.nil? ? LogExport.send(params[:export_type]).destroy_all : @log_export.destroy
    redirect_to log_exports_path, notice: "Successfully destroyed #{params[:export_type]} exports."
  end

  def export
    LogExportsJob.perform_later(@export_params)
    redirect_to log_exports_path(export_type: @export_params[:export_type]), notice: "Your export will be ready soon. Please refresh this page to reflect your export."
  end

  private

  def find_log_export
    @log_export = LogExport.find_by id: params[:id]
  end

  def find_delayed_job
    @delayed_job = Delayed::Job.where("handler like ?", "%#{LogExportsJob.to_s}%")
  end

  def initialize_export_params
    @export_params = {
      user_id: current_user.id,
      export_type: params[:export_type]
    }
  end

  def include_additional_export_params
    @export_params[:panelist_id] = params[:panelist_id] if params[:panelist_id]
  end
end
