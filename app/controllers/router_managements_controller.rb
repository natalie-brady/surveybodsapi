class RouterManagementsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_router_management, only: [:update, :edit, :destroy, :sync]
  before_action :validate_ip_address, only: [:convert_ip_address]

  def index
    @router_managements = RouterManagement.all.paginate(page: params[:page])
  end

  def new
    @router_management = RouterManagement.new
  end

  def create
    @router_management = RouterManagement.new(router_management_params)
    @router_management.save ? redirect_to(router_managements_path) : render(:new)
  end

  def edit
  end

  def update
    @router_management.update_attributes(router_management_params) ?
      redirect_to(router_managements_path, notice: "You've successfully updated #{@router_management.name}.") : render(:edit)
  end

  # def destroy
  #   if @router_management.destroy
  #     router_path = router_managements_path
  #     message     = { notice: "You have successfully deleted #{@router_management.name}"}
  #   else
  #     router_path = @router_management
  #     message     = { warning: "There is an error deleting #{@router_management.name}" }
  #   end
  #   redirect_to router_path, message
  # end

  def disconnect
    RouterManagementJob.perform_later(type: 'disconnect', username: params[:username])
    redirect_to router_managements_path, notice: 'Successfully executed disconnect command.'
  end

  def sync
    @router_management.activate! if @router_management
    RouterManagementJob.perform_later(type: 'sync', id: params[:id])
    redirect_to router_managements_path, notice: "Successfully executed sync command."
  end

  def convert_ip_address
    if @valid
      new_ip_address = IPAddr.new(params[:ip_address]).mask(params[:bit_mask]).to_s
      render json: { error: false, ip_address: new_ip_address } if request.xhr?
    else
      render json: { error: true, message: 'Ip Address invalid format.' }
    end
  end

  private

  def router_management_params
    params.require(:router_management).permit(
      :name,
      :description,
      :ip_address_range,
      :status,
      :net_mask,
      :initial_ip_address,
      :bit_mask
    )
  end

  def find_router_management
    @router_management = RouterManagement.find_by id: params[:id]
  end

  def validate_ip_address
     matched_ip = params[:ip_address] =~ /\A((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.)
                                          {3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\Z/x
     @valid = matched_ip.nil? ? false : true
  end

end
