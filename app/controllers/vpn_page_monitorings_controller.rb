class VpnPageMonitoringsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_vpn_page_monitoring, except: [:new, :create]
  before_action :calculate_test_duration, only: [:create, :update]

  def index
    @vpn_page_monitoring = VpnPageMonitoring.first
  end

  def new
    @vpn_page_monitoring  = VpnPageMonitoring.new
  end

  def create
    @vpn_page_monitoring = VpnPageMonitoring.new(vpn_page_monitoring_params)
    @vpn_page_monitoring.save ? redirect_to(vpn_page_monitorings_path, notice: @message) : render(:new)
  end

  def update
    if @vpn_page_monitoring.update(vpn_page_monitoring_params)
     redirect_to vpn_page_monitorings_path, notice: @message
    else
     render :edit
    end
  end

  private

  def vpn_page_monitoring_params
    params.require(:vpn_page_monitoring).permit(
      :frequency_of_calls,
      :page_url,
      :page_success_message,
      :response_timeout_threshold,
      :error_alert_count,
      :email_alert_addresses,
      :network_interface,
      :dns_host,
      :dns_port
    )
  end

  def find_vpn_page_monitoring
    @vpn_page_monitoring = VpnPageMonitoring.find_by id: params[:id]
  end

  def calculate_test_duration
    duration_of_test = vpn_page_monitoring_params[:frequency_of_calls].to_i * vpn_page_monitoring_params[:error_alert_count].to_i
    @message = "Error alert will be fired after #{duration_of_test} milliseconds"
  end
end
