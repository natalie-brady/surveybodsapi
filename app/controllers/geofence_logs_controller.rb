class GeofenceLogsController < ApplicationController
  include CsvExport

  before_action :authenticate_user!
  before_action :sanitize_query_params, only: [:export]
  before_action :find_logs, only: [:panelist_logs]

  def index
    @geofence_logs  = GeofenceLog.search(params[:search])
                      .ordered
                      .paginate(page: params[:page])
  end

  def panelist_logs
  end

  def export
    @valid_params[:panelist_id] = params[:panelist_id] if params[:panelist_id]
    @valid_params[:user_id]     = current_user.id
    if valid_date_params?
      @valid_params[:start_date]  = start_date.to_s
      @valid_params[:end_date]    = end_date.to_s
    end
    GeofenceLogExportJob.perform_later(@valid_params)
    redirect_to geofence_log_exports_path, notice: "Your export will be ready soon. Kindly refresh this page from time to time."
  end

  private

  def find_logs
    @geofence_logs = params[:panelist_id].present? ? panelist_geofence_logs : all_geofence_logs
  end

  def export_filename
    filename  = if params[:panelist_id].present?
                  'panelist_geofence_logs.csv'
                else
                  @geofence_group.nil? ? 'all_geofence_logs.csv' : "#{@geofence_group.name.parameterize}-geofence_logs.csv"
                end
    filename
  end

  def all_geofence_logs
    geofence_logs     = if @geofence_group.nil?
                          GeofenceLog.all
                        else
                          logs_with_params = @geofence_group.geofence_logs.query_by_parameters(@valid_params)
                          logs_with_params = logs_with_params.by_date_range(start_date.to_i, end_date.to_i) if valid_date_params?
                          logs_with_params
                        end
    geofence_logs.ordered
  end

  def panelist_geofence_logs
    panelist      = Panelist.find params[:panelist_id]
    panelist_logs = if @valid_params.nil?
                      panelist.geofence_logs.paginate(page: params[:page])
                    else
                      logs_with_params = panelist.geofence_logs.query_by_parameters(@valid_params)
                      logs_with_params = logs_with_params.by_date_range(start_date.to_i, end_date.to_i) if valid_date_params?
                      logs_with_params
                    end
    panelist_logs.ordered
  end

  def sanitize_query_params
    @valid_params = params[:geofence_log].reject{ |key, value| value.blank? }
  end

end
