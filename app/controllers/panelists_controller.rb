class PanelistsController < ApplicationController
  include CsvExport

  autocomplete :panelist, :email, extra_data: [:id], full: true, limit: 100

  before_action :authenticate_user!
  before_action :find_panelist, only: [:show,:destroy]

  def index
    @panelists  = Panelist.search(params[:search]).ordered.paginate(page: params[:page])
    respond_to do |format|
      format.html
      format.js

    end
  end

  def show
  end

  def destroy
    @panelist.destroy
    redirect_to panelists_path
  end

  def export
    custom_attributes = ['has_apn?', 'has_gcm?']
    export_params = {
      records: Panelist.all,
      header: Panelist.csv_attributes(custom_attributes: custom_attributes)
    }
    send_data generate_csv_file(export_params), :filename => "panelist_export.csv", :type => "text/plain"
  end

  def sync_last_accessed
    PanelistsSyncLastAccessedJob.perform_later(user_id: current_user.id)
    redirect_to panelist_sync_logs_path, notice: "You've successfully execute the sync of panelists last accessed."
  end

  private

  def find_panelist
    @panelist = Panelist.find(params[:id])
  end

  def panelist_stats 
  end

end
