class VpnPageMonitoringResultsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @vpn_page_monitoring_results = VpnPageMonitoringResult.ordered.paginate(page: params[:page])
  end
end
