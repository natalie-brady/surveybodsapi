require 'questback'
class Api::AuthController < ApplicationController
  include Api::AuthHelper

  include AppVersionHeader
  before_action :add_app_version_header

  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
  after_action :log_response

  def login
    # curl -H "Content-Type: application/json" -d '{"auth":{"email":"test@test.com", "password":"password"}}' localhost:3000/api/auth/login
    # TODO Account for Potential of email address changed but there is an SB user with the old email and same UID
    # TODO Add External Auth

    @response = {}
    @qb = ::Questback.new(auth_params[:email])

    @panelist = Panelist.find_by_email(auth_params[:email])
    # Existing SB Panelist
    if @panelist != nil

      if @panelist.activated
        if @panelist.authenticate(auth_params[:password])
          @response[:success] = true
          @response[:access_token] = @panelist.access_token
        else
          if @qb.is_valid_login(auth_params[:password])
            @panelist.password = auth_params[:password]
            @response[:success] = true
            @response[:access_token] = @panelist.access_token
          else
            failed_response(2, "Password not accepted by QB")
          end
        end

      else
        if @qb.is_valid_login(auth_params[:password])
          @panelist.password = auth_params[:password]
          @panelist.activated = true
          @response[:success] = true
          @response[:access_token] = @panelist.access_token
        else
          failed_response(3, "Password not accepted by QB")
        end
      end


    # Panelist not in SB
    else
      if @qb.is_valid_login(auth_params[:password])
        @panelist = Panelist.create(
          :email => auth_params[:email],
          :password => auth_params[:email],
          :last_accessed => Time.zone.now,
          :activated => true,
          :uid => @qb.get_panelist["uid"]
        )
        @response[:success] = true
        @response[:access_token] = @panelist.access_token
      else
        failed_response(4, "Password not accepted by QB")
      end
    end

    if @panelist != nil
      @panelist.last_accessed = Time.zone.now
      @panelist.save
      @panelist.generate_external_auth_code if @panelist.external_auth_code.blank?
    end

    render :json => @response
  end

  def logout
    # curl -H "Content-Type: application/json" -d '{"auth":{"access_token":"aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"}}' localhost:3000/api/auth/logout
    @response = {}
    @panelist = Panelist.find_by_access_token(auth_params[:access_token])

    if !@panelist.blank?
      @response[:success] = true
      @panelist.activated = false
      @panelist.access_token = nil
      @panelist.last_accessed = Time.zone.now
      @panelist.save
    else
      @response[:success] = false
    end

    render :json => @response
  end

  def facebook
    # curl -H "Content-Type: application/json" -d '{"auth":{"facebook_token":"CAALaZBzYemvQBALp..."}}' localhost:3000/api/auth/facebook
    @response = {}

    begin
      # Check FB with Graph API
      graph = Koala::Facebook::API.new(auth_params[:facebook_token])
      facebook_profile = graph.get_object("me")
      birthday = Date.strptime(facebook_profile["birthday"], "%m/%d/%Y")
      profile = {
        :u_firstname => facebook_profile["first_name"],
        :u_name => facebook_profile["last_name"],
        :u_email => facebook_profile["email"],
        :u_passwd => SecureRandom.urlsafe_base64(9),
        :m_0001 => birthday.day,
        :m_0002 => birthday.month,
        :m_0003 => birthday.year,
      }
      @qb = ::Questback.new(profile[:u_email])
      @panelist = Panelist.find_by_email(profile[:u_email])
      # Existing SB Panelist
      if @panelist != nil
        if @panelist.activated
          @response[:success] = true
          @response[:access_token] = @panelist.access_token

        else
          @panelist.activated = true
          @response[:success] = true
          @response[:access_token] = @panelist.access_token
        end

      # Panelist not in SB
      else
        panelist_data = @qb.get_panelist
        if !panelist_data.nil?
          @panelist = Panelist.create(
            :email => profile[:u_email],
            :password => SecureRandom.urlsafe_base64(9),
            :last_accessed => Time.zone.now,
            :activated => true,
            :uid => @qb.get_panelist["uid"]
          )
          @response[:success] = true
          @response[:access_token] = @panelist.access_token
        else
          panelist_registration = @qb.register_panelist(profile.stringify_keys, Setting.first.site_id)
          if panelist_registration.eql?(profile[:u_email])
            new_panelist_data = @qb.get_panelist
            @panelist = Panelist.create(
              :email => profile[:u_email],
              :password => profile[:u_passwd],
              :last_accessed => Time.zone.now,
              :activated => true,
              :uid => new_panelist_data["uid"]
            )
            @response[:success] = true
            @response[:access_token] = @panelist.access_token
          elsif panelist_registration.instance_of?(Hash) and panelist_registration.has_key?("error_message")
            failed_response(7, "Facebook QB Error: #{panelist_registration["error_message"]}")
          else
            failed_response(8, "Facebook QB Error")
          end
        end
      end
    # Facebook login failed
    rescue => e
      logger.debug(">>> FB_EXCEPTION : #{e}")
      failed_response(1, "Facebook Login failed")
    end

    if @panelist != nil
      @panelist.last_accessed = Time.zone.now
      @panelist.save
    end

    render :json => @response
  end

  def password_reset
    # curl -H "Content-Type: application/json" -d '{"auth":{"email":"test@something.com"}}' localhost:3000/api/auth/password_reset
    post = Panelist.reset_password(auth_params[:email])
    response =  post.code == "200" ? {success: true} : {success: false, error_message: "HTTP Error #{post.code}"}
    render :json => response
  end


  private
    def auth_params
      params.require(:auth).permit(
        :email,
        :password,
        :access_token,
        :facebook_token
        )
    end

    def log_response
      logger.debug(">>> OUTPUT: #{@response}")
    end

    def failed_response(error_code, error_message)
      @response[:success] = false
      @response[:error_code] = error_code
      @response[:error_message] = error_message
    end

end
