require 'questback'
class Api::RegisterController < ApplicationController

  include AppVersionHeader
  before_action :add_app_version_header

  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

  def create
    # curl -H "Content-Type: application/json" -d '{"register":{"u_firstname":"Joe", "u_name":"Bloggs", "m_0001":30, "m_0002":12, "m_0003":1980, "u_email":"test@test.com", "u_passwd":"password"}}' localhost:3000/api/register

    @response = {}
    qb = ::Questback.new(register_params[:u_email])

    panelist = Panelist.find_by_email(register_params[:u_email])
    if not panelist.blank?
      @response[:success] = false
      @response[:error_code] = 5
      @response[:error_message] = "User already registered"
      failed_response(5, "User already registered")

    else
      panelist_data = qb.get_panelist
      if not panelist_data.blank?
        @response[:success] = false
        @response[:error_code] = 6
        @response[:error_message] = "User already registered"
        failed_response(6, "User already registered")

      else
        panelist_registration = qb.register_panelist(register_params, Setting.first.site_id)
        if panelist_registration.instance_of? String
          panelist = Panelist.create(
            :email => register_params[:u_email],
            :password => register_params[:u_passwd],
            :last_accessed => Time.zone.now,
            :activated => true,
            :uid => qb.get_panelist["uid"]
          )
          @response[:success] = true
          @response[:access_token] = panelist.access_token

        elsif panelist_registration.instance_of?(Hash) and panelist_registration.has_key?("error_message")
          @response[:success] = false
          @response[:error_code] = 9
          @response[:error_message] = "Registration Error: #{panelist_registration["error_message"]}"
          failed_response(9, "Registration Error: #{panelist_registration["error_message"]}")

        else
          @response[:success] = false
          @response[:error_code] = 10
          @response[:error_message] = "Registration Error"
          failed_response(10, "Registration Error")
        end
      end
    end


    render :json => @response
  end

  private
    def register_params
      params.require(:register).permit(
        :u_firstname,
        :u_name,
        :u_email,
        :u_passwd,
        :m_0001,
        :m_0002,
        :m_0003
      )
    end

    def failed_response(error_code, error_message)
      @response[:success] = false
      @response[:error_code] = error_code
      @response[:error_message] = error_message
    end

end
