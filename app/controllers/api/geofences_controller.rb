class Api::GeofencesController < Api::ApiController
  include DataDescription

  def create
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" -d '{"geofence":{"latitude":53.8, "longitude":-1.5, "limit":200}}' localhost:3000/api/geofences

    @response = {:geofences => []}

    @geofences = []
    # Earth's circumference is about 40,075 km hence the 25000 range here (over half the circumference)
    geofence_regions = @panelist.geofences_without_completed_survey.near([geofence_params[:latitude],geofence_params[:longitude]], 10, units: :km).limit(geofence_params[:limit])

    radius_km = 0
    geofence_regions.each do |geofence|
      @response[:geofences] << create_geofence_description(geofence)
      distance = geofence.distance_to([geofence_params[:latitude],geofence_params[:longitude]])
      if distance > radius_km
        radius_km = distance
      end
    end
    @response[:radius] = (radius_km * 1000).to_i

    render :json => @response
  end

  def logs
    # curl -i -H  application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
    # -d '{ "logs":[{ "geofence_id":1, "logged_time": "1456149324", "location_timestamp": "1456149324",
    # "location_accuracy": "10.01", "location_latitude": 53.123123, "location_longitude": "-123.123123123" }] }'
    # localhost:3000/api/geofences/logs

    unless params[:logs].blank?
      geofence_logs = params[:logs]
      geofence_logs.each do |geofence_log|
        geofence_log[:panelist_id] = @panelist.id
        GeofenceLogsCreateJob.perform_later(geofence_log)
      end
    end
    head 200
  end


  private
    def geofence_params
      params.require(:geofence).permit(
        :latitude,
        :longitude,
        :limit
      )
    end

    def create_geofence_description(geofence)
      geofence_group        = geofence.geofence_group

      geofence_description  = geofence_data(geofence)

      if geofence_group.survey?
        geofence_description[:survey]         = survey_data(geofence_group.survey)
        geofence_description[:survey][:title] = geofence_group.survey_title unless geofence_group.survey_title.blank?
      elsif geofence_group.quick_poll?
        geofence_description[:quick_poll]           = geofence_group.quick_poll.poll_info
        geofence_description[:quick_poll]['title']  = geofence_group.survey_title unless geofence_group.survey_title.blank?
      end

      geofence_description[:dwell_time] = geofence_group.dwell_time if geofence_group.dwell?

      return geofence_description
    end

end
