class Api::VpnStudyCredentialsController < Api::ApiController
  include DataDescription

  before_action :initialize_response
  before_action :find_pending_or_active_vpn_study_credential, only: [:download]
  before_action :find_pending_vpn_study_credential, except: [:completed]
  before_action :find_active_vpn_study_credential, only: [:disconnect, :complete]

  def download
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" -d '{ "vpn_study_credential": {"vpn_study_id": 1, "type":"ios"} }' localhost:3000/api/vpn_study_credentials/download
    panelist_vpn_study_credential     = @pending_or_active_vpn_study_credential || create_pending_vpn_study_credential

    @response[:vpn_study_credential]  = vpn_study_credential_data(panelist_vpn_study_credential)
    render json: @response
  end

  def completed
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/vpn_study_credentials/completed
    @response[:completed_vpn_study_credentials] = @panelist.vpn_study_credentials.complete.map{ |vpn_study_credential| vpn_study_credential_data(vpn_study_credential) }
    render json: @response
  end

  def activate
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" -d '{ "vpn_study_credential": {"vpn_study_id": 1, "type":"ios"} }' localhost:3000/api/vpn_study_credentials/activate
    @response[:success] = !@pending_vpn_study_credential.nil? && @pending_vpn_study_credential.activate! ? true : false
    render json: @response
  end

  def disconnect
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" -d '{ "vpn_study_credential": {"vpn_study_id": 1, "type":"ios"} }' localhost:3000/api/vpn_study_credentials/disconnect
    respond_and_process_vpn_study_credential(type: 'cancel')
    render json: @response
  end

  def complete
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" -d '{ "vpn_study_credential": {"vpn_study_id": 1, "type":"ios"} }' localhost:3000/api/vpn_study_credentials/complete
    respond_and_process_vpn_study_credential(type: 'complete')
    render json: @response
  end

  private

  def initialize_response
    @response = {}
  end

  def vpn_study_id_param
    params[:vpn_study_credential][:vpn_study_id]
  end

  def find_pending_vpn_study_credential
    @pending_vpn_study_credential = @panelist.vpn_study_credentials
                                      .pending.where(vpn_study_id: vpn_study_id_param).last
  end

  def find_active_vpn_study_credential
    @active_vpn_study_credential = @panelist.vpn_study_credentials
                                    .active.where(vpn_study_id: vpn_study_id_param).last
  end

  def find_pending_or_active_vpn_study_credential
    @pending_or_active_vpn_study_credential = @panelist.vpn_study_credentials
                                                .pending_or_active.where(vpn_study_id: vpn_study_id_param).last
  end

  def create_pending_vpn_study_credential
    @panelist.create_vpn_study_credential(params[:vpn_study_credential])
  end

  def vpn_study_disconnect_delay
    Setting.first.vpn_study_disconnect_delay || 3
  end

  def respond_and_process_vpn_study_credential(type:)
    @response[:success] = @active_vpn_study_credential.nil? ? false : true
    @active_vpn_study_credential.delay(queue: :default,
                                       run_at: vpn_study_disconnect_delay.seconds.from_now).send("#{type}!") unless @active_vpn_study_credential.nil?
  end


end
