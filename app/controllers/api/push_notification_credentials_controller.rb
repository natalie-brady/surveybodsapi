class Api::PushNotificationCredentialsController < Api::ApiController
  include Api::PushNotificationCredentialsHelper

  def create
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" -d '{"push_notification_credential": {"notification_service":"apn", "token":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"}}' localhost:3000/api/push_notification_credentials

    # Remove all existing push_notification_credentials per notification_service
    remove_duplicate_push_notification_credentials
    @panelist.remove_existing_push_notification_credentials(push_notification_credential_params[:notification_service])

    @panelist.push_notification_credentials.find_or_create_by(
      token:                push_notification_credential_params[:token],
      notification_service: PushNotificationCredential.notification_services[push_notification_credential_params[:notification_service]]
    )

    @response = {}
    @response[:success] = true
    render :json => @response
  end

  private
    def push_notification_credential_params
      params.require(:push_notification_credential).permit(
        :notification_service,
        :token
      )
    end

    def remove_duplicate_push_notification_credentials
      token                         = push_notification_credential_params[:token]
      notification_service          = format_notification_service(push_notification_credential_params[:notification_service])
      push_notification_credentials = PushNotificationCredential.send(notification_service.to_sym).where(token: token)

      push_notification_credentials.destroy_all
    end
end
