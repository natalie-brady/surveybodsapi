class Api::DeviceMetadataController < Api::ApiController
  before_action :initialise_response

  def create
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"
    # -d '{ "device_metadata":{"network_provider":"test network", "device_model":"iPhone 7",
    #       "device_manufacturer":"Apple", "more_info":{"os":"ios10"}}}'
    @panelist.device_metadata.create(device_metadata_params)
    @response[:success] = true
    render json: @response
  end

  private
  def device_metadata_params
    params.require(:device_metadatum).permit(
      :network_provider,
      :device_model,
      :device_manufacturer,
      more_info: more_info_params
    )
  end

  def more_info_params
    params[:device_metadatum][:more_info].keys if params[:device_metadatum][:more_info].present?
  end

  def initialise_response
    @response = {}
  end
end
