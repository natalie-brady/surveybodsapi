class Api::ProfileController < Api::ApiController

  def index
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/profile

    @qb = ::Questback.new(@panelist.email)
    qb_panelist = @qb.get_panelist
    @response = {}
    profile = {}
    profile[:u_email] = qb_panelist["u_email"]
    profile[:u_firstname] = qb_panelist["u_firstname"]
    profile[:u_name] = qb_panelist["u_name"]
    profile[:bonus_points] = @qb.get_bonus_points
    profile[:md_strikes] = @qb.get_panelist_masterdata("md_strikes").to_i
    profile[:tickets] = @qb.get_panelist_masterdata("md_9809").to_i
    profile[:m_county] = @qb.get_panelist_masterdata("m_county").to_i
    profile[:m_region] = @qb.get_panelist_masterdata("m_region").to_i
    profile[:u_zip] = qb_panelist["u_zip"]
    profile[:profile_strength] = get_profile_strength
    profile[:profile_variables] = get_profile_variables
    profile[:membership] = get_profile_membership
    profile[:redeem_url] = @panelist.get_redeem_url
    if @panelist.avatar_file_name != nil
      profile[:avatar_url] = @panelist.avatar.url
    else
      profile[:avatar_url] = ""
    end

    @response[:profile] = profile

    render :json => @response
  end

  def change
    # curl -i -X PATCH -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" -d '{"changes":{u_zip":"AB123CD"}' localhost:3000/api/profile/change
    # TODO Update Panelist data in SB to reflect changes
    @response = {}
    begin
      qb = ::Questback.new(@panelist.email)
      qb.change_panelist(params[:changes])
      @response[:success] = true
    rescue
      @response[:success] = false
    end

    render :json => @response
  end

  def avatar
    @response = {}

    if params[:avatar]
      data = StringIO.new(Base64.decode64(params[:avatar][:data]))
      data.class.class_eval { attr_accessor :original_filename, :content_type }
      data.original_filename = params[:avatar][:filename]
      data.content_type = params[:avatar][:content_type]

      @panelist.avatar = data
      @panelist.save
    end
    @response[:avatar_url] = @panelist.avatar.url

    render :json => @response
  end

  def refer_a_friend
    # curl -H "Content-Type: application/json" -d '{"email":"test@something.com", "name": "Name"}' localhost:3000/api/profile/refer_a_friend
    refer_a_friend_params = {
      refer_email: params[:email],
      name: params[:name]
    }

    post      = @panelist.refer_a_friend(refer_a_friend_params)
    @response = post.nil? ?  failed_response(14, "Refer a Friend was unsuccessful.") : { success: true }
    render :json => @response
  end


  private

    def get_profile_strength
      profile_strength_attributes = ["md_profile_survey", "md_technology_survey", "md_media_consumption", "md_shopping_survey"]
      profile_strength = 0
      profile_strength_attributes.each do |profile_attribute|
        profile_strength += @qb.get_panelist_masterdata(profile_attribute) == "1" ? 1 : 0
      end
      100 / profile_strength_attributes.size * profile_strength
    end

    def get_profile_variables
      profile_variables = {}
      ProfileVariable.all.each do |profile_variable|
        profile_variables[profile_variable.name] = profile_variable.value
      end
      profile_variables
    end

    def get_profile_membership
      total_completed = @qb.get_panelist_masterdata("m_total_completed").to_i
      if total_completed >= 30
        return "diamond"
      elsif total_completed >= 20
        return "platinum"
      elsif total_completed >= 10
        return "gold"
      elsif total_completed >= 5
        return "silver"
      elsif total_completed >= 2
        return "bronze"
      else
        return ""
      end
    end

    def failed_response(code, message)
      {
        success: false,
        error_code: code,
        error_message: message
      }
    end

end
