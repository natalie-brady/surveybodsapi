class Api::QuickPollsController < Api::ApiController
  include Api::QuickPollsHelper

  before_action :check_already_submitted, only: [:create]
  before_action :get_answers, only: [:create]
  before_action :verify_quick_poll_response, only: [:results]

  def index
   @response = @panelist.available_quick_polls
   render :json => @response
  end

  def show
    @response = QuickPoll.find(params[:id]).get_poll_info
    render :json => @response
  end

  def create
    answer_ids = @answers.map{ |answer| answer[:id] }
    if answer_ids.uniq == answer_ids #prevent duplicated answers

      quick_poll_responses = @answers.map { |quick_poll_choice| @panelist.quick_poll_responses.build(quick_poll_choice_id: quick_poll_choice[:id],
                                                                                                     quick_poll_choice_text: quick_poll_choice[:text]) }

      if !quick_poll_responses.map(&:valid?).include? false
        quick_poll_responses.each(&:save)
        quick_poll          = quick_poll_responses.first.quick_poll
        total_bonus_points  = quick_poll.grant_bonus_points(@panelist.email)
        total_tickets       = quick_poll.grant_tickets(@panelist.email)
        @response = {
                      'success': true,
                      'points_awarded': quick_poll.points,
                      'total_bonus_points': total_bonus_points,
                      'tickets_awarded': quick_poll.tickets,
                      'total_tickets': total_tickets
                    }
        QuickPollStatsJob.perform_later(quick_poll_id: quick_poll.id)
      else
        @response = {'success': false, 'error_message': 'Answer has already been submitted', 'error_code': 12}
      end

    else
      @response = {'success': false, 'error_message': 'Duplicated answers found', 'error_code': 11}
    end

    render :json => @response
  end

  def completed
    @response = {
      'quick_polls' => @panelist.completed_quick_polls.map do |quick_poll|
        quick_poll.poll_info(true)
      end
    }

    render :json => @response
  end

  def results
    @response = quick_poll_result_info
    @response[:success] = true
    render json: @response
  end

  private

  def check_already_submitted
    completed_quick_polls = @panelist.completed_quick_polls
    params[:responses].each do |quick_poll_response|
      quick_poll = QuickPoll.find(quick_poll_response[:id])
      if completed_quick_polls.include?(quick_poll) && quick_poll.is_activity_or_geofence_group_single?
        @response = {'success': false, 'error_message': 'QuickPoll has already been submitted', 'error_code': 13}
        render :json => @response and return
      end
    end
  end

  def get_answers
    @answers = []
    params[:responses].each do |quick_poll|
      quick_poll[:questions].each do |question|
        question[:answers].each do |answer|
          @answers << answer
        end
      end
    end
  end

  def verify_quick_poll_response
    @quick_poll = QuickPoll.find_by id: params[:id]
    @response = if @quick_poll
                  {
                    success: false,
                    error: "Panelist not yet responded to this QuickPoll."
                  } if !@panelist.completed_quick_polls.include?(@quick_poll)
                else
                  { success: false, error: "QuickPoll not found." }
                end
    render json: @response if @response
  end

end
