class Api::StatisticsController < ApplicationController
  include Api::StatisticsHelper

  before_action :verify_user_email
  before_action :initialize_response

  def panelists
    # curl -H "Content-Type: application/json" -H "email: mike@terzza.com" localhost:3000/api/statistics/panelists
    @response[:panel_stats] = panelist_statistics_data

    render json: @response
  end

  private

  def initialize_response
    @response = {}
  end

  def invalid_user_email_response
    @response = { error: { message: 'Invalid user. You cannot access this without using a registered email address.' } }

    render json: @response
  end

  def verify_user_email
    email = request.headers[:email]
    user  = User.find_by email: email

    invalid_user_email_response if user.blank?
  end
end
