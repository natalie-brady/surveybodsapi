class Api::FaqsController < Api::ApiController

  def index
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/faqs

    @response = {}
    faqs = Faq.all.map {|faq| {:question => faq.question, :answer => faq.answer} }
    @response[:faqs] = faqs
    render :json => @response
  end

end
