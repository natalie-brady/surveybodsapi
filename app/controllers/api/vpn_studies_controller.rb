class Api::VpnStudiesController < Api::ApiController

  before_action :initialize_response
  before_action :find_vpn_study, only: [:is_active]

  def index
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/vpn_studies
    @response[:vpn_studies] = @panelist.has_active_vpn_study? ? {} : @panelist.available_vpn_studies.map{ |vpn_study| vpn_study.send(:attributes_with_values) }
    render json: @response
  end

  def active
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/vpn_studies/active
    @response[:active_vpn_study] = @panelist.has_active_vpn_study? ? @panelist.active_vpn_study : {}
    render json: @response
  end

  def completed
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/vpn_studies/completed
    @response[:completed_vpn_studies] = @panelist.completed_vpn_studies
    render json: @response
  end

  def cancelled
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/vpn_studies/cancelled
    @response[:cancelled_vpn_studies] = @panelist.cancelled_vpn_studies
    render json: @response
  end

  def connected
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/vpn_studies/connected
    @response[:connected] = is_request_ip_address_valid?
    render json: @response
  end

  def is_active
    response_params = @vpn_study.nil? ? { active: false } : { active: @vpn_study.active? }
    response_params.merge!(message: 'VPN Study not found') if @vpn_study.nil?
    @response[:vpn_study].merge!(response_params)
    render json: @response
  end

  private

  def initialize_response
    @response = {}
  end

  def is_request_ip_address_valid?
    setting = Setting.first
    setting.allowed_ip_addresses.include?(request.remote_ip) ? true : false
  end

  def find_vpn_study
    @vpn_study  = VpnStudy.find_by id: params[:id]
    @response   = { vpn_study: { success: true } }
  end

end
