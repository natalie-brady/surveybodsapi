require 'questback'

class Api::ApiController < ApplicationController

  include AppVersionHeader
  before_action :add_app_version_header

  before_action :authenticate_access_token
  after_action :update_last_accessed
  after_action :log_response
  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

  private

    def authenticate_access_token
      access_token = request.headers[:token]
      @panelist = Panelist.find_by_access_token(access_token)
      if @panelist.blank? or (not @panelist.activated)
        head(403)
      end
    end

    def update_last_accessed
      if !@panelist.blank?
        logger.debug(">>> LAST ACCESSED")
        @panelist.last_accessed = Time.zone.now
        @panelist.save
      end
    end

    def log_response
      logger.debug(">>> OUTPUT: #{@response}")
    end

end
