class Api::SurveysController < Api::ApiController
  before_action :initialize_questback, :initialize_response
  before_action :find_survey, only: [:is_active]

  include DataDescription

  def index
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/surveys

    qb_surveys    = @qb.get_surveys
    qb_survey_ids = qb_surveys.map{ |qb_survey| qb_survey["id"] }
    surveys       = Survey.active_and_questback_active.ordered.where(survey_id: qb_survey_ids)
    surveys       = surveys.map{ |survey| survey_data(survey, completed: false, qb_surveys: qb_surveys) }

    @response[:surveys] = surveys
    render :json => @response
  end

  def completed
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/surveys/completed

    qb_completed_surveys    = @qb.get_completed_surveys
    qb_completed_survey_ids = qb_completed_surveys.map{ |qb_completed_survey| qb_completed_survey["surveyId"] }
    completed_surveys       = Survey.ordered.where(survey_id: qb_completed_survey_ids)
    completed_surveys       = completed_surveys.map{ |completed_survey| survey_data(completed_survey, completed: true) }

    @response[:completed_surveys] = completed_surveys
    render :json => @response
  end

  def profile
    # curl -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/surveys/profile

    survey_list_ids = @qb.get_completed_surveys.map{ |survey| survey['surveyId'] }

    profile_surveys = []
    ProfileSurvey.all.each do |profile_survey|
      unless survey_list_ids.include?(profile_survey.survey.survey_id)
        profile_surveys << profile_survey_data(profile_survey)
      end
    end

    @response[:profile_surveys] = profile_surveys
    render :json => @response
  end

  def is_active
    @response[:survey].merge!(active: false, message: 'Survey not found.') if @survey.nil?
    @response[:survey].merge!(active: @survey.is_active?) if @survey
    render json: @response
  end

  private

  def initialize_questback
    @qb = ::Questback.new(@panelist.email)
  end

  def initialize_response
    @response = {}
  end

  def find_survey
    @survey   =  Survey.find_by_survey_id params[:id]
    @response = { survey: { success: true } }
  end

end
