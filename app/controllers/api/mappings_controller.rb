class Api::MappingsController < Api::ApiController

  def masterdata
    # curl -i -H "Content-Type: application/json" -H "Token: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa" localhost:3000/api/mappings/masterdata

    targets = [
      "m_county",
      "m_region",
      "m_0001", # Day of Birth
      "m_0002", # Month of Birth
      "m_0003"  # Year of Birth
    ]
    @response = {:mappings => {}}
    MasterDataDescription.where(:varname => targets).each do |mdd|
      @response[:mappings][mdd.varname] = mdd.categories
    end

    render :json => @response
  end

end