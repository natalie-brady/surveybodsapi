class PanelistSyncLogsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_delayed_job, only: [:index]

  def index
    @panelist_sync_logs     = PanelistSyncLog.all.paginate(page: params[:page])
    @sync_in_progress       = @delayed_job.size != 0
    @sync_status            = @sync_in_progress ? @delayed_job.first.status_text : ""
    @last_panelist_sync_log = PanelistSyncLog.unscoped.last
  end

  private
  def find_delayed_job
    @delayed_job = Delayed::Job.where("handler like ?", "%#{PanelistsSyncLastAccessedJob.to_s}%")
  end
end
