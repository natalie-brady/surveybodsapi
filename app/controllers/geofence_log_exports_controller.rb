class GeofenceLogExportsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_delayed_job, only: [:index]
  before_action :find_geofence_log_export, except: [:index]

  def index
    @geofence_log_exports = GeofenceLogExport.paginate(page: params[:page])
    @export_in_progress   = @delayed_job.size != 0
    @export_status        = @export_in_progress ? @delayed_job.first.status_text : ""
  end

  def destroy
    @geofence_log_exports.destroy_all if @geofence_log_exports
    @geofence_log_export.destroy if @geofence_log_export
    redirect_to geofence_log_exports_path, notice: "You have successfully deleted the export file."
  end

  def download
    send_file @geofence_log_export.file_csv.path,
              filename: @geofence_log_export.file_csv.original_filename,
              type: 'application/octet-stream',
              disposition: 'attachment',
              x_sendfile: true
  end

  private

  def find_geofence_log_export
    @geofence_log_exports = GeofenceLogExport.all if params[:id].eql?('all')
    @geofence_log_export  = GeofenceLogExport.find_by id: params[:id]
  end

  def find_delayed_job
    @delayed_job = Delayed::Job.where("handler like ?", "%#{GeofenceLogExportJob.to_s}%")
  end
end
