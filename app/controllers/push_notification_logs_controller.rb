class PushNotificationLogsController < ApplicationController
  before_action :authenticate_user!

  def index
    @push_notification_logs = PushNotificationLog.all.order("created_at DESC")
  end
end
