class MasterDataDescriptionsController < ApplicationController

  before_action :authenticate_user!
  before_action :find_delayed_job, only: [:index]

  def index
    @master_data_descriptions = MasterDataDescription.all
    @reload_in_progress       = @delayed_job.size != 0
    @reload_status            = @reload_in_progress ? @delayed_job.first.status_text : ""
  end

  def show
    @master_data_description = MasterDataDescription.find(params[:id])
  end

  def reload
    MasterDataReloadJob.perform_later
    @master_data_descriptions = MasterDataDescription.all
    respond_to do |format|
      format.js
    end
  end

  private

  def find_delayed_job
    @delayed_job = Delayed::Job.where("handler like ?", "%#{MasterDataReloadJob.to_s}%")
  end

end
