class UsageStudyAccessLogsController < ApplicationController
  include CsvExport

  before_action :authenticate_user!
  before_action :initialize_export_params, only: [:export]
  before_action :merge_export_params, only: [:export]
  before_action :find_ip_client, only: [:export]
  before_action :find_usage_study_access_log, only: [:show]

  def index
  end

  def show
  end

  def export
    if valid_date_params?
      @export_params[:start_date]  = start_date.to_s
      @export_params[:end_date]    = end_date.to_s
    end

    @export_params[:ip_client]   = @ip_client if @ip_client
    LogExportsJob.perform_later(@export_params)
    redirect_to log_exports_path(export_type: @export_params[:export_type]), notice: "Your export will be ready soon. Please refresh this page to reflect your export."
  end

  private

  def find_ip_client
    if params[:user_id].present?
      usage_study_user  = UsageStudyUser.find_by id: params[:user_id]
      @ip_client        = usage_study_user.ip_client
    end
  end

  def find_usage_study_access_log
    @usage_study_access_log = UsageStudyAccessLog.find_by id: params[:id]
  end

  def initialize_export_params
    @export_params = {
      user_id:      current_user.id,
      export_type: 'usage_study_access_log'
    }
  end

  def merge_export_params
    valid_params = sanitize_params
    @export_params = @export_params.merge(valid_params)
  end

  def sanitize_params
    params[:access_log].reject{ |key, value| value.blank? }
  end

end
