class FaqsController < ApplicationController

  before_action :authenticate_user!

  def index
    @faqs = Faq.all
  end

  def show
    @faq = Faq.find(params[:id])
  end

  def new
    @faq = Faq.new
  end

  def create
    @faq = Faq.new(faq_params)
    if @faq.save
      redirect_to faq_path(@faq)
    else
      render 'new'
    end
  end

  def edit
    @faq = Faq.find(params[:id])
  end

  def update
    @faq = Faq.find(params[:id])
    if @faq.update(faq_params)
      redirect_to faq_path(@faq)
    else
      render 'edit'
    end
  end

  def destroy
    @faq = Faq.find(params[:id])
    if @faq.destroy
      redirect_to faqs_path
    else
      redirect_to faq_path(@faq)
    end
  end


  private
    def faq_params
      params.require(:faq).permit(
        :question,
        :answer
      )
    end

end
