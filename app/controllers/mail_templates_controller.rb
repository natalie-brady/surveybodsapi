class MailTemplatesController < ApplicationController

  before_action :authenticate_user!

  def reload
    MailTemplatesReloadJob.perform_later
    respond_to do |format|
      format.js
    end
  end

end
