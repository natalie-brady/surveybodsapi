class DecryptHostsController < ApplicationController
  before_action :authenticate_user!

  def index
    @decrypt_hosts = DecryptHost.all.paginate(page: params[:page])
  end
end
