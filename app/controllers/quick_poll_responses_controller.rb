class QuickPollResponsesController < ApplicationController
  include CsvExport

  before_action :authenticate_user!
  before_action :get_quick_poll, only: [:export]
  before_action :get_quick_poll_responses, only: [:export]

  def index
    @quick_poll_responses = QuickPollResponse.all.ordered.paginate(page: params[:page])
  end

  def destroy
    QuickPollResponse.find(params[:id]).destroy
    redirect_to quick_poll_responses_path
  end

  def export
    export_params = {
      records:  @quick_poll_responses_export,
      header:   export_headers
    }

    send_data generate_csv_file(export_params), :filename => export_filename, :type => "text/plain"
  end

  private
  def get_quick_poll
    @quick_poll = QuickPoll.find_by_id(params[:quick_poll_id]) if params[:quick_poll_id].present?
  end

  def get_quick_poll_responses
    @quick_poll_responses_export  = @quick_poll.nil? ? QuickPollResponse.all.order(:id) : @quick_poll.quick_poll_responses
  end

  def export_headers
    ["Panelist Email","Panelist UID", "QuickPollId", "QuickPollTitle", "QuickPollQuestionId", "QuickPollChoiceId", "ChoiceMapping", "Text", "Timestamp"]
  end

  def export_filename
    filename = "quick_poll_responses_export.csv"
    filename = "#{@quick_poll.title.parameterize('_')}_#{filename}" if @quick_poll
    filename
  end
end
