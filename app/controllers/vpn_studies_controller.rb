class VpnStudiesController < ApplicationController
  autocomplete :vpn_study, :name, extra_data: [:id], full: true

  before_action :authenticate_user!
  before_action :find_vpn_study, except: [:index, :create, :new, :autocomplete_vpn_study_name]

  def index
    @vpn_studies = VpnStudy.search(params[:search]).ordered

    respond_to do |format|
      format.html
      format.js

      @vpn_study_active = VpnStudy.where(active: true)
      @vpn_study_inactive = VpnStudy.where(active: false)
    end
  end

  def create
    @vpn_study  = VpnStudy.new vpn_study_params
    @vpn_study.save ? redirect_to(vpn_studies_path, notice: "You have successfully created #{@vpn_study.name}!")  : render('new')
  end

  def new
    @vpn_study    = VpnStudy.new
    @bart_setting = @vpn_study.build_bart_setting
  end

  def show
  end

  def edit
  end

  def update
    @vpn_study.update_attributes(vpn_study_params) ? redirect_to(vpn_study_path(@vpn_study), notice: "You have succesfully updated #{@vpn_study.name}!") : render("edit")
  end

  # def destroy
  #   vpn_path = @vpn_study.destroy ? vpn_studies_path  : @vpn_study
  #   redirect_to vpn_path, notice: "You have successfully deleted a Vpn Study."
  # end

  def send_notifications
    VpnStudySendNotificationsJob.perform_later(vpn_study_id: @vpn_study.id, notifier_type: 'connect')
    redirect_to vpn_studies_path, notice: "Your push notification will be sent shortly."
  end

  def reload_data
    @vpn_study_activity = @vpn_study.activity_report
    respond_to do |format|
      format.js
    end
  end

  def deactivate
    @vpn_study.deactivate!
    VpnStudySendNotificationsJob.perform_later(vpn_study_id: @vpn_study.id, notifier_type: 'disconnect')
    redirect_to vpn_studies_path, notice: "You have successfully ended #{@vpn_study.name}. All participants will be disconnected automatically."
  end
  
  private

  def vpn_study_params
    params.require(:vpn_study).permit(
      :name,
      :description,
      :questback_group_id,
      :last_start_time,
      :end_time,
      :bonus_points,
      :tickets,
      :active,
      bart_setting_attributes: [:id,
                                :start_activation_period,
                                :end_activation_period,
                                :frequency_checker,
                                :exclusion_setting,
                                :exclude_all,
                                :action_csv_file,
                                :email_template,
                                :email_subject]
    )
  end

  def find_vpn_study
    @vpn_study    = VpnStudy.find params[:id]
    @bart_setting = @vpn_study.bart_setting
  end

end
