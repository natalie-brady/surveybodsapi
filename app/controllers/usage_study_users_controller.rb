class UsageStudyUsersController < ApplicationController
  include CsvExport

  before_action :authenticate_user!
  before_action :find_usage_study_user, only: [:access_logs]

  def index
    @usage_study_users = UsageStudyUser.load_associations.ordered.paginate(page: params[:page])

    @usage_active = UsageStudyUser.where(active: true)
    @usage_inactive = UsageStudyUser.where(active: false)
  end

  def access_logs
    @usage_study_access_logs = UsageStudyAccessLog.by_ip_client(@usage_study_user.ip_client).ordered.paginate(page: params[:page])
  end

  def export
    filename      = 'usage_study_users.csv'
    export_params = {
      records: UsageStudyUser.all,
      header: ['IP Client', 'Username', 'Active', 'Panelist Email']
    }

    send_data generate_csv_file(export_params), filename: filename, type: 'text/plain'
  end

  private

  def find_usage_study_user
    @usage_study_user = UsageStudyUser.find params[:id]
  end
end
