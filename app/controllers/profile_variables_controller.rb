class ProfileVariablesController < ApplicationController

  before_action :authenticate_user!
  before_action :find_profile_variable, only: [:show, :edit, :update, :destroy]

  def index
    @profile_variables = ProfileVariable.all
  end

  def show
  end

  def new
    @profile_variable = ProfileVariable.new
  end

  def create
    @profile_variable = ProfileVariable.new(profile_variable_paramas)
    if @profile_variable.save
      redirect_to profile_variable_path(@profile_variable)
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @profile_variable.update(profile_variable_paramas)
      redirect_to profile_variable_path
    else
      render 'edit'
    end
  end

  def destroy
    if @profile_variable.destroy
      redirect_to profile_variables_path
    else
      redirect_to profile_variable_path(@profile_variable)
    end
  end

  private
    def profile_variable_paramas
      params.require(:profile_variable).permit(
        :name,
        :description,
        :value
      )
    end

    def find_profile_variable
      @profile_variable = ProfileVariable.find(params[:id])
    end
end
