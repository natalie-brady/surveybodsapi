class QuickPollStatsController < ApplicationController

  before_action :authenticate_user!
  before_action :find_delayed_job, only: [:index]

  def index
    @quick_polls      = QuickPoll.all
    @update_progress  = @delayed_job.size != 0

    respond_to do |format|
      format.html
      format.js
    end
  end

  def update_stats
    QuickPollStatsJob.perform_later
    redirect_to quick_poll_stats_path, notice: "Please refresh the page after some minutes to reflect the update!"
  end

  private

  def find_delayed_job
    @delayed_job = Delayed::Job.where("handler like ?", "%#{QuickPollStatsJob.to_s}%")
  end

end
