class GeofenceUploadsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_geofence_collection, only: [:create]
  before_action :get_all_geofence_collections, only: [:new, :create]

  def new
    @geofence_upload      = GeofenceUpload.new
  end

  def create
    @geofence_upload    = GeofenceUpload.new(geofence_upload_params)
    @geofence_upload.save ? redirect_to(geofence_collection_path(@geofence_collection), notice: "You've successfully added a geofence data to #{@geofence_collection.to_s}") : render(:new)
  end

  def export_template
    send_file Rails.root.join('public/templates', 'GeofenceUploadDataTemplate.csv'),
              filename: 'GeofenceUploadDataTemplate.csv',
              disposition: 'attachment',
              x_sendfile: true
  end

  private

  def geofence_upload_params
    params.require(:geofence_upload).permit(
      :name,
      :latitude,
      :longitude,
      :radius,
      :geofence_collection_id
    )
  end

  def find_geofence_collection
    @geofence_collection = GeofenceCollection.find(params[:geofence_upload][:geofence_collection_id])
  end

  def get_all_geofence_collections
    @geofence_collections = GeofenceCollection.ordered_by_name
  end
end
