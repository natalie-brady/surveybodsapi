class SettingsController < ApplicationController

  before_action :authenticate_user!
  before_action :find_delayed_jobs, only: [:show]

  def index
    redirect_to Setting.first
  end

  def show
    @setting                    = Setting.find(params[:id])
    @reload_in_progress         = @mail_templates_delayed_job.size != 0
    @reload_survey_in_progress  = @survey_delayed_job.size != 0
    @reload_survey_status       = @reload_survey_in_progress ? @survey_delayed_job.first.status_text : ""
  end

  def edit
    @setting = Setting.find(params[:id])
  end

  def update
    @setting = Setting.find(params[:id])
    if @setting.update(settings_params)
      redirect_to @setting
    else
      render 'edit'
    end
  end

  private
    def settings_params
      params.require(:setting).permit(
        :qb_hostname,
        :qb_api_username,
        :qb_api_password,
        :auth_timeout_seconds,
        :site_id,
        :questback_external_service_id,
        :mail_template_id,
        :survey_category_id,
        :apn_cert,
        :gcm_key,
        :apn_passphrase,
        :android_minimum_version,
        :ios_minimum_version,
        :refer_promotion_id,
        :pci,
        :vpn_hostname,
        :vpn_study_disconnect_delay,
        :vpn_study_ip_addresses
      )
    end

    def find_delayed_jobs
      @survey_delayed_job         = Delayed::Job.where("handler like ?", "%#{SurveysReloadJob.to_s}%")
      @mail_templates_delayed_job = Delayed::Job.where("handler like ?", "%#{MailTemplatesReloadJob.to_s}%")
    end

end
