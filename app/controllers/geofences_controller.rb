class GeofencesController < ApplicationController
  include CsvExport

  before_action :authenticate_user!
  before_action :find_geofence, only: [:show,:edit,:update, :destroy]

  def index
    @geofences = Geofence.search(params[:search]).ordered.paginate(page: params[:page])
    respond_to do |format|
    format.html
    format.js
  end
  end

  def show
    @map_data = get_map_data(@geofence, Geofence.where("id != ?", params[:id]), false)
  end

  def new
    @geofence           = Geofence.new
    @geofence.latitude  = 53.79172136223823
    @geofence.longitude = -1.5513714951065367
    @geofence.radius    = 50
    @map_data           = get_map_data(@geofence, Geofence.all, true)
    @geofence_group     = GeofenceGroup.find_by(id: params[:geofence_group_id])
  end

  def create
    @geofence = Geofence.new(geofence_params)
    if @geofence.save
      redirect_to geofence_path(@geofence)
    else
      render 'new'
    end
  end

  def edit
    @map_data = get_map_data(@geofence, Geofence.where("id != ?", params[:id]), true)
  end

  def update
    if @geofence.update(geofence_params)
      redirect_to geofence_path
    else
      render 'edit'
    end
  end

  def destroy
    @geofence.destroy
    redirect_to geofences_path
  end

  def export
    export_params = {
      records:  Geofence.all,
      header:   Geofence.csv_attributes
    }
    send_data generate_csv_file(export_params), :filename => "geofence_export.csv", :type => "text/plain"
  end

  def info
  end


  private
    def geofence_params
      params.require(:geofence).permit(
        :name,
        :description,
        :latitude,
        :longitude,
        :radius,
        :delay,
        :geofence_group_id,
        :dwell_time
      )
    end

    def get_map_data(active_fence, background_fences, editable)
      background_fences_data = []
      background_fences.each do |fence|
        background_fences_data << {
          :icon => ActionController::Base.helpers.asset_path("rb_icon.png"),
          :latitude => fence.latitude,
          :longitude => fence.longitude,
          :radius => fence.radius,
          :active => false,
          :editable => false,
          :name => fence.name,
          :description => fence.description,
          :link => geofence_path(fence)
        }
      end

      active_fence_data = active_fence == nil ? nil : {
        :icon => ActionController::Base.helpers.asset_path("rb_icon_active.png"),
        :latitude => active_fence.latitude,
        :longitude => active_fence.longitude,
        :radius => active_fence.radius,
        :active => true,
        :editable => editable,
      }
      return {
        :active_fence => active_fence_data,
        :background_fences => background_fences_data
      }
    end

    def find_geofence
      @geofence = Geofence.find(params[:id])
    end

end
