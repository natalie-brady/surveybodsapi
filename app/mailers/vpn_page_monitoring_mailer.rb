class VpnPageMonitoringMailer < ApplicationMailer

  def speed_test_performance_alert
    vpn_page_monitoring = VpnPageMonitoring.last
    emails = vpn_page_monitoring.email_alert_addresses.split(',')
    mail(to: emails, subject: "VPN Performance Alert - #{vpn_page_monitoring.error_alert_count} errors were reported")
  end
end
