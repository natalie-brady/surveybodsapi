class ApplicationMailer < ActionMailer::Base
  default from: "Researchbods #{Rails.env.titleize} <no-reply@researchbods.com>"
  layout 'mailer'
end
