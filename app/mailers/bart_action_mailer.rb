class BartActionMailer < ApplicationMailer

  def send_survey_to_panelist(bart_setting_id:, panelist_email:, survey_url:)
    @bart_setting = BartSetting.find_by_id bart_setting_id
    @body         = if @bart_setting.email_template?
                      text        = File.read @bart_setting.email_template.path
                      valid_text  = text.encode("UTF-16be", :invalid=>:replace, :replace=>"?").encode('UTF-8')
                      valid_text.gsub(/insert_survey_url_here/, survey_url)
                    else
                      'default'
                    end
    email_subject = @bart_setting.email_subject.blank? ? "New Survey" : @bart_setting.email_subject
    mail(to: panelist_email, subject: email_subject)
  end

end

