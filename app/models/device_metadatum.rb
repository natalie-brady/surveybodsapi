class DeviceMetadatum < ActiveRecord::Base
  include CsvBuilder
  include Searchable

  serialize :more_info, Hash

  belongs_to :panelist

  scope :ordered, -> { order(created_at: :desc) }

  def panelist_email
    panelist.to_s
  end

  def self.except_attributes
    super.unshift(['id', 'panelist_id']).flatten
  end

  def self.csv_attributes
    super.unshift('panelist_email')
  end

  def self.query_by_parameters(params)
    search(params)
  end
end
