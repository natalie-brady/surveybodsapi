class GeofenceGroup < ActiveRecord::Base
  include CsvBuilder
  include Searchable

  belongs_to :survey
  belongs_to :quick_poll
  belongs_to :questback_group
  belongs_to :geofence_collection

  has_many :geofences, dependent: :destroy
  has_many :geofence_logs, through: :geofences

  enum geofence_type: [:entry, :exit, :dwell]
  enum action:        [:survey, :quick_poll, :passive]
  enum status:        [:inactive, :active]
  enum trigger_type:  [:single, :multiple, :both]

  validates :dwell_time, presence: true, if: "dwell?"
  validates :name, :message, :description, presence: true
  validates :survey_title, presence: true, unless: "passive?"

  after_save :set_survey_and_quick_poll_id_to_null
  after_update :remove_existing_geofences, if: :geofence_collection_id_changed?
  after_save :create_geofences_using_geofence_uploads, if: :has_no_geofences?

    def self.search(search)
  where("name LIKE ?", "%#{search}%") 
  end

  def to_s
    name
  end

  def has_no_geofences?
    geofences.blank?
  end

  def remove_existing_geofences
    geofences.destroy_all if geofences.any?
  end

  def activate
    update_attribute(:status, :active)
  end

  def deactivate
    update_attribute(:status, :inactive)
  end

  private

  def set_survey_and_quick_poll_id_to_null
    if passive?
      update_attributes(survey_id: nil, quick_poll_id: nil) unless (survey_id.nil? && quick_poll_id.nil?)
    elsif survey?
      update_attribute(:quick_poll_id, nil) unless quick_poll_id.nil?
    elsif quick_poll?
      update_attribute(:survey_id, nil) unless survey_id.nil?
    end
  end

  def create_geofences_using_geofence_uploads
    if geofence_collection
      geofence_uploads = geofence_collection.geofence_uploads
      if geofence_uploads.any?
        geofence_uploads.each do |geofence_upload|
          geofence_params = {
            name: geofence_upload.name,
            latitude: geofence_upload.latitude,
            longitude: geofence_upload.longitude
          }

          geofence = geofences.find_by(geofence_params)
          geofence_params[:radius] = geofence_upload.radius
          geofence.blank? ? geofences.create(geofence_params) : geofence.update_attributes(geofence_params)
        end
      end
    end
  end

end
