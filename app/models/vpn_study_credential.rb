require 'vpn_api'

class VpnStudyCredential < ActiveRecord::Base

    include Searchable
    
  belongs_to :panelist
  belongs_to :vpn_study

  has_one :usage_study_user, foreign_key: :username, primary_key: :username

  has_attached_file :ios_config
  has_attached_file :android_config
  has_attached_file :android_ssl_cert

  validates_attachment_file_name :ios_config, matches: [/mobileconfig\Z/]
  validates_attachment_file_name :android_config, matches: [/ovpn\Z/]

  enum status: [:pending, :active, :complete, :cancelled]

  scope :completed_and_cancelled, -> { where(status: [VpnStudyCredential.statuses["complete"], VpnStudyCredential.statuses["cancelled"]]) }
  scope :pending_or_active,       -> { where(status: [VpnStudyCredential.statuses["pending"], VpnStudyCredential.statuses["active"]]) }
  scope :with_unavailable_status, -> { where(status: VpnStudyCredential.unavailable_statuses) }
  scope :active,                  -> { where(status: [VpnStudyCredential.statuses["active"]]) }
  scope :ordered,                 -> { order('created_at DESC') }

  def activate!
    update_attribute(:status, :active)
  end

  def complete!
    deleted_config = deactivate_vpn_study_config
    update_attribute(:status, :complete) if deleted_config
  end

  def cancel!
    deleted_config = deactivate_vpn_study_config
    update_attribute(:status, :cancelled) if deleted_config
  end

  def deactivate_vpn_study_config
    vpn_api_params = {
      panelist_id: panelist_id,
      vpn_study_id: vpn_study_id,
      type: 'delete'
    }
    vpn_api_client  = VpnApi.new vpn_api_params
    response        = vpn_api_client.delete_config

    return true if response.to_s.include?("200")
    false
  end

  private

  def self.unavailable_statuses
    [
     VpnStudyCredential.statuses['active'],
     VpnStudyCredential.statuses['complete'],
     VpnStudyCredential.statuses['cancelled']
    ]
  end

end
