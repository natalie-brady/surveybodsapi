module CsvBuilder
  extend ActiveSupport::Concern

  def csv_values
    self.class.csv_attributes.map{ |csv_attribute| send(csv_attribute) }
  end

  class_methods do
    def except_attributes
      %w(created_at updated_at)
    end

    def csv_attributes(custom_attributes: [])
      attributes = attribute_names.reject{ |attribute_name| except_attributes.include?(attribute_name) }
      attributes << custom_attributes
      attributes.flatten
    end
  end
end
