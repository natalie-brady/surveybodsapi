module UrlBuilder
  extend ActiveSupport::Concern

  def self.build_url(host, path=nil, query=nil)
    uri = URI(host)
    uri.path = path
    uri.query = query
    uri.to_s
  end

end
