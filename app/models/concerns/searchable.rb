module Searchable
  extend ActiveSupport::Concern

  included do
    def self.search(search_params={})
      search_params.reject!{ |search_key, search_value| search_value.blank? } unless search_params.blank?
      sanitised_params = search_params.blank? ? nil : search_params.symbolize_keys
      where(sanitised_params)
    end
  end
end

