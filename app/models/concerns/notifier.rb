module Notifier
  extend ActiveSupport::Concern

  class PushNotification

    def initialize
      @gateway = Rails.env.production? ? "gateway.push.apple.com" : "gateway.sandbox.push.apple.com"
      @pusher = Grocer.pusher(
        certificate: StringIO.new(Setting.first.apn_cert),
        passphrase:  Setting.first.apn_passphrase,
        gateway:     @gateway,
        port:        2195,
        retries:     3
      )
    end

    def send(token, alert, custom=nil, sound="default")
      Rails.logger.debug("APN Token: #{token} | #{alert} | #{custom} | #{sound}")
      notification = Grocer::Notification.new(
        device_token: token,
        alert: alert,
        sound: sound,
        custom: custom
      )
      @pusher.push(notification)
    end

    def send_background(token)
      Rails.logger.debug("APN Token: #{token}")
      notification = Grocer::Notification.new(
        device_token: token,
        alert: "",
        content_available: true
      )
      @pusher.push(notification)
    end


    def process_feedback
      feedback = Grocer.feedback(
        certificate: StringIO.new(Setting.first.apn_cert),
        passphrase:  Setting.first.apn_passphrase,
        gateway:     @gateway,
        retries:     3
      )
      feedback_items = feedback.to_a
      feedback_items.each do |feedback_item|
        pnc = PushNotificationCredential.find_by_token(feedback_item.device_token)
        if pnc != nil
          Rails.logger.debug(">>> Removing PNC : #{pnc.token}")
          pnc.destroy
        end
      end
    end

  end

  class GoogleCloudMessage

    def initialize
      @key = Setting.first.try(:gcm_key)
      @uri = URI("https://android.googleapis.com/gcm/send")
    end

    def send(tokens, data)
      Rails.logger.debug("GCM Tokens: #{tokens.to_s}")
      https = Net::HTTP.new(@uri.host, @uri.port)
      req = Net::HTTP::Post.new(@uri.path)
      https.use_ssl = true
      req['Content-Type'] = "application/json"
      req['Authorization'] = "key=#{@key}"
      body = {
        "registration_ids": tokens,
        "data": data
      }
      req.body = body.to_json
      res = https.request(req)
      Rails.logger.debug("Response #{res.code} #{res.message}: #{res.body}")
      gcm_results = JSON.parse(res.body)['results']
      invalid_tokens = get_invalid_tokens(gcm_results, tokens)
      destroy_invalid_push_notification_credentials(invalid_tokens) if invalid_tokens.any?
    end

    def get_invalid_tokens(response, tokens)
      invalid_tokens = []
      errors = ["InvalidRegistration", "NotRegistered"]
      response.each_with_index do |r, i|
        r = r.stringify_keys #make sure I can do the logical test with an string as key
        if errors.include?(r["error"])
          invalid_tokens << tokens[i]
        end
      end
      invalid_tokens
    end

    def destroy_invalid_push_notification_credentials(invalid_tokens)
      invalid_push_notification_credentials = PushNotificationCredential.android_devices.where(token: invalid_tokens)
      invalid_push_notification_credentials.destroy_all
    end

  end

end
