class PushNotificationLog < ActiveRecord::Base
  serialize :sample_ids

  belongs_to :user
end
