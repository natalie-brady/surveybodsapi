class VpnPageMonitoringResult < ActiveRecord::Base
  enum test_status: [:fail, :success]

  scope :ordered, -> { order("start_time desc") }
end
