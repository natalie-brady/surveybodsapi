class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :push_notification_logs
  has_many :panelist_sync_logs
  has_many :log_exports
  has_many :geofence_log_exports

  def to_s
    email
  end
end
