class UsageStudyUser < UsageStudyStagingDatabase
  self.table_name = 'user'

  belongs_to  :vpn_study_credential, primary_key: :username, foreign_key: :username

  has_many    :usage_study_access_logs, primary_key: :ip_client, foreign_key: :ip_client
  has_many    :vpn_bandwidth_usages, primary_key: :ip_client, foreign_key: :ip_client
  has_one     :panelist, through: :vpn_study_credential
  has_one     :vpn_study, through: :vpn_study_credential

  scope :ordered,           -> { order('id DESC') }
  scope :load_associations, -> { includes(:vpn_study_credential, :panelist, :vpn_study) }

  def csv_values
    [
      ip_client,
      username,
      active,
      panelist
    ]
  end
end
