require 'vpn_api'

class Panelist < ActiveRecord::Base
  include UrlBuilder
  include CsvBuilder
  include Searchable

  has_secure_password validations: false
  has_many :push_notification_credentials, :dependent => :destroy
  has_many :quick_poll_choices, through: :quick_poll_responses
  has_many :quick_poll_responses, :dependent => :destroy
  has_many :geofence_logs, dependent: :destroy
  has_and_belongs_to_many :samples

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  before_save   :create_access_token
  after_create  :generate_external_auth_code

  has_many :questback_group_users, :foreign_key => :uid, :primary_key => :uid, :class_name => "QuestbackGroupUser"
  has_many :questback_groups, through: :questback_group_users
  has_many :geofence_groups,  through: :questback_groups
  has_many :geofences,    through: :geofence_groups
  has_many :quick_polls,  through: :questback_groups
  has_many :vpn_studies,  through: :questback_groups
  has_many :vpn_study_credentials
  has_many :device_metadata
  has_many :bart_exclusions
  has_many :bart_audit_logs

  scope :ordered, -> { order("last_accessed desc") }
  scope :active_by_last_accessed_with_range, -> (start_date, end_date) { where(last_accessed: start_date..end_date) }

  def self.search(search)
  where("email LIKE ?", "%#{search}%") 
  end

  def to_s
    email
  end

  def self.active_between(start_date, end_date)
    where(last_accessed: start_date..end_date)
  end

  # def self.get_email(access_token)
  #   panelist = Panelist.find_by_access_token(access_token)
  #   !panelist.blank? && !panelist.expired? ? panelist.email : nil
  # end

  def completed_quick_polls
    quick_poll_choices.map(&:quick_poll).uniq if quick_poll_choices
  end

  def available_quick_polls
    available_polls       = quick_polls.activities.active.with_quick_poll_questions - completed_quick_polls
    polls = {
      'quick_polls' => available_polls.map do |quick_poll|
        quick_poll.poll_info
      end
    }
    return polls
  end

  def get_redeem_url
    UrlBuilder::build_url(Setting.first.qb_hostname, "/survey_redeem.php", "login_code=#{external_auth_panelist_code}")
  end

  # Don't remove this. A fix in EFS side is on going for this implementation
  # def refer_a_friend(refer_email: nil, name: nil)
  #   refer_promotion_id = Setting.first.refer_promotion_id

  #   params = {
  #     promotion_id: refer_promotion_id,
  #     email: refer_email,
  #     panelist_id: uid
  #   }

  #   qb = ::Questback.new(email)
  #   qb.tell_a_friend(params)
  # end

  def refer_a_friend(refer_email: nil, name: nil)
    app_setting     = Setting.first
    base_url        = UrlBuilder::build_url(app_setting.qb_hostname, "/my_refer.php")
    post_url        = URI.parse(base_url)
    post_url.query  = URI.encode_www_form(
      ch_stage_module_my_refer_2: 'execute',
      ch_header_module_my_refer_2: name,
      ch_email_module_my_refer_2: refer_email,
      ch_campaign_id_module_my_refer_2: app_setting.refer_promotion_id,
      login_code: external_auth_panelist_code,
      frmnd: 'my_refer'
    )

    HTTParty.post(post_url)
  end

  def self.reset_password(email)
    post_url = UrlBuilder::build_url(Setting.first.qb_hostname, '/password_recover.php', 'ch_stage_RECOVERY=sendticket_email_check&frmnd=password_recover')
    uri = URI.parse(post_url)

    http = Net::HTTP.new(uri.host, uri.port)

    request = Net::HTTP::Post.new(uri.request_uri, {"Accept-Encoding" => ""})
    params = {
      :ch_email_RECOVERY => email,
      :frmnd => 'password_recover'
    }
    request.set_form_data(params)
    http.request(request)
  end

  def generate_external_auth_code
    qb = ::Questback.new(email)
    panelist_code = nil

    if external_auth_code.nil?
      auth_code = SecureRandom.uuid
      qb.create_external_auth(auth_code)
      self.external_auth_code = auth_code
      panelist_code = qb.get_external_auth_pcode(external_auth_code)
    else
      if external_auth_panelist_code.nil?
        panelist_code = qb.get_external_auth_pcode(external_auth_code)
      else
        panelist_code = external_auth_panelist_code
      end
    end

    self.external_auth_panelist_code = panelist_code
    self.save!
  end

  def geofences_without_completed_survey
    geofences.where(geofence_group_id: allowed_geofence_group_ids)
  end

  def remove_existing_push_notification_credentials(service)
    notification_service = service.eql?('apn') ? "apple_devices" : "android_devices"
    notification_service = "all" if service.nil?

    credentials = push_notification_credentials.send(notification_service.to_sym)
    credentials.destroy_all
  end

  def send_device_notification(survey_id:)
    unless is_included_in_completed_survey?(survey_id)
      push_notification_credentials.each do |push_notification_credential|
        push_notification_credential.send_push_notification(survey_id: survey_id)
      end
    end
  end

  def has_apn?
    push_notification_credentials.apple_devices.any?
  end

  def has_gcm?
    push_notification_credentials.android_devices.any?
  end

  def csv_values
    [
      id,
      access_token,
      email,
      last_accessed,
      uid,
      created_at,
      activated,
      external_auth_code,
      external_auth_panelist_code,
      has_apn?,
      has_gcm?
    ]
  end

  def has_active_vpn_study?
    vpn_study_credentials.active.any?
  end

  def active_vpn_study
    active_vpn_study_credential = vpn_study_credentials.active.last
    active_vpn_study_credential.vpn_study.attributes_with_values
  end

  def available_vpn_studies
    completed_and_cancelled_vpn_study_ids   = vpn_study_credentials.completed_and_cancelled.pluck(:vpn_study_id)
    available_vpn_studies                   = vpn_studies.active.where.not(id: completed_and_cancelled_vpn_study_ids)

    available_vpn_studies
  end

  def completed_vpn_studies
    vpn_study_credentials.complete.map{ |vpn_study_credential| vpn_study_credential.vpn_study.attributes_with_values}
  end

  def cancelled_vpn_studies
    vpn_study_credentials.cancelled.map{ |vpn_study_credential| vpn_study_credential.vpn_study.attributes_with_values}
  end

  def create_vpn_study_credential(vpn_study_credential)
    vpn_study_api_client = VpnApi.new(panelist_id:  id,
                                      vpn_study_id: vpn_study_credential[:vpn_study_id],
                                      type:         vpn_study_credential[:type])
    vpn_study_api_client.create_config
  end

  private
    def create_access_token
      if self.access_token.blank?
        self.access_token = SecureRandom.uuid
      end
    end

    def allowed_geofence_group_ids
      sample_survey_geofence_group_ids - (completed_survey_geofence_group_ids + completed_quick_poll_geofence_group_ids)
    end

    def completed_survey_geofence_group_ids
      completed_survey_ids = Survey.where(survey_id: completed_questback_surveys_ids).pluck(:id)
      geofence_groups.single.where(survey_id: completed_survey_ids).pluck(:id)
    end

    def completed_quick_poll_geofence_group_ids
      completed_quick_poll_ids = completed_quick_polls.map{ |completed_quick_poll| completed_quick_poll.id }
      geofence_groups.single.where(quick_poll_id: completed_quick_poll_ids).pluck(:id)
    end

    def sample_survey_geofence_group_ids
      active_survey_ids_from_sample = samples.map{ |sample| sample.survey_id if sample.survey && sample.survey.is_active_and_questback_active? }.compact
      geofence_groups.active.where(survey_id: [active_survey_ids_from_sample, nil]).pluck(:id)
    end

    def self.except_attributes
      %w(updated_at password_digest avatar_file_name avatar_content_type avatar_file_size avatar_updated_at)
    end

    def is_included_in_completed_survey?(survey_id)
      completed_questback_surveys_ids.include?(survey_id)
    end

    def completed_questback_surveys_ids
      questback             = Questback.new email
      completed_surveys     = questback.get_completed_surveys
      completed_surveys.nil? ? [] : completed_surveys.map{ |completed_survey| completed_survey['surveyId'].to_s }
    end

end
