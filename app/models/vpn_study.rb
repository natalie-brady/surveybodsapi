class VpnStudy < ActiveRecord::Base
  include Searchable

  belongs_to :questback_group

  has_many  :panelists, through: :questback_group
  has_many  :vpn_study_credentials, dependent: :destroy
  has_one   :bart_setting, dependent: :destroy

  accepts_nested_attributes_for :bart_setting,
    reject_if: ->(attributes){ attributes.except(*BartSetting.built_in_attributes).values.all?( &:blank? ) }

  validates :questback_group_id, presence: true

  scope :active,              -> { where(active: true) }
  scope :ordered,             -> { order('created_at DESC') }
  scope :alphabetical_order,  -> { order('name ASC') }

  def self.search(search)
  where("name LIKE ?", "%#{search}%") 
  end

  def deactivate!
    update_attribute(:active, false)
  end

  def activate!
    update_attribute(:active, true)
  end

  def usage_study_users
    usernames = vpn_study_credentials.pluck(:username)
    UsageStudyUser.where username: usernames
  end

  def usage_study_access_logs
    ip_clients = usage_study_users.pluck(:ip_client)
    UsageStudyAccessLog.where(ip_client: ip_clients)
  end

  def to_s
    name
  end

  def vpn_study_credential_for_panelist(panelist_id:, status:)
    vpn_study_credentials.send(status).where(panelist_id: panelist_id).first
  end

  def attributes_with_values
    mapped_attributes_with_values = {}
    allowed_attributes.map{ |allowed_attribute|
      value = convert_attributes.include?(allowed_attribute) ? send(allowed_attribute.to_sym).to_i : send(allowed_attribute.to_sym)
      mapped_attributes_with_values.merge!(allowed_attribute => value)
    }
    mapped_attributes_with_values
  end

  def active_users(number_of_hours: 0, activity_log: [])
    start_date  = Time.zone.now
    end_date    = start_date - "#{number_of_hours}".to_i.hour

    #usage_study_access_logs.by_date_range(start_date, end_date)
      #.uniq.pluck(:ip_client).count
    total = 0
    activity_log.each{|a| total+=1 if a["last_logtime"] >= end_date}
    return total
  end

  def disconnected_users(number_of_hours: 0, activity_log: [])
    past_date = (Time.zone.now - "#{number_of_hours}".to_i.hours).to_i
    #usage_study_access_logs.where("time_since_epoch <= ?", past_date)
    #  .uniq.pluck(:ip_client).count
    total = 0
    activity_log.each{|a| total+=1 if a["last_logtime"].to_i <= past_date}
    return total
  end

  def activity_report
    panelist_info = vpn_study_credentials.active.select(panelist_info_select_string).joins(:panelist)
    active_usernames = panelist_info.map(&:username)
    sql_query = latest_logs_sql_query(active_usernames)
    logs = UsageStudyAccessLog.find_by_sql(sql_query)
    logs_hash = logs.map(&:serializable_hash)
    panelist_hash = panelist_info.map(&:serializable_hash)
    logs_hash.each do |l|
      panelist_hash.each do |p| 
        l["email"] = p["email"] if l["username"] == p["username"]
        l["id"] = p["id"] if l["username"] == p["username"]
      end
    end
    return logs_hash
  end

  private

  def convert_attributes
    %w(end_time bonus_points tickets last_start_time)
  end

  def panelist_info_select_string
    "`vpn_study_credentials`.`id`,
     `vpn_study_credentials`.`username`,
     `panelists`.`email`,
     CASE
       when (`vpn_study_credentials`.`ios_config_file_name` IS null) then 'Android'
       when (`vpn_study_credentials`.`android_config_file_name` IS null) then 'IOS'
       else 'NA'
     end
     as 'device'"
  end

  def latest_logs_sql_query(usernames)
    names = usernames.join("','")
    sql_query_string = "SELECT from_unixtime(time_since_epoch) last_logtime, user.username FROM access_log JOIN (SELECT MAX(`id`) `max_id`, `ip_client` FROM `access_log` GROUP BY `ip_client`) `max_ids` ON (`max_ids`.`max_id` = `access_log`.`id`) JOIN `user` ON (`user`.`ip_client` = `access_log`.`ip_client`) where `user`.`username` IN ('#{names}')"
    return sql_query_string
  end

  def study_usernames
    vpn_study_credentials.pluck(:username) if vpn_study_credentials
  end

  def except_attributes
    %w(active created_at updated_at)
  end

  def allowed_attributes
    attribute_names.reject{ |attribute_name| except_attributes.include?(attribute_name) }
  end

  def panelists_without_active_or_completed_or_cancelled_record
    panelists.where.not(id: all_unavailable_panelist_ids.uniq)
  end

  def all_unavailable_panelist_ids
    panelist_ids_with_active_vpn_study  = VpnStudyCredential.active.pluck(:panelist_id).uniq
    unavailable_panelist_ids            = vpn_study_credentials.with_unavailable_status.pluck(:panelist_id).uniq
    panelist_ids_with_active_vpn_study + unavailable_panelist_ids
  end

end
