class UsageStudyStagingDatabase < ActiveRecord::Base
  self.abstract_class = true
  establish_connection :usage_study_staging
end
