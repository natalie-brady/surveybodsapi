class QuickPollResponse < ActiveRecord::Base

  belongs_to  :quick_poll_choice
  belongs_to  :panelist

  delegate :quick_poll_question, to: :quick_poll_choice
  delegate :quick_poll, to: :quick_poll_choice
  scope :ordered,                     -> { order(created_at: :desc) }

  validates :panelist_id, :quick_poll_choice_id, presence: true
  validates :quick_poll_choice_id, uniqueness: {:scope => :panelist_id}, if: :is_quick_poll_activity_or_geofence_group_single?
  validates :quick_poll_choice_text, length: {
    maximum: 140,
    too_long: "%{count} characters is the maximum allowed" }

  scope :ordered, -> { order(:id) }

  def csv_values
    [
      panelist.to_s,
      panelist.uid,
      quick_poll.id,
      quick_poll.title,
      quick_poll_question.id,
      quick_poll_choice_id,
      quick_poll_choice.answer,
      quick_poll_choice_text,
      created_at
    ]
  end

  def is_quick_poll_activity_or_geofence_group_single?
    quick_poll.is_activity_or_geofence_group_single?
  end
end
