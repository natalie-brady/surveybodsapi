class GeofenceCollection < ActiveRecord::Base

  has_attached_file                     :file
  do_not_validate_attachment_file_type  :file
  validates_attachment_content_type     :file, content_type: ['text/plain', 'text/csv', 'application/vnd.ms-excel']

  has_many :geofence_uploads, dependent: :destroy

  scope :ordered_by_name, -> { order(name: :asc) }

  def to_s
    name
  end

  def self.search(search)
  where("name LIKE ?", "%#{search}%") 
  end

  def import(attached_file=nil)
    file_to_process = attached_file.nil? ? file : attached_file
    CSV.foreach(file_to_process.path, headers: true, encoding: "ISO-8859-1") do |row|
      geofence_upload_params = {
        name: sanitize_row(row[0]),
        latitude: row[1],
        longitude: row[2]
      }

      existing_geofence_upload  = geofence_uploads.find_by(geofence_upload_params)
      geofence_upload_params[:radius]  = row[3].blank? ? 50 : row[3]
      existing_geofence_upload.blank? ? geofence_uploads.create(geofence_upload_params) : existing_geofence_upload.update_attributes(geofence_upload_params)
    end
  end

  private

  def sanitize_row(row)
    row.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: ' ')
  end
end
