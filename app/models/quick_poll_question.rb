class QuickPollQuestion < ActiveRecord::Base
  QUESTION_TYPES = %w(single multiple)

  belongs_to :quick_poll
  has_many :quick_poll_choices, dependent: :destroy
  has_many :quick_poll_responses, through: :quick_poll_choices

  validates_inclusion_of :question_type, in: QUESTION_TYPES
  accepts_nested_attributes_for :quick_poll_choices, allow_destroy: true

  #Adding different class methods for question types (e.g QuickPollQuestion.single = 'single')
  class << self
    QUESTION_TYPES.each do |qtype|
      define_method(qtype) do
        qtype
      end
    end
  end

  #Instance boolean methods for logical tests
  QUESTION_TYPES.each do |qtype|
    define_method "#{qtype}?" do
      question_type == qtype
    end
  end

  def self.question_types
     QUESTION_TYPES.map {|t| [t.titleize, t]}
  end

end
