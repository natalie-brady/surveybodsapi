class QuestbackGroup < ActiveRecord::Base
  include Searchable

  has_many :questback_group_users
  has_many :panelists, :through => :questback_group_users
  has_many :geofence_groups
  has_many :quick_polls

  has_one :vpn_study

  scope :ordered_by_name, -> { order(name: :asc) }

  def to_s
    name
  end
end
