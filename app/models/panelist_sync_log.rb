class PanelistSyncLog < ActiveRecord::Base
  default_scope { order("started_date DESC") }
  belongs_to :user
end
