require 'questback'

class MailTemplate < ActiveRecord::Base
  has_one :setting

  def self.reload
    qb = ::Questback.new("")
    response = qb.get_mail_templates
    response.each do |template|
      mail_template = MailTemplate.find_or_create_by(:template_id => template["id"])
      mail_template.description = template["meta"]["description"]
      mail_template.save
    end
  end

end
