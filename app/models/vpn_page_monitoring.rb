class VpnPageMonitoring < ActiveRecord::Base
  validates :page_url, format: { with: URI::regexp(%w(http https)) }

  after_save :load_speed_test_job

  def self.remove_existing_speed_test_job
    existing_jobs = Delayed::Job.where("handler like ?", "%#{VpnPageSpeedTestJob}%")
    existing_jobs.destroy_all if existing_jobs.any?
  end

  private
  def load_speed_test_job
    VpnPageMonitoring.remove_existing_speed_test_job

    wait_time = frequency_of_calls / 1000
    VpnPageSpeedTestJob.set(wait: wait_time.seconds).perform_later(error_count: 0)
  end

end
