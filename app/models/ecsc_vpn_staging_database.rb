class EcscVpnStagingDatabase < ActiveRecord::Base
  self.abstract_class = true
  establish_connection :ecsc_vpn_staging
end
