class QuickPollStat < ActiveRecord::Base
  belongs_to :quick_poll
  scope :with_responses, -> { where(responded_to: true) }
end
