class VpnBandwidthUsage < UsageStudyStagingDatabase
  self.table_name = 'bandwidth'
  belongs_to :usage_study_user, foreign_key: :ip_client

  def self.csv_headers
    attribute_names.reject{ |attribute_name| ['id'].include?(attribute_name) }
  end

  def csv_values
    [
      ip_client,
      session_start,
      session_end,
      bytes_used,
      username,
      bytes_received,
      bytes_sent,
      terminal
    ]
  end
end
