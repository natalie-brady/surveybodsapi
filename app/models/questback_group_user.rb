class QuestbackGroupUser < ActiveRecord::Base
  belongs_to :questback_group
  belongs_to :panelist, :foreign_key => :uid, :primary_key => :uid
end
