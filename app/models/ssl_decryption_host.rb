class SslDecryptionHost < ActiveRecord::Base

  enum status: [:disabled, :enabled]

  belongs_to :decrypthosts, class_name: 'DecryptHost'

  def creation_time
    created_at.to_i
  end

  def create_or_update_decrypt_host
    decrypthosts.nil? ? create_decrypthost : update_decrypthost
  end

  def create_decrypthost
    decrypt_host = DecryptHost.where(fqdn: decrypt_host_params[:fqdn]).first_or_initialize
    decrypt_host.nil? ? DecryptHost.create(decrypt_host_params) :
      decrypt_host.update_attributes(decrypt_host_params)
    update_attribute(:decrypthosts, decrypt_host)
  end

  def update_decrypthost
    decrypthosts.update_attributes decrypt_host_params
  end

  def decrypt_host_params
    {
      fqdn: host_name,
      comment: name,
      created: created_at,
      active: SslDecryptionHost.statuses[status.to_sym]
    }
  end
end
