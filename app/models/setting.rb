class Setting < ActiveRecord::Base
  belongs_to :mail_template
  belongs_to :survey_category

  def allowed_ip_addresses
    vpn_study_ip_addresses.split(",")
  end
end
