class RouterManagement < ActiveRecord::Base

  validates :ip_address_range, format: { with: /\A((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.)
                                                {3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\Z/x,
                                         message: "invalid format." }
  validates :net_mask,         format: { with: /\A(((128|192|224|240|248|252|254)\.0\.0\.0)|
                                                (255\.(0|128|192|224|240|248|252|254)\.0\.0)|
                                                (255\.255\.(0|128|192|224|240|248|252|254)\.0)|
                                                (255\.255\.255\.(0|128|192|224|240|248|252|254|255)))\Z/x,
                                         message: 'invalid format. Format can be one of the following
                                                  (128|192|224|240|248|252|254).0.0.0 | 255.(0|128|192|224|240|248|252|254).0.0 |
                                                  255.255.(0|128|192|224|240|248|252|254).0 | 255.255.255.(0|128|192|224|240|248|252|254|255)' }

  enum status: [:false, :true]

  scope :active,        -> { where(status: RouterManagement.statuses[:true]) }
  scope :disconnected,  -> { where(status: RouterManagement.statuses[:false]) }

  belongs_to :vpnroute, class_name: 'VpnRoute', dependent: :destroy

  def activate!
    update_attribute(:status, RouterManagement.statuses[:true])
  end

  def creation_date
    created_at.to_i
  end

  def create_or_update_vpnroute
    vpnroute.nil? ? create_vpn_route : update_vpn_route
  end

  def create_vpn_route
    vpn_route = VpnRoute.create vpn_route_params
    update_attribute(:vpnroute, vpn_route)
  end

  def update_vpn_route
    vpnroute.update_attributes(vpn_route_params)
  end

  def vpn_route_params
    {
      ip_range: ip_address_range,
      comment: name,
      created: updated_at,
      netmask: net_mask,
      active: status
    }
  end

end
