class BartAction < ActiveRecord::Base
  belongs_to  :bart_setting
  has_many    :bart_exclusions
end
