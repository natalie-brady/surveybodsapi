class Sample < ActiveRecord::Base
  belongs_to :survey
  has_and_belongs_to_many :panelists
  scope :ordered,                     -> { order(created_at: :asc) }

  def send_notification_to_panelists(survey_id:)
    panelists.each do |panelist|
      panelist.send_device_notification(survey_id: survey_id)
    end
  end

  def self.search(search)
  where("title LIKE ?", "%#{search}%") 
  end

end
