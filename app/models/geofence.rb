class Geofence < ActiveRecord::Base
  include CsvBuilder

  belongs_to  :geofence_group

  has_many    :geofence_logs, dependent: :destroy

  scope :with_active_geofence_group, -> { joins(:geofence_group).where('geofence_group.status = 1') }
  scope :ordered,                     -> { order(name: :asc) }

  reverse_geocoded_by :latitude, :longitude

  validates \
    :name,
    :latitude,
    :longitude,
    :radius,
    presence: true

  def self.search(search)
  where("name LIKE ?", "%#{search}%") 
  end

  def to_s
    name
  end
end
