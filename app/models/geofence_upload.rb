class GeofenceUpload < ActiveRecord::Base
  belongs_to :geofence_collection

  validates :name, :latitude, :longitude, presence: true, uniqueness: { scope: :geofence_collection_id }
end
