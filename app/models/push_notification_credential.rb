class PushNotificationCredential < ActiveRecord::Base
  include Notifier
  validates_uniqueness_of :token, scope: :panelist_id

  scope :apple_devices, -> { where(notification_service: PushNotificationCredential.notification_services['apn']) }
  scope :android_devices, -> { where(notification_service: PushNotificationCredential.notification_services['gcm']) }
  scope :all_devices, -> { where(notification_service: [PushNotificationCredential.notification_services['apn'], PushNotificationCredential.notification_services['gcm']]) }
  scope :by_panelist_ids, -> (ids) { where(panelist_id: ids) }

  belongs_to :panelist
  enum notification_service: { apn: 0, gcm: 1 }

  def test(message)
    logger.debug(">>> Panelist:#{panelist.email}")
    logger.debug(">>> Data:#{message}")

    if apn?
      push_notification = PushNotification.new
      push_notification.send(token, message)
      push_notification.process_feedback
    else
      gcm = GoogleCloudMessage.new
      data = {'message': message}
      gcm.send([token], data)
    end
  end

  def send_push_notification(survey_id:)
    notification_params = {
      survey_id: survey_id,
      pnc_token: token
    }

    AppleNotificationsJob.perform_later(notification_params) if apn?
    AndroidNotificationsJob.perform_later(notification_params) if gcm?
  end

  def self.count_by_device(device_type)
    send("#{device_type}_devices").pluck(:panelist_id).uniq.count
  end

end
