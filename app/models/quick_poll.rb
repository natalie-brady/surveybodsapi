class QuickPoll < ActiveRecord::Base
  include Notifier
  include Searchable

  has_many :quick_poll_questions, dependent: :destroy
  has_many :quick_poll_choices, :through => :quick_poll_questions
  has_many :quick_poll_responses, :through => :quick_poll_choices
  has_many :completed_panelists, :through => :quick_poll_responses, :source => :panelist
  scope :ordered,                     -> { order(title: :asc) }

  has_one :quick_poll_stat, dependent: :destroy
  has_one :geofence_group

  belongs_to :questback_group
  validates_presence_of :title, :description
  accepts_nested_attributes_for :quick_poll_questions,
                                allow_destroy: true,
                                reject_if: :reject_quick_poll_questions

  enum :quick_poll_type => [:activity, :geofence]
  enum :quick_poll_reward_type => [:bonus_points, :tickets]

  scope :active,            -> { where(active: true) }
  scope :activities,        -> { where(quick_poll_type: QuickPoll.quick_poll_types['activity']) }
  scope :geofences,         -> { where(quick_poll_type: QuickPoll.quick_poll_types['geofence']) }
  scope :ordered_by_title,  -> { order(title: :asc) }

  after_create :create_quick_poll_stat_record

  def self.search(search)
  where("title LIKE ?", "%#{search}%") 
  end


  def is_activity_or_geofence_group_single?
    activity? || (geofence? ? geofence_group.single? : true)
  end

  def create_quick_poll_stat_record
    quick_poll_stat_param = {
      completed_panelists_count:        completed_panelists_count,
      questback_group_panelists_count:  questback_group_panelists_count,
      responded_to:                     quick_poll_responses.any?
    }

    create_quick_poll_stat(quick_poll_stat_param)
  end

  def self.with_quick_poll_questions
    all.reject{ |quick_poll| quick_poll.quick_poll_questions.count == 0 }
  end

  def to_s
    try(:title)
  end

  def questions
    try(:quick_poll_questions)
  end

  def completed_panelists_count
    completed_panelists.uniq.count
  end

  def stat_for_completed_panelists_count
    quick_poll_stat ? quick_poll_stat.completed_panelists_count : 0
  end

  def stat_for_questback_group_panelists_count
    quick_poll_stat ? quick_poll_stat.questback_group_panelists_count : 0
  end

  def self.all_polls_info
    polls = {
      'quick_polls' => all.map do |quick_poll|
          quick_poll.poll_info
       end
   }
   return polls
  end

  def get_poll_info
    {'quick_poll' => poll_info }
  end

  def poll_info(without_questions=false)
    info = {
      'id' => id,
      'title'=> title,
      'description' => description,
      'duration' => duration,
      'bonus_points' => points,
      'tickets' => tickets.to_i,
      'start_time' => start_time.to_i,
      'end_time' => end_time.to_i,
     }
    unless without_questions
      info['questions'] = questions.map do |question| 
        {
          'question_type' => question.question_type, 
          'question' => question.question,
          'randomize_order' => question.randomize_order,
          'id'=> question.id,
          'answers'=> question.quick_poll_choices.map{|quick_poll_choice|
            {
              'id' => quick_poll_choice.id,
              'answer' => quick_poll_choice.answer,
              'anchored' => quick_poll_choice.anchored,
              'answer_type' => quick_poll_choice.answer_type.nil? ? "standard" : quick_poll_choice.answer_type
            }
          }
        }
      end
    end
    return info
  end

  def grant_tickets(email)
    qb              = Questback.new(email)
    current_tickets = qb.get_panelist_masterdata("md_9809").to_i
    new_tickets     = current_tickets + tickets.to_i
    attributes      = { "md_tickets" => new_tickets.to_s }
    qb.change_panelist(attributes)
    new_tickets
  end

  def grant_bonus_points(email)
    qb = Questback.new(email)
    qb.modify_bonus_points(bonus_points.to_i, "Quick poll #{title} (id: #{id})")
  end

  def notify
    selected = questback_group_panelists

    if selected.any?
      logger.debug(">>> Selected Panelists #{selected.map {|p| p.uid}}")
      notify_apple_devices(selected)
      notify_android_devices(selected)
    end
  end

  def points
    bonus_points.to_i
  end

  def questback_group_panelists_count
    questback_group.nil? ? 0: questback_group.panelists.count
  end

  private

  def questback_group_panelists
    selected = []

    questback_group.panelists.each do |panelist|
      if panelist.quick_poll_choices.map(&:quick_poll).uniq.index(self) == nil
        selected << panelist
      end
    end

    selected
  end

  def notify_apple_devices(selected = [])
    push_notification = PushNotification.new
    selected.each do |panelist|
      panelist_pncs = panelist.try(:push_notification_credentials).try(:apple_devices)
      panelist_pncs.each do |pnc|
        push_notification.send(pnc.token, notification_message, notification_data[:ios])
      end
    end
  end

  def notify_android_devices(selected = [])
    gcm = GoogleCloudMessage.new
    # TODO add it to delayed jobs
    gcm_tokens = selected.map {|p| p.push_notification_credentials.android_devices.map(&:token)}.flatten
    logger.debug(">>> Selected GCM Tokens #{gcm_tokens}")

    if gcm_tokens.any?
      gcm_tokens.each_slice(1000) do |tokens|
        gcm.send(tokens, notification_data[:android])
      end
    end
  end

  def notification_message
    "New quick poll!"
  end

  def notification_data
    {
      :ios => {
        :quick_poll => {
          :id => id
        }
      },
      :android => {
        :message => notification_message,
        :quick_poll_id => id
      }
    }
  end

  def reject_quick_poll_questions(quick_poll_question)
    quick_poll_question['quick_poll_choices_attributes'].blank?
  end

end
