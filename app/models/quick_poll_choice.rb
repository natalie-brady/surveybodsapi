class QuickPollChoice < ActiveRecord::Base
  enum answer_type: { standard: 0, text: 1 }

  belongs_to :quick_poll_question

  has_many :quick_poll_responses, dependent: :destroy

  delegate :quick_poll, to: :quick_poll_question
end
