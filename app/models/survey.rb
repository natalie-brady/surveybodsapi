class Survey < ActiveRecord::Base
  include UrlBuilder
  include Notifier
  include Searchable
  belongs_to :survey_category
  has_many :samples

  scope :active_and_questback_active, -> { where(active: true, questback_status: "ACTIVE") }
  scope :with_duration,               -> { where.not(duration: [nil, ""]) }
  scope :without_duration,            -> { where(duration: [nil, ""]) }
  scope :ordered,                     -> { order(registration_survey: :desc, start_time: :desc) }
  scope :ordered_by_title,            -> { order(title: :asc) }

  def self.search(search)
  where("title LIKE ?", "%#{search}%") 
  end

  def is_active?
    active? && questback_status.eql?("ACTIVE")
  end

  def to_s
    title
  end

  def get_panelist_url(panelist, geofence_id: nil)
    panelist_url = "syid=#{self.survey_id}&pcode=#{panelist.external_auth_panelist_code}&b=#{panelist.id}"

    UrlBuilder::build_url(Setting.first.qb_hostname, "/gto.php", panelist_url)
  end

  def is_active_and_questback_active?
    active? && questback_status.eql?("ACTIVE")
  end

end
