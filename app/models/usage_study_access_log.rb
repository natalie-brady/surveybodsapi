class UsageStudyAccessLog < UsageStudyStagingDatabase
  self.table_name = 'access_log'

  belongs_to  :usage_study_user, primary_key: :ip_client, foreign_key: :ip_client
  has_many    :bart_audit_logs

  scope :by_ip_client,  -> (ip_client) { where(ip_client: ip_client) }
  scope :ordered,       -> { order('time_since_epoch DESC') }
  scope :by_date_range, -> (start_date, end_date) { where(time_since_epoch: start_date.to_i..end_date.to_i) }

  def self.csv_attributes
    attribute_names << 'panelist_email'
    attribute_names.map{ |attribute_name| attribute_name.camelcase }
  end

  def formatted_time_since_epoch(type: nil)
    format = type.nil? ? '%F %T' : type
    Time.zone.at(time_since_epoch).to_datetime.strftime(format)
  end

  def panelist_email
    usage_study_user.nil? ? '' : usage_study_user.panelist
  end

  def csv_values
    [
      id,
      time_since_epoch,
      time_response,
      ip_client,
      ip_server,
      http_status_code,
      http_reply_size,
      http_method,
      http_url,
      http_username,
      http_mime_type,
      squid_request_status,
      squid_hier_status,
      http_useragent,
      panelist_email
    ]
  end

  def self.query_by_parameters(params, start_date: nil, end_date: nil)
    params.reject{ |param| param.blank? }
    vpn_study_id        = params.delete(:vpn_study_id) if params[:vpn_study_id]
    params[:ip_client]  = self.get_ip_clients(vpn_study_id) unless vpn_study_id.blank?
    params[:time_since_epoch] = start_date.to_i..end_date.to_i if !start_date.nil? && !end_date.nil?
    where(params)
  end

  def self.get_ip_clients(vpn_study_id)
    vpn_study   = VpnStudy.find_by id: vpn_study_id
    usernames   = vpn_study.vpn_study_credentials.pluck(:username)
    users       = UsageStudyUser.where(username: usernames)
    users.pluck(:ip_client)
  end
end
