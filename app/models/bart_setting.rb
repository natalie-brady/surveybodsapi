class BartSetting < ActiveRecord::Base
  has_attached_file :action_csv_file
  do_not_validate_attachment_file_type  :action_csv_file
  validates_attachment_content_type     :action_csv_file, content_type: ['text/plain', 'text/csv', 'application/vnd.ms-excel', 'application/octet-stream']

  has_attached_file :email_template
  do_not_validate_attachment_file_type :email_template

  belongs_to  :vpn_study
  has_many    :bart_actions, dependent: :destroy

  after_save :process_bart_action, if: :action_csv_file_uploaded?
  after_save :enqueue_bart_setting, if: :bart_actions_are_present?

  def bart_actions_are_present?
    bart_actions.any?
  end

  def action_csv_file_uploaded?
    action_csv_file_updated_at_changed?
  end

  def self.built_in_attributes
    ['id', 'vpn_study_id', 'created_at', 'updated_at', 'exclude_all']
  end

  private

  def process_bart_action
    if action_csv_file_updated_at_changed?
      bart_actions.destroy_all

      CSV.foreach(action_csv_file.path, headers: true, encoding: "ISO-8859-1") do |row|
        unless row[0].blank? || row[1].blank?
          bart_action_params = {
            search_term:        row[0],
            survey_url:         row[1],
            survey_parameters:  row[2]
          }

          bart_actions.find_or_create_by bart_action_params
        end
      end
    end
  end

  def enqueue_bart_setting
    delayed_job = Delayed::Job.where("handler like ?", "%#{BartSettingJob.to_s}%bart_setting_id%#{id}%")
    delayed_job.destroy_all if delayed_job

    BartSettingJob.delay(run_at: frequency_checker.to_i.minutes.from_now).
      perform_later(bart_setting_id: id) unless Time.zone.now >= end_activation_period
  end
end
