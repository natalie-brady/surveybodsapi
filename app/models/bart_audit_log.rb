class BartAuditLog < ActiveRecord::Base
  include CsvBuilder
  include Searchable

  belongs_to :panelist
  belongs_to :usage_study_access_log

  scope :ordered, -> { order(access_log_timestamp: :desc) }

  def formatted_access_log_email_sent
    access_log_email_sent ? 'Yes' : 'No'
  end

  def formatted_access_log_timestamp
    Time.zone.at(access_log_timestamp.to_i).to_datetime.strftime('%F %T')
  end

  def panelist_email
    panelist.to_s
  end

  def self.except_attributes
    super.unshift(['id', 'panelist_id', 'access_log_timestamp']).flatten
  end

  def self.csv_attributes
    super.unshift(['panelist_email', 'formatted_access_log_timestamp']).flatten
  end

  def self.query_by_parameters(params)
    search(params)
  end
end
