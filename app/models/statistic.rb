class Statistic < ActiveRecord::Base

  def self.allowed_attributes
    attribute_names.reject{ |attribute_name| except_attributes.include?(attribute_name) }
      .map{ |attribute| attribute.gsub("_count","") }
  end

  def weekly_actively_participating_count
    "-"
  end

  private

  def self.except_attributes
    %w(id week_number created_at updated_at)
  end
end
