class GeofenceLogExport < ActiveRecord::Base
  belongs_to :user
  has_attached_file :file_csv
  do_not_validate_attachment_file_type :file_csv
end
