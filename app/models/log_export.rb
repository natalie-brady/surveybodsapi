class LogExport < ActiveRecord::Base
  has_attached_file :file
  do_not_validate_attachment_file_type :file

  enum export_type: {
                      geofence_log: 'geofence_log',
                      usage_study_access_log: 'usage_study_access_log',
                      device_metadatum: 'device_metadatum',
                      bart_audit_log: 'bart_audit_log'
                    }

  belongs_to :user
end
