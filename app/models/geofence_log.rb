class GeofenceLog < ActiveRecord::Base
  include Searchable

  belongs_to :panelist
  belongs_to :geofence

  has_one :geofence_group, through: :geofence

  scope :ordered,       -> { order(logged_time: :desc) }
  scope :by_date_range, -> (start_date, end_date) { where(logged_time: start_date..end_date) }


  def self.group_with_count
    select("geofence_logs.*, COUNT(geofence_logs.panelist_id) as logs_count").group(:panelist_id)
  end

  def formatted_epoch_time(timestamp)
    send(timestamp).nil? ? '' : DateTime.strptime(send(timestamp), '%Q')
  end

  def formatted_device_type
    return '' if device_type.nil?
    device_type.downcase.eql?('ios') ? 'iOS' : device_type.titleize
  end

  def formatted_created_date
    created_at.strftime('%FT%T%:z')
  end

  def action
    geofence_group.action
  end

  def csv_values
    [
      panelist.to_s,
      geofence_id,
      geofence,
      geofence_group.nil? ? "" : geofence_group.geofence_type,
      geofence_group.nil? ? "" : geofence_group.id,
      geofence_group.nil? ? "" : geofence_group,
      geofence_group.nil? ? "" : geofence_group.action,
      formatted_epoch_time('logged_time'),
      formatted_epoch_time('location_timestamp'),
      location_accuracy,
      location_latitude,
      location_longitude,
      formatted_device_type
    ]
  end

  def self.query_by_parameters(params)
    params.reject{ |param| param.blank? }
    geofence_group_ids = params[:geofence_group_id].present? ? params.delete(:geofence_group_id) :
                          GeofenceGroup.where(params.symbolize_keys).pluck(:id)

    geofence_ids_by_geofence_group_ids  = Geofence.where(geofence_group_id: geofence_group_ids).pluck(:id)

    where(geofence_id: geofence_ids_by_geofence_group_ids)
  end

end
