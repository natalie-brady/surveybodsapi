class SurveyCategory < ActiveRecord::Base
  has_many :surveys, :dependent => :nullify
  has_one :setting

  scope :ordered_by_name, -> { order(name: :asc) }
end
