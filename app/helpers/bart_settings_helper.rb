module BartSettingsHelper

  def bart_setting_email_subject
    @bart_setting.email_subject.blank? ? 'New Survey' : @bart_setting.email_subject
  end
end
