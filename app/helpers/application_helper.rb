module ApplicationHelper

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).new #need to use send because association is a symbol
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |child|
      render(association.to_s.singularize + "_fields", f: child) #every partial filename must be  <object name in singular>_fields for this to work
    end
    link_to(name, '#', class: 'add_fields', data: {id: id, fields: fields.gsub("\n", "")})
  end

  def environment_name
    "( #{Rails.env.titleize} )"
  rescue
    ""
  end

  def navbar_item(indented:, text:, fa_icon:, link:, controller_name:, multi_indent: false)
    content_tag(
      :li,
      (
        link_to link do
          content_tag(:i, "", class: "fa #{fa_icon} fa-fw").html_safe + " " + text
        end
      ),
      class: "#{indented ? 'navbar-indent'.html_safe : ''} #{controller.controller_name.eql?(controller_name) ? 'active' : '' } #{multi_indent ? 'navbar-multi-indent'.html_safe : '' }"
    )
  end

  def link_to_fa_icon(icon_name, text, link, btn_type, options = {})
    link_to(
      content_tag(:i, '', class: "fa fa-#{icon_name}", title: text),
      link,
      class: "btn btn-#{btn_type}",
      method: options[:method] || '',
      data: options[:data] || ''
    )
  end

  def admin_links_collection
    [
      "admin",
      "questback_groups",
      "samples",
      "profile_variables",
      "master_data_descriptions",
      "users",
      "settings",
      "faqs"
    ]
  end

  def geofences_links_collection
    [
      'geofences',
      'geofence_collections',
      'geofence_groups',
      'geofence_uploads'
    ]
  end

  def panelist_collection
    Panelist.pluck(:email, :id).sort
  end

  def logs_links_collection
    [
      'logs',
      'geofence_logs',
      'push_notification_logs',
      'usage_study_access_logs',
      'quick_poll_responses',
      'panelist_sync_logs',
      'log_exports',
      'geofence_log_exports',
      'device_metadata',
      'vpn_page_monitoring_results',
      'bart_audit_logs'
    ]
  end

  def vpn_studies_links_collection
    [
      'vpn_studies',
      'vpn_study_credentials',
      'usage_study_users',
      'vpn_managements',
      'router_managements',
      'vpn_routes',
      'ssl_decryption_hosts',
      'decrypt_hosts',
      'vpn_bandwidth_usages',
      'vpn_page_monitorings'
    ]
  end

  def vpn_managements_links_collection
    [
      'vpn_managements',
      'router_managements',
      'vpn_routes',
      'ssl_decryption_hosts',
      'decrypt_hosts',
      'vpn_bandwidth_usages',
      'vpn_page_monitorings'
    ]
  end

  def log_exports_links_collection
    [
      'log_exports',
      'geofence_log_exports'
    ]
  end
end
