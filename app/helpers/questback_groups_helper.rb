module QuestbackGroupsHelper
  def questback_groups_collection
    QuestbackGroup.ordered_by_name.collect{ |questback_group| [questback_group.name, questback_group.id] }
  end
end
