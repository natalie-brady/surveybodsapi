module GeofencesHelper

  def select_options
    @geofence_group.nil? ? {} : { selected: @geofence_group.id }
  end
end
