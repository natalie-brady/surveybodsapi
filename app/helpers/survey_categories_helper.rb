module SurveyCategoriesHelper

  def survey_category_collection
    SurveyCategory.pluck(:name, :id)
  end

  def selected_survey_category
    @survey.survey_category_id || default_survey_category_selection
  end

  def default_survey_category_selection
    SurveyCategory.where(name: 'Survey').limit(1).pluck(:name, :id).flatten
  end
end
