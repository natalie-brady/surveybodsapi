module UsageStudyAccessLogsHelper

  def vpn_study_collection
    VpnStudy.alphabetical_order.pluck(:name, :id)
  end
end
