module QuickPollsHelper

  def quick_poll_titles
    QuickPoll.all.sort.collect(&:title)
  end

  def completed_quick_polls
    QuickPollStat.all.map{ |quick_poll_stat| {quick_poll_stat.quick_poll.title => quick_poll_stat.completed_panelists_count} if quick_poll_stat.quick_poll }.reduce({}, :merge)
  end

  def percentage_breakdown_for_questions(question)
    quick_poll_responses = question.quick_poll_responses
    quick_poll_responses.group_by(&:quick_poll_choice)
      .map{ |key, value|
        {
          value: value.count,
          label: key[:answer],
        }
      }
  end

  def total_responded_quick_polls
    QuickPollStat.with_responses.count
  end

end
