module GeofenceGroupsHelper
  def geofence_group_action
    case @geofence_group.action.to_s
    when "survey"
      "Survey: #{@geofence_group.survey.title}"
    when "quick_poll"
      "QuickPoll: #{@geofence_group.quick_poll.title}"
    when "passive"
      "Passive"
    else
      ""
    end
  end

  def geofence_group_actions_collection
    GeofenceGroup.actions.map{ |action| [action[0], action[0]] }
  end

  def geofence_group_collection
    GeofenceGroup.pluck(:name, :id)
  end

  def geofence_group_status
    @geofence_group.status.to_s.titleize
  end

  def link_to_update_status(geofence_group, filter_status)
    status      = geofence_group.active? ? 'deactivate' : 'activate'
    status_text = geofence_group.active? ? 'deactivate' : 'set to active'
    link_to status_text.titleize,
            status_update_geofence_group_path(geofence_group, status: status, filter_status: filter_status),
            method: :post, data: { confirm: "Are you sure you want to #{status_text} #{geofence_group.name}?" },
            class: "btn btn-#{geofence_group.active? ? 'warning' : 'success'} btn-md",
            remote: true
  end

  def available_quick_polls
    used_quick_poll_ids = GeofenceGroup.pluck(:quick_poll_id).uniq.compact
    used_quick_poll_ids -= [@geofence_group.quick_poll_id]
    quick_polls         = QuickPoll.active.where.not(id: used_quick_poll_ids)
    quick_polls.order(:title).map {|quick_poll| [quick_poll.title, quick_poll.id]}
  end

  def geofence_group_trigger_types
    GeofenceGroup.trigger_types.map{ |trigger_type| [rename_trigger_type(trigger_type[0]), trigger_type[0]] }
  end

  def rename_trigger_type(trigger_type)
    trigger_type.eql?('both') ? 'Unrestricted' : trigger_type.titleize
  end
end
