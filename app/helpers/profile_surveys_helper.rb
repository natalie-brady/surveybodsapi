module ProfileSurveysHelper

  def survey_with_duration_collection
    Survey.with_duration.map{ |s| [s.title.strip, s.id] }.sort
  end
end
