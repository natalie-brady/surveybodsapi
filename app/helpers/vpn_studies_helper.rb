module VpnStudiesHelper

  def panelist_has_active_or_pending_vpn_study_credential?(panelist:)
    panelist.vpn_study_credentials.pending_or_active.where(vpn_study_id: @vpn_study.id).any?
  end

  def panelist_has_completed_vpn_study?(panelist:)
    panelist.vpn_study_credentials.complete.where(vpn_study_id: @vpn_study.id).any?
  end

  def panelist_active_or_pending_vpn_study_credential(panelist:)
    panelist.vpn_study_credentials.pending_or_active.where(vpn_study_id: @vpn_study.id).first
  end

  def last_log_entry_timestamp(vpn_study_credential)
    return 'No Vpn Study Logs' if vpn_study_credential.usage_study_user.nil?
    usage_study_user  = vpn_study_credential.usage_study_user
    last_log_entry    = usage_study_user.usage_study_access_logs.ordered.first
    last_log_entry ? last_log_entry.formatted_time_since_epoch : 'No Vpn Study Logs'
  end

  def last_log_status(vpn_activity)
    last_log_entry_timestamp  = vpn_activity["last_logtime"]
    return 'disconnected' if last_log_entry_timestamp.eql?('No Vpn Study Logs')
    base_timestamp        = last_log_entry_timestamp.to_datetime.to_i
    one_hour_timestamp    = (1.hour.ago).to_i
    twelve_hour_timestamp = (12.hours.ago).to_i

    if (base_timestamp > one_hour_timestamp) || (base_timestamp > twelve_hour_timestamp)
      status = 'connected'
    elsif base_timestamp < twelve_hour_timestamp
      status = 'disconnected'
    end
    status
  end

  def last_log_currently_up_to_date?(vpn_study_credential)
    last_log_date = last_log_entry_timestamp(vpn_study_credential).to_datetime
    last_log_date.to_i > (5.minutes.ago).to_i && last_log_date.to_i < Time.zone.now.to_i
  end

  def ending_study_alert_message(vpn_study)
    "Are you sure wish to end #{vpn_study.name} study?\nThis will notify all active users to disconnect from the VPN and the study cannot be started again."
  end

  def existing_or_build_bart_setting
    @bart_setting || @vpn_study.build_bart_setting
  end
end
