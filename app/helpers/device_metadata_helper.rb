module DeviceMetadataHelper
  def sanitize_more_info(more_info)
    return "No additional info" if more_info.blank?
    content_tag :p do
      more_info.collect{ |key, value| concat content_tag(:p, "#{key}: #{value}") }
    end
  end
end
