require "statistics_data"

module Api::StatisticsHelper

  def panelist_statistics_data
    statistic_data = StatisticsData.new

    {
      active_user_count: statistic_data.get_panelist_count_by_filter(filter_type: 'all_active'),
      active_user_surveybods_count: statistic_data.get_panelist_count_by_group(group_type: 'total_active_panelists'),
      active_user_ios_count: PushNotificationCredential.count_by_device('apple'),
      active_user_android_count: PushNotificationCredential.count_by_device('android'),
      active_4_week_count: statistic_data.get_panelist_count_by_group(group_type: 'total_actively_participating'),
      active_4_week_ios_count: active_by_range(range: 4.week, device_type: 'apple'),
      active_4_week_android: active_by_range(range: 4.week, device_type: 'android'),
    }
  end

  def active_by_range(range:, device_type:)
    start_date  = (Time.now - range).beginning_of_day
    end_date    = Time.now.end_of_day

    panelist_ids                  = Panelist.active_by_last_accessed_with_range(start_date, end_date).pluck(:id)
    push_notification_credentials = PushNotificationCredential.send("#{device_type}_devices").by_panelist_ids(panelist_ids)
    push_notification_credentials.pluck(:panelist_id).uniq.count
  end

end
