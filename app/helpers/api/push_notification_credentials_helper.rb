module Api::PushNotificationCredentialsHelper

  def format_notification_service(notification_service)
    case notification_service
    when 'apn' then 'apple_devices'
    when 'gcm' then 'android_devices'
    else 'all'
    end
  end

end
