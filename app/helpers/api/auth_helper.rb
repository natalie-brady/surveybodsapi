require 'questback'

module Api::AuthHelper

  def get_access_token(email, password, facebook=false)
    success = false
    access_token = ""
    panelist = Panelist.find_by_email(email)
    qb = ::Questback.new(email)

    if !panelist.blank? && panelist.active?
      # Already authorised
      if facebook || panelist.authenticate(password)
        access_token = panelist.access_token
        success = true
      end

    elsif facebook && (qb.get_panelist != nil)
      # Is already a SurveyBods user with their Facebook email
      panelist = Panelist.create(:email => email, :last_accessed => Time.zone.now)
      access_token = panelist.access_token
      success = true

    elsif !facebook
      # Is a normal SurveyBods user
      qb.is_valid_login(password)
      panelist = Panelist.create(:email => email, :password => password, :last_accessed => Time.zone.now)
      access_token = panelist.access_token
      success = true
    end

    if success
      return access_token
    else
      return nil
    end
  end

end
