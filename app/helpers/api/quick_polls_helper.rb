module Api::QuickPollsHelper
  def quick_poll_result_info
    {
      id: @quick_poll.id,
      title: @quick_poll.title,
      total_responses: @quick_poll.quick_poll_stat.completed_panelists_count,
      questions: quick_poll_questions_info
    }
  end

  def quick_poll_questions_info
    @quick_poll.quick_poll_questions.map do |quick_poll_question|
      {
        id: quick_poll_question.id,
        question: quick_poll_question.question,
        question_type: quick_poll_question.question_type,
        total_responses: quick_poll_question.quick_poll_responses.count,
        answers: quick_poll_question_answers_info(quick_poll_question)
      }
    end
  end

  def quick_poll_question_answers_info(quick_poll_question)
    quick_poll_question.quick_poll_choices.map do |quick_poll_choice|
      {
        id: quick_poll_choice.id,
        answer: quick_poll_choice.answer,
        responses: quick_poll_choice.quick_poll_responses.count
      }
    end
  end
end
