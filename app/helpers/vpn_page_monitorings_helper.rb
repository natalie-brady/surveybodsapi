module VpnPageMonitoringsHelper

  def prettify_email_addresses(email_addresses_string)
    email_addresses = email_addresses_string.split(',')
    content_tag(:ul) do
      email_addresses.collect do |email_address|
        concat(content_tag(:li, email_address))
      end
    end
  end

end
