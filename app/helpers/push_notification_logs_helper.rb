module PushNotificationLogsHelper

  def survey_or_quick_poll_link(push_notification_log:)
    if !push_notification_log.survey_id.nil?
      survey = Survey.find push_notification_log.survey_id
      (link_to survey.to_s, survey_path(survey.id))
    elsif !push_notification_log.quick_poll_id.nil?
      quick_poll = QuickPoll.find push_notification_log.quick_poll_id
      (link_to quick_poll.to_s, quick_poll_path(quick_poll.id))
    end
  end

  def sample_or_questback_group(push_notification_log:)
    return push_notification_log.sample_ids.join(', ') unless push_notification_log.sample_ids.nil?
    push_notification_log.questback_group_id unless push_notification_log.questback_group_id.nil?
  end
end
