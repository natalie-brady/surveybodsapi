module StatisticsHelper

  def sanitize_key(key)
    key = 'actively_participating' if key.to_s.eql?('response_rate')
    key.to_s.titleize
  end

  def statistics_records
    all_data    = []
    week_number = 50

    statistics_data_attributes.each do |statistic_data_attribute|
      all_data << statistic_data(type: statistic_data_attribute, week_number: week_number)
    end

    all_data
  end

  def statistic_data(type:, week_number: 50)
    statistic = Statistic.find_by week_number: week_number

    {
      "#{type}": [statistic.send("weekly_#{type}_count"), statistic.send("total_#{type}_count")]
    }
  end

  def statistics_data_attributes
    ['registrations', 'active_panelists', 'actively_participating']
  end

end
