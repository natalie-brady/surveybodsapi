module GeofenceLogsHelper

  def geofence_log_info(geofence_log:, type:)
    case type
    when 'geofence'
      geofence_log.geofence ? (link_to geofence_log.geofence.to_s, geofence_path(geofence_log.geofence_id)) : "No geofence."
    when 'geofence_type'
      geofence_log.geofence_group ? geofence_log.geofence_group.geofence_type : "No Geofence Type."
    when 'geofence_group'
      geofence_log.geofence_group ? (link_to geofence_log.geofence_group.to_s, geofence_group_path(geofence_log.geofence_group)) : "No Geofence Group."
    when 'action'
      geofence_log.geofence ? geofence_log.action : "No Action."
    when 'logged_time'
      geofence_log.formatted_epoch_time('logged_time')
    when 'device'
      geofence_log.formatted_device_type
    else
      type
    end
  end

end
