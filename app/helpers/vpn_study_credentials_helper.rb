module VpnStudyCredentialsHelper
  include VpnStudiesHelper

  def panelist_access_token
    if @panelist
      @panelist.access_token
    else
      @vpn_study_credential.panelist.access_token
    end
  end

  def vpn_study_id
    params[:vpn_study_id] ? params[:vpn_study_id] : @vpn_study_credential.vpn_study_id
  end
end
