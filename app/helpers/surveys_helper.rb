module SurveysHelper

  def survey_collection
    Survey.ordered_by_title.map{ |survey| [survey.title, survey.id] }
  end
end
