module GeofenceCollectionsHelper

  def geofence_collections_or_new(active:)
    [:geofence_collections, :new].include?(active)
  end

  def geofence_link(geofence:)
    geofence.class.eql?(Geofence) ? (link_to geofence.name, geofence) : geofence.name
  end

  def geofence_collection
    GeofenceCollection.ordered_by_name.map{ |geofence_collection|
      [geofence_collection.name, geofence_collection.id] if geofence_collection.geofence_uploads.any?
    }.compact
  end
end
