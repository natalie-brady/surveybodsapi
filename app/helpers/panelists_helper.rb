module PanelistsHelper
  def active_panelist(range=nil)
    start_date    = range.nil? ? Time.zone.now : (Time.zone.now - 1.send(range))
    current_date  = Time.zone.now
    Panelist.active_between(start_date.beginning_of_day, current_date.end_of_day).count
  end
end
