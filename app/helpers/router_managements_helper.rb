module RouterManagementsHelper

  def netmask_collection
    netmasks = []
    (1..32).each do |num|
      netmasks << [cidr_to_netmask(num), num]
    end
    netmasks.reverse
  end

  def cidr_to_netmask(cidr)
    IPAddr.new('255.255.255.255').mask(cidr).to_s
  end
end
