class SslDecryptionHostJob < ActiveJob::Base
  queue_as :default

  def perform(options={})
    id = options['id']

    sync_ssl_decrypt_host(id)

    vpn_api_client = VpnApi.new type: 'decrypt_host'
    vpn_api_result = vpn_api_client.sync
    Rails.logger.debug ">>>>> SSL Decrypt Host Result: #{vpn_api_result.inspect} <<<<<"
  end

  def sync_ssl_decrypt_host(id)
    ssl_decryption_hosts = id.to_s.eql?('all') ?
      SslDecryptionHost.all : SslDecryptionHost.where(id: id)

    ssl_decryption_hosts.map{ |ssl_decryption_host|
      ssl_decryption_host.create_or_update_decrypt_host
    } if ssl_decryption_hosts.any?
  end

end
