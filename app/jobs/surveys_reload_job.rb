class SurveysReloadJob < ActiveJob::Base
  queue_as :default

  before_perform do |job|
    @delayed_job = Delayed::Job.where("handler like ?", "%#{job.job_id}%").first
  end

  def perform(options={})
    qb = Questback.new("")

    qb_response     = options['active'] ?  qb.get_all_active_surveys : qb.get_all_surveys
    response_count  = 1

    qb_response.each do |qb_survey|
      @delayed_job.update_column(:status_text, "Updating #{response_count} of #{qb_response.size}")

      survey = Survey.find_or_create_by(:survey_id => qb_survey["id"])
      survey.title = qb_survey["title"]
      survey.created_time = qb_survey["createTime"]
      survey.start_time = qb_survey["fieldTime"]["startTime"]
      survey.end_time = qb_survey["fieldTime"]["endTime"]
      survey.bonus_points = qb_survey["bonusPoints"]["value"]
      survey.questback_status = qb_survey["status"]
      if survey.changed?
        survey.save
      end

      response_count += 1
    end
  end
end
