class MasterDataReloadJob < ActiveJob::Base
  queue_as :default

  before_perform do |job|
    @delayed_job = Delayed::Job.where("handler like ?", "%#{job.job_id}%").first
  end

  def perform
    qb = Questback.new("")
    response_count = 1

    qb_response = qb.get_masterdata_descriptions

    qb_response.each do |description|
      @delayed_job.update_column(:status_text, "Updating #{response_count} of #{qb_response.size}")

      mdd = MasterDataDescription.find_or_create_by(:varname => description["varname"])
      mapping = {}
      if description.has_key?("categories")
        description["categories"].each do |category|
          mapping[category["code"]] = category["label"]
        end
      end
      mdd.categories = mapping
      mdd.label = description["label"]
      mdd.datatype = description["type"]
      mdd.save

      response_count += 1
    end
  end
end
