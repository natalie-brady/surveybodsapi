class GeofenceLogsCreateJob < ActiveJob::Base
  queue_as :geofence_logs

  ## options should contain the following: panelist_id, geofence_id, logged_time
  def perform(options)
    options   = options.symbolize_keys
    panelist  = Panelist.find_by(id: options[:panelist_id])
    panelist.geofence_logs.create(options) if panelist
  end
end
