class ActiveSamplesReloadJob < ActiveJob::Base
  queue_as :default

  before_perform do |job|
    @delayed_job = Delayed::Job.where("handler like ?", "%#{job.job_id}%").first
  end

  def perform
    qb                = Questback.new("")
    active_surveys    = qb.get_all_active_surveys
    delayed_job_count = 1
    active_surveys.each do |active_survey|
      @delayed_job.update_column(:status_text, "Updating #{delayed_job_count} of #{active_surveys.size}")

      survey_samples = qb.get_survey_samples(active_survey['id'])

      unless survey_samples.nil?
        survey_samples.each do |survey_sample|
          save_sample(survey_sample, active_survey['id'], qb)
        end
      end

      delayed_job_count += 1
    end
  end

  def save_sample(survey_sample, survey_id, qb)
    unless survey_sample["isInternal"]
      sample              = Sample.find_or_create_by(sample_id: survey_sample["sampleId"])
      sample.title        = survey_sample["title"]
      sample.description  = survey_sample["desc"]
      survey              = Survey.find_by_survey_id(survey_id)
      sample.survey       = survey
      sample_panelists    = qb.get_sample_panelists(sample.sample_id)
      panelists           = Panelist.where(uid: sample_panelists.flatten)
      sample.panelists    = panelists

      if sample.changed?
        sample.save
      end
    end
  end
end
