require 'vpn_page_speed_test_service'

class VpnPageSpeedTestJob < ActiveJob::Base
  queue_as :vpn_page_speed_test

  def perform(options={})
    error_count = options['error_count']
    client      = VpnPageSpeedTestService.new error_count: error_count
    client.perform_test
  end
end
