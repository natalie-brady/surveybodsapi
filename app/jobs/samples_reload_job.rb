class SamplesReloadJob < ActiveJob::Base
  queue_as :default

  before_perform do |job|
    @delayed_job = Delayed::Job.where("handler like ?", "%#{job.job_id}%").first
  end

  def perform(options={})
    qb = Questback.new("")
    response = qb.get_samples
    delayed_job_count = 1
    response.each do |sample_response|
      @delayed_job.update_column(:status_text, "Updating #{delayed_job_count} of #{response.size}")

      unless sample_response["isInternal"]

        sample              = Sample.find_or_create_by(:sample_id => sample_response["sampleId"])
        sample.title        = sample_response["title"]
        sample.description  = sample_response["desc"]
        survey              = Survey.find_by_survey_id(sample_response["surveyId"])
        sample.survey       = survey
        sample_panelists    = qb.get_sample_panelists(sample.sample_id)
        panelists           = Panelist.where(uid: sample_panelists.flatten)
        sample.panelists    = panelists

        if sample.changed?
          sample.save
        end

        delayed_job_count += 1
      end
    end
  end
end
