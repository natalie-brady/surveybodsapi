require 'vpn_study_push_notifier'

class VpnStudySendNotificationsJob < ActiveJob::Base
  queue_as :vpn_study_push_notification

  def perform(options = {})
    notifier_type = options['notifier_type']
    client        = VpnStudyPushNotifier.new(vpn_study_id: options['vpn_study_id'], action_type: notifier_type)
    if client.vpn_study
      client.complete_active_vpn_study_credentials if notifier_type.eql?('disconnect')
      client.send_notification_to_devices
    end
  end
end
