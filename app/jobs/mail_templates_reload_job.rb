class MailTemplatesReloadJob < ActiveJob::Base
  queue_as :default

  def perform
    MailTemplate.reload
  end
end
