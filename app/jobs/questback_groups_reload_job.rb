class QuestbackGroupsReloadJob < ActiveJob::Base
  queue_as :default


  before_perform do |job|
    @dj = Delayed::Job.where("handler like ?", "%#{job.job_id}%").first
  end

  def perform
    qb = Questback.new("")
    response = qb.get_groups
    count = 1
    response.each do |group_response|
      @dj.update_column(:status_text, "Updating #{count} of #{response.size}")

      group = QuestbackGroup.find_or_create_by(:group_id => group_response["groupId"])
      logger.debug(">>>> Reloading Group  #{group_response['groupId']} | #{group_response['name']}")
      group.name = group_response["name"]
      group.description = group_response["description"]
      if group.changed?
        group.save
      end

      uids_response = qb.get_group_panelist_uids(group_response["groupId"]).map {|uid| uid.to_i}
      current_panelists = group.questback_group_users.map {|u| u.uid}

      # Add all new panelists
      new_panelists = uids_response - current_panelists
      logger.debug(">>>> New Group Panelists Group  #{new_panelists.size}")
      ActiveRecord::Base.transaction do
        new_panelists.each do |uid|
          group.questback_group_users.create(:uid => uid)
        end
      end

      # Remove all old panelists
      old_panelists = current_panelists - uids_response
      logger.debug(">>>> Old Group Panelists Group  #{old_panelists.size}")
      ActiveRecord::Base.transaction do
        old_panelists.each do |uid|
          group.questback_group_users.find_by_uid(uid).destroy
        end
      end
      count += 1
    end
  end

end
