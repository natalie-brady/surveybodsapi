require 'log_csv_exporter'

class GeofenceLogExportJob < ActiveJob::Base
  queue_as :geofence_log_export

  before_perform do |job|
    @delayed_job = Delayed::Job.where("handler like ?", "%#{job.job_id}%").first
  end

  def perform(options={})
    export_type = options['panelist_id'] ? 'panelist' : 'geofence_group'
    user_id     = options.delete('user_id')

    client = LogCsvExporter.new(user_id: user_id, type: export_type, options: options)
    client.generate_csv(@delayed_job.id)
  end

end
