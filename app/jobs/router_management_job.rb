require "vpn_api"

class RouterManagementJob < ActiveJob::Base
  queue_as :default

  def perform(options={})
    type      = options['type']
    id        = options['id']
    username  = options['username'] || nil

    sync_router_management(id: id) if type.to_s.eql?('sync')

    vpn_api_client          = VpnApi.new type: "route_#{type}"
    vpn_api_client_response = type.to_s.eql?('sync') ?
      vpn_api_client.send(type.to_sym) : vpn_api_client.send(type.to_sym, username: username)

    Rails.logger.debug ">>>>> RouterManagementJob Debug: #{vpn_api_client_response.inspect} <<<<<"
  end

  def sync_router_management(id:)
    router_managements  = id.to_s.eql?('all') ?
      RouterManagement.all : RouterManagement.where(id: id)
    router_managements.map{ |router_management|
                        router_management.create_or_update_vpnroute
                      } if router_managements.any?
  end
end
