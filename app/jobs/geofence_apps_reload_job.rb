class GeofenceAppsReloadJob < ActiveJob::Base
  queue_as :default

  include Notifier

  def perform
    apple_pncs        = PushNotificationCredential.apple_devices
    push_notification = PushNotification.new
    apple_pncs.each do |pnc|
      push_notification.send_background(pnc.token)
    end

    android_tokens = PushNotificationCredential.android_devices.pluck(:token)
    if android_tokens.any?
      gcm   = GoogleCloudMessage.new
      data  = { refresh_geofence: true }

      android_tokens.each_slice(1000) do |tokens|
        gcm.send(tokens, data)
      end
    end
  end
end
