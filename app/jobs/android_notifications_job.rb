class AndroidNotificationsJob < ActiveJob::Base
  queue_as :push_notification

  include Notifier

  def perform(options = {})
    pnc_token = options['pnc_token']
    survey_id = options['survey_id']

    gcm               = GoogleCloudMessage.new
    notification_data = { message: notification_message, id: survey_id }
    gcm.send([pnc_token], notification_data)
  end

  def notification_message
    "You have a new survey!"
  end
end
