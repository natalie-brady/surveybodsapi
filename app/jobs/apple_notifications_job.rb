class AppleNotificationsJob < ActiveJob::Base
  queue_as :push_notification

  include Notifier

  def perform(options = {})
    pnc_token = options['pnc_token']
    survey_id = options['survey_id']

    push_notification = PushNotification.new
    notification_data = { survey: { id: survey_id } }
    push_notification.send(pnc_token, notification_message, notification_data)

    push_notification.process_feedback
  end

  def notification_message
    "You have a new survey!"
  end
end
