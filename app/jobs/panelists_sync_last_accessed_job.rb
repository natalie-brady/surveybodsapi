class PanelistsSyncLastAccessedJob < ActiveJob::Base
  queue_as :default

  before_perform do |job|
    @delayed_job = Delayed::Job.where("handler like ?", "%#{job.job_id}%").first
  end

  def perform(options = {})
    user = User.find options['user_id']

    panelist_sync_log = user.panelist_sync_logs.create(started_date: Time.now)
    # panelists = Panelist.where(email: 'manolet@cloudemployee.co.uk')
    panelists = Panelist.all
    count = 1

    panelists.each do |panelist|
      @delayed_job.update_column(:status_text, "Updating #{count} of #{panelists.size}")
      questback = Questback.new panelist.email
      panelist_data = {
        'md_app_login' => panelist.last_accessed
      }

      questback.change_panelist panelist_data
      count += 1
    end

    panelist_sync_log.update_attribute(:ended_date, Time.now)
  rescue
    logger.debug(">>>>> Error updating md_app_login field for #{panelist} <<<<<")
  end
end
