require 'behaviour_activated_research_tasks'

class BartSettingJob < ActiveJob::Base
  queue_as :bart_setting

  def perform(options={})
    bart_setting = BartSetting.find_by_id options[:bart_setting_id]

    if bart_setting
      options[:end_date]    = (Time.zone.now >= bart_setting.end_activation_period) ? bart_setting.end_activation_period : Time.zone.now
      options[:start_date]  = options[:end_date] - bart_setting.frequency_checker.to_i.minutes

      BehaviourActivatedResearchTasks.new(options).process_behaviour_activated_research_task
      BartSettingJob.delay(run_at: bart_setting.frequency_checker.to_i.minutes.from_now).perform_later(bart_setting_id: bart_setting.id) unless options[:end_date] == bart_setting.end_activation_period
    end
  end
end

