class QuickPollStatsJob < ActiveJob::Base
  queue_as :default

  def perform(options={})
    quick_poll_id = options['quick_poll_id']
    quick_polls   = quick_poll_id.nil? ? QuickPoll.all.sort : QuickPoll.where(id: quick_poll_id)
    if quick_polls.any?
      quick_polls.each do |quick_poll|
        quick_poll_stat_attributes = {
          completed_panelists_count:        quick_poll.completed_panelists_count,
          questback_group_panelists_count:  quick_poll.questback_group_panelists_count,
          responded_to:                     quick_poll.quick_poll_responses.any?
        }

        QuickPollStat.find_or_initialize_by(quick_poll_id: quick_poll.id).update_attributes(quick_poll_stat_attributes)
      end
    end
  end
end
