class SurveySendNotificationsJob < ActiveJob::Base
  queue_as :push_notification

  def perform(options = {})
    sample = Sample.find options['sample_id']
    sample.send_notification_to_panelists(survey_id: options['survey_id']) if sample
  end

end
