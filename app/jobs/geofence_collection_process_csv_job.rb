class GeofenceCollectionProcessCsvJob < ActiveJob::Base
  queue_as :default

  def perform(options={})
    geofence_collection_id  = options['geofence_collection_id']
    geofence_collection     = GeofenceCollection.find_by id: geofence_collection_id
    geofence_collection.import
  end
end
