require 'log_exporter'

class LogExportsJob < ActiveJob::Base
  queue_as :log_export

  before_perform do |job|
    @delayed_job = Delayed::Job.where("handler like ?", "%#{job.job_id}%").first
  end

  def perform(options={})
    user_id       = options.delete("user_id")
    log_exporter  = LogExporter.new(user_id: user_id, options: options)
    log_exporter.generate_csv(@delayed_job.id)
  end
end
