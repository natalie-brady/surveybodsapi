function verifyVpnStudy(vpnStudy){
  submit_btn = $('#access_log_submit_btn');
  disabled_value = false;

  if (vpnStudy == "") {
    disabled_value = true;
  } else {
    disabled_value = false;
  }

  submit_btn.prop('disabled', disabled_value);
}

function calculateDateBeforeSubmit(submitBtn){
  oneDay      = 24*60*60*1000;
  startDate   = exportDateSelect('start_date');
  endDate     = exportDateSelect('end_date');

  dateDiff    = Math.round((endDate.getTime() - startDate.getTime()) / (oneDay))

  if(dateDiff <= 7 && dateDiff >= 0){
    submitBtn.closest('form').submit();
  } else {
    alert('Invalid date format or export dates exceeds 7 days.')
  }
}

function exportDateSelect(date){
  dateSelect  = $('select[id*=' + date + ']');
  monthSelect = dateSelect.siblings("select[id*=_2i]").val();
  yearSelect = dateSelect.siblings("select[id*=_1i]").val();
  daySelect = dateSelect.siblings("select[id*=_3i]").val();

  selectedDate = new Date(monthSelect + "/" + daySelect + "/" + yearSelect);

  return selectedDate;
}

$(document).ready(function() {
  var $accordion_container = $('.accordion-container');
  $accordion_container.find("tr").not('.accordion').hide();
  $accordion_container.find("tr").eq(0).show();

  $accordion_container.find(".accordion").click(function(){
    $accordion_container.find('.accordion').not(this).removeClass('info');
    $accordion_container.find('.accordion').not(this).siblings().removeClass("info");
    $accordion_container.find('.accordion').not(this).siblings().removeClass("text-wrap");
    $accordion_container.find('.accordion').not(this).siblings().fadeOut(500);
    $accordion_container.find('.accordion').removeClass("text-wrap");
    $(this).find('.url-p').removeClass('nowrap');
    $(this).addClass('info');
    $(this).addClass('text-wrap');
    $(this).siblings().addClass('info');
    $(this).siblings().fadeToggle(500);
    $accordion_container.find('.accordion').not(this).find('.url-p').addClass('nowrap');
  }).eq(0).trigger('click');
});
