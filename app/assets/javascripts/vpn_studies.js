$(document).on("click","#reloadVpnStudyBtn", function(event){
  var $elem  = $(this);
  $elem.attr("disabled", 'disabled');
  var path = $elem.data("path");
  //Add loader message while fetching data
  $(".data-loader").append("<span class='pull-right vpn-data-loading-msg'><img src='/loadingsb.gif'> Loading data, may take a few minutes ... </span>");
  $.get(path, function( data ) {
    $(".vpn-data-loading-msg").fadeOut();
    $elem.removeAttr('disabled');
  }).fail(function() {
    $elem.removeAttr('disabled');
    $(".data-loader").append("<span class='pull-right vpn-data-loading-msg'>An error occurred  </span> ");
    $(".vpn-data-loading-msg").fadeOut(5000);
  });
  event.preventDefault();
});

$( function() {
  $( ".ui-datepicker" ).datetimepicker({
    dateFormat: 'yy-mm-dd',
    timeFormat: 'HH:mm',
    controlType: 'select',
    minDate: 'today',
    maxDate: "+5Y",
    minTime: 'today'
  });
} );

$(document).ready(function(){
  $(".ui-datepicker").attr('readOnly', 'true');
});
