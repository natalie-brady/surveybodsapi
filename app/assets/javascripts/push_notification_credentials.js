// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/

function showPushNotificationForm(survey_data) {
  showUrl = "/surveys/" + survey_data.id + "/show_push_notification_form";
  $.get(showUrl, function(data){
    $(".modal-content").html(data);
    $("#ajaxModal").modal('show');
  });
};

$(document).ready(function() {
	$('#test-notification').click(function() {
		var data = prompt("test");
		$.get(window.location.href.split("#")[0] + "/test?data="+data)
	});
});
