function calculateIpAddressValue(selectedValue) {
  var bitMask = selectedValue.options[selectedValue.selectedIndex].value;
  var textValue = selectedValue.options[selectedValue.selectedIndex].text;
  var initialIp = $("#router_management_initial_ip_address").val();

  console.log(initialIp);

  $.ajax({
    url: "/router_managements/convert_ip_address",
    type: "GET",
    dataType: "json",
    data: { ip_address: initialIp, bit_mask: bitMask },
    complete: function() {},
    success: function(data){
      if(data.error == false) {
        $("#router_management_ip_address_range").val(data.ip_address);
        $("#router_management_net_mask").val(textValue);
      } else {
        alert(data.message);
      };
    },
    error: function() {
             alert("There is an error converting the IP. Please try again. By changing the bitmask to another value, then back to the original.");
           }
  });
}
