function updateType() {
	switch ($("#geofence_group_geofence_type").val()) {
		case "dwell":
			$("#dwell-time-container").show();
			break;
		default:
			$("#dwell-time-container").hide();
			break;
	}
}

function updateAction() {
	switch ($("#geofence_group_action").val()) {
		case "survey":
			$("#survey-id-container").show();
			$("#quick-poll-id-container").hide();
      if($("#survey-title-container").is(":hidden")){
			  $("#survey-title-container").show();
      }
			break;
		case "quick_poll":
			$("#survey-id-container").hide();
			$("#quick-poll-id-container").show();
      if($("#survey-title-container").is(":hidden")){
			  $("#survey-title-container").show();
      }
			break;
		case "passive":
			$("#survey-id-container").hide();
			$("#quick-poll-id-container").hide();
			$("#survey-title-container").hide();
			break;
	}
}

function geofenceGroupSubmitForm(filterStatus) {
  $(filterStatus).trigger('submit.rails');
}

$(document).ready(function() {
	$("#geofence_group_geofence_type").change(updateType);
	updateType();

	$("#geofence_group_action").change(updateAction);
	updateAction();
});
