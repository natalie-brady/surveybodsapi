function markerDrag(event) {
	markerUpdate(event.latLng, false);
}

function markerDragged(event) {
	markerUpdate(event.latLng, true);
}

function markerUpdate(latLng, reposition) {
	var latitude = latLng.lat();
	longitude = latLng.lng();
	var activeLocation = new google.maps.LatLng(latitude, longitude);
	$("#geofence_latitude").val(latitude);
	$("#geofence_longitude").val(longitude);
	$("#geofence_radius").val(activeFenceRadius.getRadius());
	if (reposition) {
		map.setCenter(activeLocation);
	}
	activeFenceRadius.setCenter(activeLocation);
}

function initMap() {
	var activeFence = $("#map").data("active-fence");
	var backgroundFences = $("#map").data("background-fences");

	if (activeFence != null) {
		latOrigin = activeFence["latitude"];
		lonOrigin = activeFence["longitude"];
		radOrigin = activeFence["radius"];
	}

	map = new google.maps.Map(document.getElementById("map"));
	mapInfowindow = new google.maps.InfoWindow();
	markers = [];

	if (backgroundFences.length != 0) {
		backgroundFences.forEach(function(fence) {
			addGeofence(fence["latitude"], fence["longitude"], fence["radius"], fence["icon"], fence["active"], fence["editable"], fence["name"], fence["description"], fence["link"]);
		});
	}
	if (activeFence != null) {
		addGeofence(activeFence["latitude"], activeFence["longitude"], activeFence["radius"], activeFence["icon"], activeFence["active"], activeFence["editable"], null, null, null);
	} else {
		var bounds = new google.maps.LatLngBounds();
		backgroundFences.forEach(function(fence) {
			bounds.extend(new google.maps.LatLng(fence["latitude"], fence["longitude"]));
		});
		map.fitBounds(bounds);
	}
	markerCluster = new MarkerClusterer(map, markers);

}

function addGeofence(latitude, longitude, radius, icon, active, editable, name, description, link) {
	var activeLocation = new google.maps.LatLng(latitude, longitude);
	var marker = new google.maps.Marker({
		position: activeLocation,
		draggable: editable,
		icon:icon
	});
	marker.addListener('drag', markerDrag);
	marker.addListener('dragend', markerDragged);

	if (active) {
		marker.setMap(map);
	} else {
		markers.push(marker);
	}


	if (active) {
  	var radiusCircle = new google.maps.Circle({
      center: activeLocation,
      radius: radius,
      strokeColor: active ? "#0000FF" : "#000000",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: active ? "#0000FF" : "#000000",
      fillOpacity: 0.2
    });
	  radiusCircle.setMap(map);
		map.fitBounds(radiusCircle.getBounds());
		activeFenceMarker = marker;
		activeFenceRadius = radiusCircle;
	} else {
		marker.addListener('click', function() {
			mapInfowindow.setContent(
				"<h1>"+name+"</h1>"+
				"<p>"+description+"</p>"+
				'<p><a href="'+link+'">View Geofence</a></p>'
				)
			mapInfowindow.open(map, marker);
		});
	}
}


function updateMap(latitude, longitude, radius, reposition) {
	var activeLocation = new google.maps.LatLng(latitude, longitude);
	activeFenceMarker.setPosition(activeLocation);
	activeFenceRadius.setCenter(activeLocation);
	activeFenceRadius.setRadius(radius);
	if (reposition) {
		map.setCenter(activeLocation);
	}
}

$(document).ready(function() {
	if ($("#map").size() != 0) {
		$("#geofence_latitude").change(function(){
			updateMap(parseFloat($(this).val()), activeFenceMarker.getPosition().lng(), activeFenceRadius.getRadius(), true);
		})
		$("#geofence_longitude").change(function(){
			updateMap(activeFenceMarker.getPosition().lat(), parseFloat($(this).val()), activeFenceRadius.getRadius(), true);
		})
		$("#geofence_radius").change(function(){
			updateMap(activeFenceMarker.getPosition().lat(), activeFenceMarker.getPosition().lng(), parseInt($(this).val()), true);
		})
		$("#reset-map").click(function(){
			var activeLocation = new google.maps.LatLng(latOrigin, lonOrigin);
			activeFenceMarker.setPosition(activeLocation);
			activeFenceRadius.setCenter(activeLocation);
			activeFenceRadius.setRadius(radOrigin);
			$("#geofence_latitude").val(latOrigin);
			$("#geofence_longitude").val(lonOrigin);
			$("#geofence_radius").val(radOrigin);

			updateMap(latOrigin, lonOrigin, radOrigin, true);
		})
	}
});

