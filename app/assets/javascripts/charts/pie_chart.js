$(document).ready(function() {
  $(".pie-canvas").each(function(index, element) {
    var b, c, chart_data, ctx, data, g, h, pie_chart, pie_data, r;
    chart_data = $(element).data('breakdown');
    data = [];
    for (pie_data in chart_data) {
      r = Math.floor(Math.random() * 200);
      g = Math.floor(Math.random() * 200);
      b = Math.floor(Math.random() * 200);
      c = 'rgb(' + r + ', ' + g + ', ' + b + ')';
      h = 'rgb(' + r + 20 + ', ' + g + 20 + ', ' + b + 20 + ')';
      data.push({
        value: chart_data[pie_data].value,
        label: chart_data[pie_data].label,
        color: c,
        highlight: c
      });
    }
    ctx = element.getContext("2d");
    pie_chart = new Chart(ctx).Pie(data, {
      tooltipTemplate: "<%= value %> : <%= Math.round(circumference / 6.283 * 100) %>%"
    });
    $(".chart-legend").get(index).innerHTML = "<h6>Legend :</h6>" + pie_chart.generateLegend();
  });
});

