$(document).ready(function() {
  generateBarChart();
});

function generateBarChart() {
  var overallCanvas, overallChart, overallData, overallCanvasData, canvasData, canvasTitles;
  overallCanvas     = $("#quick_poll_canvas");
  canvasData        = overallCanvas.data("completed-quick-polls");
  canvasTitles      = overallCanvas.data("title");
  overallCanvasData = [];

  for(title in canvasTitles){
    if(canvasData.hasOwnProperty(canvasTitles[title])) {
      overallCanvasData.push(canvasData[canvasTitles[title]]);
    } else {
      overallCanvasData.push(0);
    }
  };

  overallData = {
    labels: canvasTitles,
    datasets: [
      {
        fillColor: "rgba(27,163,200,0.5)",
        strokeColor: "rgba(27,163,200,1)",
        pointColor: "rgba(27,163,200,1)",
        pointStrokeColor: "#fff",
        data: overallCanvasData
      }
    ]
  };
  overallChart = new Chart(overallCanvas.get(0).getContext("2d")).Bar(overallData);
}

