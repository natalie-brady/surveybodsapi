# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every :day, at: '12am' do
  runner "QuestbackGroupsReloadJob.perform_later"
end

every :day, at: '12:30am' do
  runner "SurveysReloadJob.perform_later"
end

every :day, at: '1am' do
  runner "MasterDataReloadJob.perform_later"
end

every :day, at: '1:30am' do
  runner "ActiveSamplesReloadJob.perform_later"
end

every :day, at: '2am' do
  runner "SamplesReloadJob.perform_later"
end

every :day, at: '2:30am' do
  runner "QuickPollStatsJob.perform_later"
end

every :day, at: '4am' do
  rake 'statistics:create_or_update_weekly_data'
end
