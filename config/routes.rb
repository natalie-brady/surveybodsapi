Rails.application.routes.draw do
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'statistics#index'

  namespace :api, :defaults => { :format => :json } do
    resources :surveys do
      collection do
        get :completed
        get :profile
      end
      get :is_active, on: :member
    end
    resources :auth do
      post :login, on: :collection
      post :logout, on: :collection
      post :facebook, on: :collection
      post :password_reset, on: :collection
    end
    resources :profile do
      patch :change, on: :collection
      post :avatar, on: :collection
      post :refer_a_friend, on: :collection
    end
    resources :mappings do
      get :masterdata, on: :collection
    end
    resources :register
    resources :push_notification_credentials
    resources :faqs
    resources :quick_polls do
      get :results, on: :member
      get :completed, on: :collection
    end
    resources :geofences do
      post :logs, on: :collection
    end
    resources :log
    resources :vpn_studies do
      collection do
        get :active
        get :completed
        get :cancelled
        get :connected
      end
      member do
        get :is_active
      end
    end
    resources :vpn_study_credentials do
      post  :download, on: :collection
      post  :activate, on: :collection
      post  :disconnect, on: :collection
      post  :complete, on: :collection
      get   :completed, on: :collection
    end
    resources :statistics do
      get :panelists, on: :collection
    end
    resources :device_metadata
  end

  resources :panelists do
    get :export, on: :collection
    get :sync_last_accessed, on: :collection
    get :autocomplete_panelist_email, on: :collection
    resources :push_notification_credentials do
      get :test, on: :member
    end
  end
  resources :settings
  resources :profile_variables
  resources :users
  resources :master_data_descriptions do
    get :reload, on: :collection
  end
  resources :mail_templates do
    get :reload, on: :collection
  end
  resources :survey_categories
  resources :surveys do
    get   :reload, on: :collection
    post  :send_notifications, on: :member
    get   :show_push_notification_form, on: :member
  end
  resources :profile_surveys
  resources :faqs
  resources :quick_polls do
    get :send_notifications, on: :member
    get :autocomplete_quick_poll_title, on: :collection
  end
  resources :quick_poll_responses do
    get :export, on: :collection
  end
  resources :geofences do
    get :export, on: :collection
    get :info, on: :collection
  end
  resources :geofence_logs do
    post  :export, on: :collection
    get   :panelist_logs, on: :collection
  end
  resources :geofence_groups do
    get   :reload_apps, on: :collection
    get   :map, on: :member
    get   :export, on: :collection
    post  :status_update, on: :member
    get :autocomplete_geofence_group_name, on: :collection
  end
  resources :samples do
    get :reload, on: :collection
  end
  resources :questback_groups do
    get :reload, on: :collection
    get :search, on: :collection
    get :autocomplete_questback_group_name, on: :collection
  end
  resources :geofence_collections do
    post  :import, on: :collection
    get   :upload, on: :collection
  end
  resources :quick_poll_stats do
    get :update_stats, on: :collection
  end
  resources :vpn_studies do
    member do
      get :send_notifications
      get :reload_data
      get :deactivate
    end
    get :autocomplete_vpn_study_name, on: :collection
  end
  resources :usage_study_access_logs do
    post :export, on: :collection
  end
  resources :usage_study_users do
    get :access_logs, on: :member
    get :export, on: :collection
  end
  resources :router_managements do
    post  :disconnect, on: :collection
    post  :sync, on: :collection
    get   :convert_ip_address, on: :collection
  end
  resources :ssl_decryption_hosts do
    post :sync, on: :collection
  end
  resources :vpn_bandwidth_usages do
    get :export, on: :collection
  end
  resources :device_metadata do
    get :panelist, on: :collection
  end
  resources :log_exports do
    get :export, on: :collection
    get :download, on: :member
  end
  resources :geofence_log_exports do
    get :download, on: :member
  end
  resources :vpn_managements do
    get :vpn_speed_test, on: :collection
  end
  resources :geofence_uploads do
    get :export_template, on: :collection
  end
  resources :bart_settings do
    get :export_csv_file, on: :collection
  end
  resources :panelist_stats do
  end

  resources :admin
  resources :push_notification_logs
  resources :statistics
  resources :panelist_sync_logs
  resources :vpn_routes
  resources :decrypt_hosts
  resources :vpn_page_monitorings
  resources :vpn_page_monitoring_results
  resources :vpn_study_credentials
  resources :logs
  resources :bart_audit_logs

  # This will redirect all missing routes to "/"
  get '*path' => redirect('/')

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
