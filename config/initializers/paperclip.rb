Paperclip.options[:content_type_mappings] = {
  mobileconfig: %w(application/xml),
  ovpn: %w(text/plain),
  crt: %w(text/plain),
  csv: %w(text/plain text/csv application/vnd.ms-excel)
}
