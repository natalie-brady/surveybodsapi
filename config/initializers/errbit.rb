Airbrake.configure do |config|
  config.host = 'http://monit.researchbods.com'
  config.project_id = 1 # required, but any positive integer works
  config.project_key = '772eddbdf814f01b69fd4a193cdd5550'

  # Uncomment for Rails apps
  config.environment = Rails.env
  config.ignore_environments = %w(development test)
end
