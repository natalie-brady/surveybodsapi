# config valid only for Capistrano 3.1
lock '3.4.0'

set :application, 'surveybodsapi'

set :repo_url, 'git@bitbucket.org:researchbods-team/surveybodsapi.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/var/www/surveybods.com'
# set :deploy_to, '/var/www/my_app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{ log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/templates }

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

#set :public_system_dir, "#{deploy_to}/shared/system"

#before 'deploy:restart', 'deploy:symlink_public_system'

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

set :delayed_job_queues, ['default',
                          'geofence_logs',
                          'push_notification',
                          'vpn_study_push_notification',
                          'log_export',
                          'geofence_log_export',
                          'vpn_page_speed_test',
                          'bart_setting']

# TODO fix db:migrate on deployment
after "deploy", "deploy:migrate"
#after "deploy", "delayed_job:restart"

before "deploy", 'monit_delayed_job:stop'

after 'deploy:published', 'restart' do
  invoke 'monit_delayed_job:start'
end


namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :mkdir, '-p', release_path.join('tmp/')
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  # desc "Symlink the public/system dir"
  # task :symlink_public_system do
  #   run "ln -s #{:public_system_dir} #{release_path}/public/system"
  # end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
