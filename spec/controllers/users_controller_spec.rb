require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:user)       { create(:user) }
  let!(:user_keys)  do
    [
      "id",
      "email",
      "encrypted_password",
      "reset_password_token",
      "reset_password_sent_at",
      "remember_created_at",
      "sign_in_count",
      "current_sign_in_at",
      "last_sign_in_at",
      "current_sign_in_ip",
      "last_sign_in_ip",
      "created_at",
      "updated_at"
    ]
  end

  describe "#GET index" do
    it 'assigns users' do
      get :index
      expect(assigns(:users)).to include(user)
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe "#GET show" do
    it "assigns the requested user to user" do
      get :show, id: user
      expect(assigns(:user)).to eq(user)
    end

    it "renders the show view" do
      get :show, id: user
      expect(response).to render_template(:show)
    end
  end

  describe "#GET new" do
    it "assigns the user to the new user" do
      @new_user = User.new
      get :new
      expect(assigns(:user)).to be_a(User)
      expect(assigns(:user).attributes).to eq(@new_user.attributes)
      expect(assigns(:user).attributes.keys).to eql(user_keys)
    end

    it "renders the new view" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe " #POST create" do
    it do
      expect{
        post :create, user: FactoryGirl.attributes_for(:user)
      }.to change(User, :count).by(1)
    end

    it 'redirects to the new user' do
      post :create, user: FactoryGirl.attributes_for(:user)
      expect(response).to redirect_to(User.last)
    end
  end

  describe "#PUT update" do
    it "locates the requested user" do
      put :update, id: user, user: FactoryGirl.attributes_for(:user)
      expect(assigns(:user)).to eq(user)
    end

    it "changes the @user attributes" do
      put :update, id: user, user: FactoryGirl.attributes_for(:user, email: "test@gmail.com")
      user.reload
      expect(user.email).to eq("test@gmail.com")
    end

    it 'redirects to the updated user' do
      put :update, id: user, user: FactoryGirl.attributes_for(:user)
      expect(response).to redirect_to(user)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the user" do
      expect{
        delete :destroy, id: user
      }.to change(User, :count).by(-1)
    end

    it "redirects to the user#index" do
      delete :destroy, id: user
      expect(response).to redirect_to(users_url)
    end
  end
end
