require 'rails_helper'

RSpec.describe ProfileVariablesController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:profile_variable) { create(:profile_variable) }

  describe "#GET index" do
    it 'assigns profile_variables' do
      get :index
      expect(assigns(:profile_variables)).to eq([profile_variable])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe "#GET show" do
    it "assigns the requested profile_variable to profile_variable" do
      get :show, id: profile_variable
      expect(assigns(:profile_variable)).to eq(profile_variable)
    end

    it "renders the show view" do
      get :show, id: profile_variable
      expect(response).to render_template(:show)
    end
  end

  describe "#GET new" do
    it "assigns the profile_variable to the new profile_variable" do
      @profile_variable = ProfileVariable.new
      get :new
      expect(assigns(:profile_variable)).to be_a(ProfileVariable)
      expect(assigns(:profile_variable).attributes).to eq(@profile_variable.attributes)
      expect(assigns(:profile_variable).attributes.keys).to eql(['id', 'name', 'description', 'value', 'created_at', 'updated_at'])
    end

    it "renders the new view" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe " #POST create" do
    it do
      expect{
        post :create, profile_variable: FactoryGirl.attributes_for(:profile_variable)
      }.to change(ProfileVariable, :count).by(1)
    end

    it 'redirects to the new profile_variable' do
      post :create, profile_variable: FactoryGirl.attributes_for(:profile_variable)
      expect(response).to redirect_to(ProfileVariable.last)
    end
  end

  describe "#PUT update" do
    it "locates the requested profile_variable" do
      put :update, id: profile_variable, profile_variable: FactoryGirl.attributes_for(:profile_variable)
      expect(assigns(:profile_variable)).to eq(profile_variable)
    end

    it "changes the @profile_variable attributes" do
      new_profile_variable_attributes = {
        name: 'New Profile Variable Name',
        description: 'New Description',
        value: 'New Value'
      }
      put :update, id: profile_variable, profile_variable: FactoryGirl.attributes_for(:profile_variable, new_profile_variable_attributes)
      profile_variable.reload
      expect(profile_variable.name).to eq("New Profile Variable Name")
      expect(profile_variable.description).to eq("New Description")
      expect(profile_variable.value).to eq("New Value")
    end

    it 'redirects to the updated profile_variable' do
      put :update, id: profile_variable, profile_variable: FactoryGirl.attributes_for(:profile_variable)
      expect(response).to redirect_to(profile_variable)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the profile_variable" do
      expect{
        delete :destroy, id: profile_variable
      }.to change(ProfileVariable, :count).by(-1)
    end

    it "redirects to the profile_variable#index" do
      delete :destroy, id: profile_variable
      expect(response).to redirect_to(profile_variables_url)
    end
  end

end
