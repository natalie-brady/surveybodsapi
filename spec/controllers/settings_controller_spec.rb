require 'rails_helper'

RSpec.describe SettingsController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:setting) { create(:setting) }

  describe "#GET index" do
    it 'redirect to setting' do
      get :index
      expect(response).to redirect_to(setting)
    end
  end

  describe "#GET show" do
    it "assigns the requested setting to setting" do
      get :show, id: setting
      expect(assigns(:setting)).to eq(setting)
    end

    it "renders the show view" do
      get :show, id: setting
      expect(response).to render_template(:show)
    end

    context 'no delayed_job' do
      it do
        get :show, id: setting
        expect(assigns(:reload_in_progress)).to eq(false)
      end
    end
  end

  describe "#PUT update" do
    it "locates the requested setting" do
      put :update, id: setting, setting: FactoryGirl.attributes_for(:setting)
      expect(assigns(:setting)).to eq(setting)
    end

    it "changes the @setting attributes" do
      new_setting_attributes = {
        qb_hostname:                   "localhost:3000",
        qb_api_username:               "new_user_name",
        qb_api_password:               "new_password",
        auth_timeout_seconds:          6000,
        site_id:                       123,
        mail_template_id:              234,
        apn_cert:                      "345",
        apn_passphrase:                "new_passphrase",
        survey_category_id:            567,
        questback_external_service_id: 678,
        gcm_key:                       "new_gcm_key",
        android_minimum_version:       2,
        ios_minimum_version:           2,
        refer_promotion_id:            "12312354"
      }
      put :update, id: setting, setting: FactoryGirl.attributes_for(:setting, new_setting_attributes)
      setting.reload
      expect(setting.qb_hostname).to eq("localhost:3000")
      expect(setting.qb_api_username).to eq("new_user_name")
      expect(setting.qb_api_password).to eq("new_password")
      expect(setting.auth_timeout_seconds).to eq(6000)
      expect(setting.site_id).to eq(123)
      expect(setting.mail_template_id).to eq(234)
      expect(setting.apn_cert).to eq('345')
      expect(setting.apn_passphrase).to eq('new_passphrase')
      expect(setting.survey_category_id).to eq(567)
      expect(setting.questback_external_service_id).to eq(678)
      expect(setting.gcm_key).to eq("new_gcm_key")
      expect(setting.android_minimum_version).to eq(2)
      expect(setting.ios_minimum_version).to eq(2)
      expect(setting.refer_promotion_id).to eq("12312354")
    end

    it 'redirects to the updated setting' do
      put :update, id: setting, setting: FactoryGirl.attributes_for(:setting)
      expect(response).to redirect_to(setting)
    end
  end

end
