require 'rails_helper'

RSpec.describe ProfileSurveysController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:profile_survey) { create(:profile_survey) }

  describe "#GET index" do
    it 'assigns profile_surveys' do
      get :index
      expect(assigns(:profile_surveys)).to eq([profile_survey])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe "#GET show" do
    it "assigns the requested profile_survey to profile_survey" do
      get :show, id: profile_survey
      expect(assigns(:profile_survey)).to eq(profile_survey)
    end

    it "renders the show view" do
      get :show, id: profile_survey
      expect(response).to render_template(:show)
    end
  end

  describe "#GET new" do
    it "assigns the profile_survey to the new profile_survey" do
      @profile_survey = ProfileSurvey.new
      get :new
      expect(assigns(:profile_survey)).to be_a(ProfileSurvey)
      expect(assigns(:profile_survey).attributes).to eq(@profile_survey.attributes)
      expect(assigns(:profile_survey).attributes.keys).to eql(['id', 'name', 'survey_id', 'created_at', 'updated_at'])
    end

    it "renders the new view" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe " #POST create" do
    it do
      expect{
        post :create, profile_survey: FactoryGirl.attributes_for(:profile_survey)
      }.to change(ProfileSurvey, :count).by(1)
    end

    it 'redirects to the new profile_survey' do
      post :create, profile_survey: FactoryGirl.attributes_for(:profile_survey)
      expect(response).to redirect_to(ProfileSurvey.last)
    end
  end

  describe "#PUT update" do
    it "locates the requested profile_survey" do
      put :update, id: profile_survey, profile_survey: FactoryGirl.attributes_for(:profile_survey)
      expect(assigns(:profile_survey)).to eq(profile_survey)
    end

    it "changes the @profile_survey attributes" do
      new_profile_survey_attributes = {
        name: 'New Profile Survey Name'
      }
      put :update, id: profile_survey, profile_survey: FactoryGirl.attributes_for(:profile_survey, new_profile_survey_attributes)
      profile_survey.reload
      expect(profile_survey.name).to eq("New Profile Survey Name")
    end

    it 'redirects to the updated profile_survey' do
      put :update, id: profile_survey, profile_survey: FactoryGirl.attributes_for(:profile_survey)
      expect(response).to redirect_to(profile_survey)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the profile_survey" do
      expect{
        delete :destroy, id: profile_survey
      }.to change(ProfileSurvey, :count).by(-1)
    end

    it "redirects to the profile_survey#index" do
      delete :destroy, id: profile_survey
      expect(response).to redirect_to(profile_surveys_url)
    end
  end

end
