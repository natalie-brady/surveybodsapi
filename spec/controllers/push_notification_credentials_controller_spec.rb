require 'rails_helper'

RSpec.describe PushNotificationCredentialsController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:push_notification_credential)   { create(:push_notification_credential) }
  let!(:panelist)                       { create(:panelist, questback_auth_code: false) }

  describe "#GET show" do
    it "assigns the requested push_notification_credential to push_notification_credential" do
      get :show, id: push_notification_credential, panelist_id: panelist.id
      expect(assigns(:push_notification_credential)).to eq(push_notification_credential)
    end

    it "renders the show view" do
      get :show, id: push_notification_credential, panelist_id: panelist.id
      expect(response).to render_template(:show)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the push_notification_credential" do
      expect{
        delete :destroy, id: push_notification_credential, panelist_id: panelist.id
      }.to change(PushNotificationCredential, :count).by(-1)
    end

    it "redirects to the push_notification_credential#index" do
      delete :destroy, id: push_notification_credential, panelist_id: panelist.id
      expect(response).to redirect_to(panelist_path(panelist))
    end
  end

end
