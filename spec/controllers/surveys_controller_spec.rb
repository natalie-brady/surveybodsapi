require 'rails_helper'

RSpec.describe SurveysController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:survey) { create(:survey) }
  let!(:sample) { create(:sample) }

  describe "#GET index" do
    it 'assigns surveys' do
      get :index
      expect(assigns(:surveys)).to eq([survey])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end

    context 'no delayed_job' do
      it do
        get :index
        expect(assigns(:reload_in_progress)).to eq(false)
        expect(assigns(:reload_status)).to eq("")
      end
    end
  end

  describe "#GET show" do
    it "assigns the requested survey to survey" do
      get :show, id: survey
      expect(assigns(:survey)).to eq(survey)
    end

    it "renders the show view" do
      get :show, id: survey
      expect(response).to render_template(:show)
    end
  end

  describe "#GET reload" do
    it 'add a delayed_job to process the reload' do
      expect{
        xhr :get, :reload
      }.to change(Delayed::Job, :count).by(1)
    end

    it 'responds as json' do
      xhr :get, :reload
      expect(response.content_type).to eq('text/javascript')
    end
  end

  describe "#GET send_notifications" do
    it "add a delayed_job to send notifications" do
      expect{
        get :send_notifications, id: survey, sample_ids: [ sample.sample_id ]
      }.to change(Delayed::Job, :count).by(1)
    end

    it "redirects to survey index page" do
      get :send_notifications, id: survey, sample_ids: [ sample.sample_id ]
      expect(response).to redirect_to(surveys_url)
    end
  end

  describe "#PUT update" do
    it "locates the requested survey" do
      put :update, id: survey, survey: FactoryGirl.attributes_for(:survey)
      expect(assigns(:survey)).to eq(survey)
    end

    it "changes the @survey attributes" do
      new_survey_attributes = {
        active: true,
        registration_survey: true,
        duration: '10 seconds',

      }
      put :update, id: survey, survey: FactoryGirl.attributes_for(:survey, new_survey_attributes)
      survey.reload
      expect(survey.active).to eq(true)
      expect(survey.registration_survey).to eq(true)
      expect(survey.duration).to eq("10 seconds")
    end

    it 'redirects to the updated survey' do
      put :update, id: survey, survey: FactoryGirl.attributes_for(:survey)
      expect(response).to redirect_to(survey)
    end
  end

end
