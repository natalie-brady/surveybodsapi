require 'rails_helper'

RSpec.describe MasterDataDescriptionsController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:master_data_description) { create(:master_data_description) }

  describe "#GET index" do
    it 'assigns master_data_description' do
      get :index
      expect(assigns(:master_data_descriptions)).to eq([master_data_description])
      expect(assigns(:delayed_job)).to be_empty
      expect(assigns(:reload_in_progress)).to eql(false)
      expect(assigns(:reload_status)).to eq("")
    end
  end

  describe "#GET reload" do
    it 'add a delayed_job to process the reload' do
      expect{
        xhr :get, :reload
      }.to change(Delayed::Job, :count).by(1)
      expect(assigns(:master_data_descriptions)).to eq([master_data_description])
    end

    it 'responds as json' do
      xhr :get, :reload
      expect(response.content_type).to eq('text/javascript')
    end
  end

end
