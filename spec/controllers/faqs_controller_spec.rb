require 'rails_helper'

RSpec.describe FaqsController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:faq) { create(:faq) }

  describe "#GET index" do
    it 'assigns faqs' do
      get :index
      expect(assigns(:faqs)).to eq([faq])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe "#GET show" do
    it "assigns the requested faq to faq" do
      get :show, id: faq
      expect(assigns(:faq)).to eq(faq)
    end

    it "renders the show view" do
      get :show, id: faq
      expect(response).to render_template(:show)
    end
  end

  describe "#GET new" do
    it "assigns the faq to the new faq" do
      @faq = Faq.new
      get :new
      expect(assigns(:faq)).to be_a(Faq)
      expect(assigns(:faq).attributes).to eq(@faq.attributes)
      expect(assigns(:faq).attributes.keys).to eql(['id', 'question', 'answer', 'created_at', 'updated_at'])
    end

    it "renders the new view" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe " #POST create" do
    it do
      expect{
        post :create, faq: FactoryGirl.attributes_for(:faq)
      }.to change(Faq, :count).by(1)
    end

    it 'redirects to the new faq' do
      post :create, faq: FactoryGirl.attributes_for(:faq)
      expect(response).to redirect_to(Faq.last)
    end
  end

  describe "#PUT update" do
    it "locates the requested faq" do
      put :update, id: faq, faq: FactoryGirl.attributes_for(:faq)
      expect(assigns(:faq)).to eq(faq)
    end

    it "changes the @faq attributes" do
      put :update, id: faq, faq: FactoryGirl.attributes_for(:faq, question: "question 1", answer: "answer 1")
      faq.reload
      expect(faq.question).to eq("question 1")
      expect(faq.answer).to eq("answer 1")
    end

    it 'redirects to the updated faq' do
      put :update, id: faq, faq: FactoryGirl.attributes_for(:faq)
      expect(response).to redirect_to(faq)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the faq" do
      expect{
        delete :destroy, id: faq
      }.to change(Faq, :count).by(-1)
    end

    it "redirects to the faq#index" do
      delete :destroy, id: faq
      expect(response).to redirect_to(faqs_url)
    end
  end
end
