require 'rails_helper'

RSpec.describe SurveyCategoriesController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:survey_category) { create(:survey_category) }

  describe "#GET index" do
    it 'assigns survey_categories' do
      get :index
      expect(assigns(:survey_categories)).to eq([survey_category])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe "#GET show" do
    it "assigns the requested survey_category to survey_category" do
      get :show, id: survey_category
      expect(assigns(:survey_category)).to eq(survey_category)
    end

    it "renders the show view" do
      get :show, id: survey_category
      expect(response).to render_template(:show)
    end
  end

  describe "#GET new" do
    it "assigns the survey_category to the new survey_category" do
      @survey_category      = SurveyCategory.new
      survey_category_keys  = ['id', 'name', 'description', 'color', 'created_at', 'updated_at']

      get :new

      expect(assigns(:survey_category)).to be_a(SurveyCategory)
      expect(assigns(:survey_category).attributes).to eq(@survey_category.attributes)
      expect(assigns(:survey_category).attributes.keys).to eql(survey_category_keys)
    end

    it "renders the new view" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe " #POST create" do
    it do
      expect{
        post :create, survey_category: FactoryGirl.attributes_for(:survey_category)
      }.to change(SurveyCategory, :count).by(1)
    end

    it 'redirects to the new survey_category' do
      post :create, survey_category: FactoryGirl.attributes_for(:survey_category)
      expect(response).to redirect_to(SurveyCategory.last)
    end
  end

  describe "#PUT update" do
    it "locates the requested survey_category" do
      put :update, id: survey_category, survey_category: FactoryGirl.attributes_for(:survey_category)
      expect(assigns(:survey_category)).to eq(survey_category)
    end

    it "changes the @survey_category attributes" do
      new_survey_category_attributes = {
        name: 'Test Survey Category',
        description: 'Test Survey Category Description',
        color: '#FFFFFF'

      }
      put :update, id: survey_category, survey_category: FactoryGirl.attributes_for(:survey_category, new_survey_category_attributes)
      survey_category.reload
      expect(survey_category.name).to eq('Test Survey Category')
      expect(survey_category.description).to eq('Test Survey Category Description')
      expect(survey_category.color).to eq("#FFFFFF")
    end

    it 'redirects to the updated survey_category' do
      put :update, id: survey_category, survey_category: FactoryGirl.attributes_for(:survey_category)
      expect(response).to redirect_to(survey_category)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the survey_category" do
      expect{
        delete :destroy, id: survey_category
      }.to change(SurveyCategory, :count).by(-1)
    end

    it "redirects to the survey_category#index" do
      delete :destroy, id: survey_category
      expect(response).to redirect_to(survey_categories_url)
    end
  end

end
