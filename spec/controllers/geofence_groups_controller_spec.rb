require 'rails_helper'

RSpec.describe GeofenceGroupsController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:geofence_group) { create(:geofence_group) }
  let!(:geofence_group_params) do
    [
      "id",
      "name",
      "description",
      "created_at",
      "updated_at",
      "questback_group_id",
      "geofence_type",
      "dwell_time",
      "survey_id",
      "quick_poll_id",
      "message",
      "action",
      "survey_title"
    ]
  end


  describe "#GET index" do
    it 'assigns @geofence_groups' do
      get :index
      expect(assigns(:geofence_groups)).to eq([geofence_group])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe "#GET show" do
    it "assigns the requested geofence_group to @geofence_group" do
      get :show, id: geofence_group
      expect(assigns(:geofence_group)).to eq(geofence_group)
    end

    it "renders the show view" do
      get :show, id: geofence_group
      expect(response).to render_template(:show)
    end
  end

  describe "#GET new" do
    it "assigns the geofence_group to the new geofence_group" do
      @geofence_group = GeofenceGroup.new
      get :new
      expect(assigns(:geofence_group)).to be_a(GeofenceGroup)
      expect(assigns(:geofence_group).attributes).to eq(@geofence_group.attributes)
      expect(assigns(:geofence_group).attributes.keys).to eql(geofence_group_params)
    end

    it "renders the new view" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe " #POST create" do
    it do
      expect{
        post :create, geofence_group: FactoryGirl.attributes_for(:geofence_group)
      }.to change(GeofenceGroup, :count).by(1)
    end

    it 'redirects to the new geofence_group' do
      post :create, geofence_group: FactoryGirl.attributes_for(:geofence_group)
      expect(response).to redirect_to(GeofenceGroup.last)
    end
  end

  describe "#PUT update" do
    let!(:new_geofence_attributes) do
      {
        name:           'Test Name',
        description:    'Test Description',
        geofence_type:  'exit',
        message:        'Test Message',
        action:         'quick_poll'
      }
    end
    it "locates the requested geofence_group" do
      put :update, id: geofence_group, geofence_group: FactoryGirl.attributes_for(:geofence_group)
      expect(assigns(:geofence_group)).to eq(geofence_group)
    end

    it "changes the @geofence_group attributes" do
      put :update, id: geofence_group, geofence_group: FactoryGirl.attributes_for(:geofence_group, new_geofence_attributes)
      geofence_group.reload
      expect(geofence_group.name).to eq('Test Name')
      expect(geofence_group.description).to eq('Test Description')
      expect(geofence_group.geofence_type).to eq('exit')
      expect(geofence_group.message).to eq('Test Message')
      expect(geofence_group.action).to eq('quick_poll')
    end

    it 'redirects to the updated geofence_group' do
      put :update, id: geofence_group, geofence_group: FactoryGirl.attributes_for(:geofence_group)
      expect(response).to redirect_to(geofence_group)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the geofence_group" do
      expect{
        delete :destroy, id: geofence_group
      }.to change(GeofenceGroup, :count).by(-1)
    end

    it "redirects to the geofence_group#index" do
      delete :destroy, id: geofence_group
      expect(response).to redirect_to(geofence_groups_url)
    end
  end

  describe '#GET map' do
    context "without geofences" do
      it do
        get :map, id: geofence_group.id
        expect(assigns(:geofence_group)).to eq(geofence_group)
        expect(assigns(:map_data)).to include({ active_fence: nil, background_fences: []})
      end
    end

    context "with geofences" do
      before do
        geofence_group.geofences = build_list(:geofence, 1)
      end

      let!(:geofence)               { geofence_group.geofences.first }
      let!(:map_data_keys)          { [:active_fence, :background_fences] }
      let!(:background_fences) do
        [{
          icon:         ActionController::Base.helpers.asset_path("rb_icon.png"),
          latitude:     geofence.latitude,
          longitude:    geofence.longitude,
          radius:       geofence.radius,
          active:       false,
          editable:     false,
          name:         geofence.name,
          description:  geofence.description,
          link:         "/geofences/#{geofence.id}"
        }]
      end

      it do
        get :map, id: geofence_group.id

        map_data = assigns(:map_data)

        expect(map_data).to have_key(:active_fence)
        expect(map_data).to have_key(:background_fences)
        expect(map_data.keys).to eql(map_data_keys)
        expect(map_data).to include({ active_fence: nil, background_fences: background_fences})
        expect(map_data[:background_fences]).to be_an(Array)
      end
    end
  end

end
