require 'rails_helper'

RSpec.describe GeofencesController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:geofence) { create(:geofence) }
  let!(:geofence_params) do
    [
      "id",
      "name",
      "description",
      "latitude",
      "longitude",
      "radius",
      "delay",
      "created_at",
      "updated_at",
      "geofence_group_id",
    ]
  end

  describe "#GET index" do
    it 'assigns @geofences' do
      get :index
      expect(assigns(:geofences)).to eq([geofence])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe "#GET show" do
    it "assigns the requested geofence to @geofence" do
      get :show, id: geofence
      @map_data = assigns(:map_data)
      expect(assigns(:geofence)).to eq(geofence)
      expect(@map_data).to have_key(:active_fence)
      expect(@map_data).to have_key(:background_fences)
    end

    it "renders the show view" do
      get :show, id: geofence
      expect(response).to render_template(:show)
    end
  end

  describe "#GET new" do
    it "assigns the geofence to the new geofence" do
      @geofence_group     = FactoryGirl.create(:geofence_group)
      @geofence           = Geofence.new
      @geofence.latitude  = 53.79172136223823
      @geofence.longitude = -1.5513714951065367
      @geofence.radius    = 50
      get :new, geofence_group_id: @geofence_group.id

      @map_data = assigns(:map_data)
      @geofence = assigns(:geofence)

      expect(@geofence).to be_a(Geofence)
      expect(@geofence.attributes).to eq(@geofence.attributes)
      expect(@geofence.attributes.keys).to eql(geofence_params)
      expect(@map_data).to have_key(:active_fence)
      expect(@map_data).to have_key(:background_fences)
      expect(assigns(:geofence_group)).to eq(@geofence_group)
    end

    it "renders the new view" do
      get :new
      expect(response).to render_template(:new)
    end
  end

  describe " #POST create" do
    it do
      expect{
        post :create, geofence: FactoryGirl.attributes_for(:geofence)
      }.to change(Geofence, :count).by(1)
    end

    it 'redirects to the new geofence' do
      post :create, geofence: FactoryGirl.attributes_for(:geofence)
      expect(response).to redirect_to(Geofence.last)
    end
  end

  describe "#PUT update" do
    let!(:new_geofence_attributes) do
      {
        name:           'Test Name',
        description:    'Test Description',
      }
    end
    it "locates the requested geofence" do
      put :update, id: geofence, geofence: FactoryGirl.attributes_for(:geofence)
      expect(assigns(:geofence)).to eq(geofence)
    end

    it "changes the @geofence attributes" do
      put :update, id: geofence, geofence: FactoryGirl.attributes_for(:geofence, new_geofence_attributes)
      geofence.reload
      expect(geofence.name).to eq('Test Name')
      expect(geofence.description).to eq('Test Description')
    end

    it 'redirects to the updated geofence' do
      put :update, id: geofence, geofence: FactoryGirl.attributes_for(:geofence)
      expect(response).to redirect_to(geofence)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the geofence" do
      expect{
        delete :destroy, id: geofence
      }.to change(Geofence, :count).by(-1)
    end

    it "redirects to the geofence#index" do
      delete :destroy, id: geofence
      expect(response).to redirect_to(geofences_url)
    end
  end

end
