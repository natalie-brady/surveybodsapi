require 'rails_helper'

RSpec.describe MailTemplatesController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  describe "#GET reload" do
    it 'add a delayed_job to process the reload' do
      expect{
        xhr :get, :reload
      }.to change(Delayed::Job, :count).by(1)
    end

    it 'responds as json' do
      xhr :get, :reload
      expect(response.content_type).to eq('text/javascript')
    end
  end

end
