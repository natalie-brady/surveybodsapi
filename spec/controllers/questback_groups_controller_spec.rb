require 'rails_helper'

RSpec.describe QuestbackGroupsController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:questback_group) { create(:questback_group) }

  describe "#GET index" do
    it 'assigns questback_group' do
      get :index
      expect(assigns(:questback_groups)).to eq([questback_group])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end

    context 'no delayed_job' do
      it do
        get :index
        expect(assigns(:reload_in_progress)).to eq(false)
        expect(assigns(:reload_status)).to eq("")
      end
    end
  end

  describe "#GET show" do
    it "assigns the requested questback_group to questback_group" do
      get :show, id: questback_group
      expect(assigns(:questback_group)).to eq(questback_group)
    end

    it "renders the show view" do
      get :show, id: questback_group
      expect(response).to render_template(:show)
    end
  end

  describe "#GET reload" do
    it 'add a delayed_job to process the reload' do
      expect{
        xhr :get, :reload
      }.to change(Delayed::Job, :count).by(1)
    end

    it 'responds as json' do
      xhr :get, :reload
      expect(response.content_type).to eq('text/javascript')
    end
  end

end
