require 'rails_helper'

RSpec.describe PanelistsController, type: :controller do

  before(:each) do
    @user = create(:user)
    sign_in :user, @user
  end

  let!(:panelist) { create(:panelist, questback_auth_code: false) }

  describe "#GET index" do
    it 'assigns panelist' do
      get :index
      expect(assigns(:panelists)).to eq([panelist])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template(:index)
    end
  end

  describe "#GET show" do
    it "assigns the requested panelist to panelist" do
      get :show, id: panelist
      expect(assigns(:panelist)).to eq(panelist)
    end

    it "renders the show view" do
      get :show, id: panelist
      expect(response).to render_template(:show)
    end
  end

  describe "#DELETE destroy" do
    it "deletes the panelist" do
      expect{
        delete :destroy, id: panelist
      }.to change(Panelist, :count).by(-1)
    end

    it "redirects to the panelist#index" do
      delete :destroy, id: panelist
      expect(response).to redirect_to(panelists_url)
    end
  end
end
