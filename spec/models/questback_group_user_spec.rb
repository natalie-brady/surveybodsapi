require 'rails_helper'

RSpec.describe QuestbackGroupUser, type: :model do

  context 'associations' do
    it { should belong_to(:questback_group) }
    it { should belong_to(:panelist) }
  end

end
