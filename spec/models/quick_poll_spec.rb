require 'rails_helper'

RSpec.describe QuickPoll, type: :model do

  let!(:quick_poll) { create(:quick_poll) }

  subject{ quick_poll }

  context 'associations' do
    it { should belong_to(:questback_group) }
    it { should have_many(:quick_poll_questions)}
    it { should have_many(:quick_poll_choices).through(:quick_poll_questions)  }
    it { should have_many(:quick_poll_responses).through(:quick_poll_choices)  }
    it { should have_one(:quick_poll_stat) }
    it do
      should have_many(:completed_panelists)
        .through(:quick_poll_responses)
        .source(:panelist)
    end
  end

  context 'enums' do
    it { should define_enum_for(:quick_poll_type)}
  end

  context 'validations' do
    it { should accept_nested_attributes_for(:quick_poll_questions) }
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
  end

  context 'methods' do
    context 'to_s' do
      it { expect(subject.to_s).to eql(subject.title) }
    end

    context 'questions' do
      it { expect(subject.questions).to eql(subject.quick_poll_questions) }
    end

    context 'completed_panelists_count' do
      it { expect(subject.completed_panelists_count).to eql(subject.completed_panelists.uniq.count) }
    end

    context 'get_poll_info' do
      it { expect(subject.get_poll_info).to include({
            "quick_poll" => {
              "id"            => subject.id,
              "title"         => subject.title,
              "description"   => subject.description,
              "duration"      => subject.duration,
              "bonus_points"  => subject.bonus_points.to_i,
              "tickets"       => subject.tickets.to_i,
              "start_time"    => subject.start_time.to_i,
              "end_time"      => subject.end_time.to_i,
              "questions"     => []
            }
        })
      }
    end

    context 'poll_info' do
      context 'without_questions' do
        let!(:poll_info)      { subject.poll_info(true) }
        let!(:poll_info_keys) { ["id","title","description","duration","bonus_points","tickets","start_time","end_time"] }
        let!(:quick_poll_info) do
          {
            "id"            => subject.id,
            "title"         => subject.title,
            "description"   => subject.description,
            "duration"      => subject.duration,
            "bonus_points"  => subject.bonus_points.to_i,
            "tickets"       => subject.tickets.to_i,
            "start_time"    => subject.start_time.to_i,
            "end_time"      => subject.end_time.to_i,
          }
        end

        it { expect(poll_info).to include(quick_poll_info) }
        it { expect(poll_info).not_to have_key("questions") }
        it { expect(poll_info.keys).to eql(poll_info_keys)}
      end

      context 'with_questions' do
        before do
          subject.quick_poll_questions = build_list(:quick_poll_question, 1)
          subject.save!
        end

        let!(:quick_poll_question)  { subject.quick_poll_questions.first }
        let!(:poll_info)            { subject.poll_info }
        let!(:poll_info_keys)       { ["id", "title", "description", "duration", "bonus_points", "tickets", "start_time", "end_time", "questions"] }
        let!(:quick_poll_info) do
          {
            "id"            => subject.id,
            "title"         => subject.title,
            "description"   => subject.description,
            "duration"      => subject.duration,
            "bonus_points"  => subject.bonus_points.to_i,
            "tickets"       => subject.tickets.to_i,
            "start_time"    => subject.start_time.to_i,
            "end_time"      => subject.end_time.to_i,
            "questions"     => [{
              "question_type"   => quick_poll_question.question_type,
              "question"        => quick_poll_question.question,
              "randomize_order" => quick_poll_question.randomize_order,
              "id"              => quick_poll_question.id,
              "answers"         => []
            }]
          }
        end

        it { expect(poll_info).to have_key("questions") }
        it { expect(poll_info.keys).to eql(poll_info_keys) }
        it { expect(poll_info).to include(quick_poll_info) }
        it { expect(poll_info["questions"]).to be_a(Array) }
      end
    end

    context 'stat_for_completed_panelists_count' do
      context 'no quick_poll_stat' do
        it { expect(subject.stat_for_completed_panelists_count).to eq(0) }
      end

      context 'with quick_poll_stat' do
        let!(:quick_poll_stat) do
          subject.quick_poll_stat.update_attributes(completed_panelists_count: 20)
          subject.quick_poll_stat
        end

        it { expect(subject.stat_for_completed_panelists_count).to eq(20) }
      end
    end

    context 'stat_for_questback_group_panelists_count' do
      context 'no quick_poll_stat' do
        it { expect(subject.stat_for_questback_group_panelists_count).to eq(0) }
      end

      context 'with quick_poll_stat' do
        let!(:quick_poll_stat) do
          subject.quick_poll_stat.update_attributes(questback_group_panelists_count: 20)
          subject.quick_poll_stat
        end

        it { expect(subject.stat_for_questback_group_panelists_count).to eq(20) }
      end
    end

  end

end
