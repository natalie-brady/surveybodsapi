require 'rails_helper'

RSpec.describe PushNotificationLog, type: :model do

  context 'associations' do
    it { should belong_to(:user) }
  end

  context 'serialize' do
    it { should serialize(:sample_ids) }
  end
end
