require 'rails_helper'

RSpec.describe QuickPollResponse, type: :model do

  context 'associations' do
    it { should belong_to(:quick_poll_choice) }
    it { should belong_to(:panelist) }
  end

  context 'delegations' do
    it { should delegate_method(:quick_poll_question).to(:quick_poll_choice) }
    it { should delegate_method(:quick_poll).to(:quick_poll_choice) }
  end

  context 'validations' do
    it { should validate_presence_of(:panelist_id) }
    it { should validate_presence_of(:quick_poll_choice_id) }
    it { should validate_uniqueness_of(:quick_poll_choice_id).scoped_to(:panelist_id) }
    it do
      should validate_length_of(:quick_poll_choice_text)
        .is_at_most(140)
        .with_long_message("140 characters is the maximum allowed")
    end
  end

end
