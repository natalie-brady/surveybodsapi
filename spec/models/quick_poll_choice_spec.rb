require 'rails_helper'

RSpec.describe QuickPollChoice, type: :model do

  context 'associations' do
    it { should belong_to(:quick_poll_question) }
    it { should have_many(:quick_poll_responses) }
  end

  context 'delegations' do
    it { should delegate_method(:quick_poll).to(:quick_poll_question) }
  end

  context 'enums' do
    it { should define_enum_for(:answer_type)}
  end

end
