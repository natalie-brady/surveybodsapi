require 'rails_helper'

RSpec.describe Panelist, type: :model do

  let!(:panelist) { create(:panelist, questback_auth_code: false) }

  subject{ panelist }

  context 'associations' do
    it { should have_secure_password }
    it { should have_many(:push_notification_credentials) }
    it { should have_many(:quick_poll_choices).through(:quick_poll_responses) }
    it { should have_many(:quick_poll_responses) }
    it { should have_many(:geofence_logs) }
    it { should have_and_belong_to_many(:samples) }
    it { should have_many(:questback_group_users).class_name("QuestbackGroupUser") }
    it { should have_many(:questback_groups).through(:questback_group_users) }
    it { should have_many(:geofence_groups).through(:questback_groups) }
    it { should have_many(:geofences) }
    it { should have_many(:device_metadata) }
  end

  context 'methods' do
    context '#to_s' do
      it{ expect(subject.to_s).to eq(subject.email) }
    end
  end

end
