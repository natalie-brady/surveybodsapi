require 'rails_helper'

RSpec.describe Sample, type: :model do

  context 'associations' do
    it { should belong_to(:survey) }
    it { should have_and_belong_to_many(:panelists) }
  end

end
