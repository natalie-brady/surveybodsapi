require 'rails_helper'

RSpec.describe VpnStudyCredential, type: :model do

  context 'associations' do
    it { should belong_to(:panelist) }
    it { should belong_to(:vpn_study) }
    it { should have_one(:usage_study_user).with_foreign_key(:username).with_primary_key(:username) }
  end

  context 'enums' do
    it { should define_enum_for(:status) }
  end

end
