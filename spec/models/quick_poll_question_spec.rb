require 'rails_helper'

RSpec.describe QuickPollQuestion, type: :model do

  let!(:quick_poll_question) { create(:quick_poll_question) }
  let(:mapped_question_types) { QuickPollQuestion::QUESTION_TYPES.map{ |question_type| [question_type.titleize, question_type] } }

  subject{ quick_poll_question }

  context 'associations' do
    it { should belong_to(:quick_poll) }
    it { should have_many(:quick_poll_choices) }
    it { should have_many(:quick_poll_responses) }
  end

  context 'validations' do
    it { should validate_inclusion_of(:question_type).in_array(QuickPollQuestion::QUESTION_TYPES) }
    it { should accept_nested_attributes_for(:quick_poll_choices) }
  end

  context "methods" do
    it { expect(QuickPollQuestion.single).to eql("single") }
    it { expect(QuickPollQuestion.multiple).to eql("multiple") }
    it { expect(subject.single?).to be_in([true, false]) }
    it { expect(subject.multiple?).to be_in([true, false]) }
    it { expect(QuickPollQuestion.question_types).to eql(mapped_question_types) }
  end

end
