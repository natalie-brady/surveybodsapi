require 'rails_helper'

RSpec.describe GeofenceGroup, type: :model do

  let!(:geofence_group) { create(:geofence_group) }

  subject { geofence_group }

  context 'associations' do
    it { should belong_to(:survey) }
    it { should belong_to(:quick_poll) }
    it { should belong_to(:questback_group) }
    it { should belong_to(:geofence_collection) }
    it { should have_many(:geofences) }
  end

  context 'enum' do
    it { should define_enum_for(:geofence_type) }
    it { should define_enum_for(:action) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:message) }
    it { should validate_presence_of(:description) }

    context "if dwell" do
      before { allow(subject).to receive(:dwell?).and_return(true) }
      it { should validate_presence_of(:dwell_time) }
    end

    context "if not dwell" do
      before { allow(subject).to receive(:dwell?).and_return(false) }
      it { should_not validate_presence_of(:dwell_time) }
    end
  end

  context 'methods' do
    context '#has_no_geofences?' do
      it { expect(subject.has_no_geofences?).to be_truthy }
    end
  end

end
