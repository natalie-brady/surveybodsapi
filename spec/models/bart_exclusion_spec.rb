require 'rails_helper'

RSpec.describe BartExclusion, type: :model do
  it { should belong_to(:bart_action) }
  it { should belong_to(:panelist) }
end
