require 'rails_helper'

RSpec.describe GeofenceLog, type: :model do

  let!(:geofence_log) { create(:geofence_log) }

  subject { geofence_log }

  context 'associations' do
    it { should belong_to(:panelist) }
    it { should belong_to(:geofence) }
    it { should have_one(:geofence_group).through(:geofence) }
  end

  context 'methods' do
    context 'action' do
      it '' do
        geofence_group = subject.geofence_group
        expect(subject.action).to eq(geofence_group.action)
      end
    end
  end
end
