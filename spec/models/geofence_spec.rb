require 'rails_helper'

RSpec.describe Geofence, type: :model do
  let!(:geofence) { create(:geofence) }
  subject{ geofence }

  context 'association' do
    it { should belong_to(:geofence_group) }
    it { should have_many(:geofence_logs) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:latitude) }
    it { should validate_presence_of(:longitude) }
    it { should validate_presence_of(:radius) }
  end

  context 'method' do
    context 'to_s' do
      it { expect(subject.to_s).to eq(subject.name) }
    end
  end
end
