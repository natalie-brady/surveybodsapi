require 'rails_helper'

RSpec.describe UsageStudyAccessLog, type: :model do

  let!(:usage_study_access_log) { create(:usage_study_access_log) }

  context 'association' do
    it { should belong_to(:usage_study_user).with_primary_key(:ip_client).with_foreign_key(:ip_client) }
  end

  context 'methods' do

    context '#formatted_time_since_epoch' do
      it { expect(usage_study_access_log.formatted_time_since_epoch).to eq(Time.zone.at(usage_study_access_log.time_since_epoch).to_datetime.strftime('%F %T')) }
    end

    context '#panelist_email' do
      it { expect(usage_study_access_log.panelist_email).to eq(usage_study_access_log.usage_study_user.panelist) }
    end

    context '#csv_values' do
      it { expect(usage_study_access_log.csv_values).to include(usage_study_access_log.formatted_time_since_epoch,
                                                                usage_study_access_log.time_response,
                                                                usage_study_access_log.ip_client,
                                                                usage_study_access_log.ip_server,
                                                                usage_study_access_log.http_status_code,
                                                                usage_study_access_log.http_reply_size,
                                                                usage_study_access_log.http_method,
                                                                usage_study_access_log.http_url,
                                                                usage_study_access_log.http_username,
                                                                usage_study_access_log.http_mime_type,
                                                                usage_study_access_log.squid_request_status,
                                                                usage_study_access_log.squid_hier_status,
                                                                usage_study_access_log.http_useragent,
                                                                usage_study_access_log.panelist_email) }
    end
  end
end
