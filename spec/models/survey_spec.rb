require 'rails_helper'

RSpec.describe Survey, type: :model do

  context 'associations' do
    it { should belong_to(:survey_category) }
    it { should have_many(:samples) }
  end

  context 'scopes' do
    context 'with_duration' do
      it 'excludes surveys that has no duration' do
        @survey = create(:survey, duration: nil)
        expect(Survey.with_duration).to_not include(@survey)
      end

      it 'includes surveys that has duration' do
        @survey = create(:survey)
        expect(Survey.with_duration).to include(@survey)
      end
    end

    context 'without_duration' do
      it 'excludes surveys that has duration' do
        @survey = create(:survey)
        expect(Survey.without_duration).to_not include(@survey)
      end

      it 'includes surveys that has no duration' do
        @survey = create(:survey, duration: nil)
        expect(Survey.without_duration).to include(@survey)
      end
    end
  end

end
