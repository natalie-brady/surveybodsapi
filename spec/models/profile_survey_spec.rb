require 'rails_helper'

RSpec.describe ProfileSurvey, type: :model do

  context 'associations' do
    it { should belong_to(:survey) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:survey_id) }
  end

end
