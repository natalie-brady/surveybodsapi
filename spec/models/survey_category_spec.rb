require 'rails_helper'

RSpec.describe SurveyCategory, type: :model do

  context 'associations' do
    it { should have_many(:surveys) }
    it { should have_one(:setting) }
  end

end
