require 'rails_helper'

RSpec.describe BartAction, type: :model do
  it { should belong_to(:bart_setting) }
  it { should have_many(:bart_exclusions) }
end
