require 'rails_helper'

RSpec.describe BartSetting, type: :model do

  context 'association' do
    it { should belong_to(:vpn_study) }
    it { should have_many(:bart_actions) }
  end
end
