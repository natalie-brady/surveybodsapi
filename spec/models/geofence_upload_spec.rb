require 'rails_helper'

RSpec.describe GeofenceUpload, type: :model do
  let!(:geofence_upload) { create(:geofence_upload) }
  subject { geofence_upload }

  context 'associations' do
    it { should belong_to(:geofence_collection) }
  end

  context 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:latitude) }
    it { should validate_presence_of(:longitude) }
    it { should validate_uniqueness_of(:name).scoped_to(:geofence_collection_id) }
    it { should validate_uniqueness_of(:latitude).scoped_to(:geofence_collection_id) }
    it { should validate_uniqueness_of(:longitude).scoped_to(:geofence_collection_id) }
  end
end
