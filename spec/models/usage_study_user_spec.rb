require 'rails_helper'

RSpec.describe UsageStudyUser, type: :model do

  let!(:usage_study_user) { create(:usage_study_user) }

  context 'associations' do
    it { should belong_to(:vpn_study_credential).with_primary_key(:username).with_foreign_key(:username) }
    it { should have_many(:usage_study_access_logs).with_primary_key(:ip_client).with_foreign_key(:ip_client) }
    it { should have_many(:vpn_bandwidth_usages).with_primary_key(:ip_client).with_foreign_key(:ip_client) }
    it { should have_one(:panelist).through(:vpn_study_credential)}
    it { should have_one(:vpn_study).through(:vpn_study_credential)}
  end

  context 'methods' do
    context '#csv_values' do
      it { expect(usage_study_user.csv_values).to include(usage_study_user.ip_client,
                                                          usage_study_user.username,
                                                          usage_study_user.active,
                                                          usage_study_user.panelist) }
    end
  end
end
