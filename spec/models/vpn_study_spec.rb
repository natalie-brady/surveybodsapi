require 'rails_helper'

RSpec.describe VpnStudy, type: :model do

  let!(:vpn_study)            { create(:vpn_study) }
  let!(:except_attributes)    { %w(active created_at updated_at) }
  let!(:allowed_attributes)   { %w(id questback_group_id name description end_time bonus_points tickets last_start_time max_duration) }
  let!(:vpn_study_credential) { create(:vpn_study_credential) }
  let!(:vpn_studies) do
    {
      'id'                  => vpn_study.id,
      'questback_group_id'  => vpn_study.questback_group_id,
      'name'                => vpn_study.name,
      'description'         => vpn_study.description,
      'last_start_time'     => vpn_study.last_start_time.to_i,
      'end_time'            => vpn_study.end_time.to_i,
      'bonus_points'        => vpn_study.bonus_points.to_i,
      'tickets'             => vpn_study.tickets.to_i,
      'max_duration'        => vpn_study.max_duration
    }

  end

  context 'associations' do
    it { should belong_to(:questback_group) }
    it { should have_many(:panelists).through(:questback_group) }
    it { should have_many(:vpn_study_credentials) }
    it { should have_one(:bart_setting) }
  end

  context 'validations' do
    it { should validate_presence_of(:questback_group_id) }
  end

  context 'nested_attributes' do
    it { should accept_nested_attributes_for :bart_setting }
  end

  context 'methods' do

    context 'usage_study_users' do
      let(:usage_study_user) { create(:usage_study_user) }
      before do
        vpn_study_credential.usage_study_user = usage_study_user
        vpn_study.vpn_study_credentials << vpn_study_credential
      end

      it { expect(vpn_study.usage_study_users).to include(usage_study_user) }
    end

    context 'usage_study_access_logs' do
    end

    context 'deactivate!' do
      before do
        vpn_study.deactivate!
      end
      it { expect(vpn_study.active).to eq(false) }
    end

    context 'activate!' do
      before do
        vpn_study.activate!
      end
      it { expect(vpn_study.active).to eq(true) }
    end

    context '#to_s' do
      it { expect(vpn_study.to_s).to eq(vpn_study.name) }
    end

    context '#attributes_with_values' do
      it { expect(vpn_study.attributes_with_values).to eq(vpn_studies) }
    end

    context '#except_attributes' do
      it { expect(vpn_study.send(:except_attributes)).to eq(except_attributes) }
    end

    context 'allowed_attributes' do
      it { expect(vpn_study.send(:allowed_attributes)).to eq(allowed_attributes) }
    end
  end

end
