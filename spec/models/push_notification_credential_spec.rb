require 'rails_helper'

RSpec.describe PushNotificationCredential, type: :model do

  context 'association' do
    it { should belong_to(:panelist) }
  end

  context 'enum' do
    it { should define_enum_for(:notification_service) }
  end

  context 'validations' do
    it { should validate_uniqueness_of(:token).scoped_to(:panelist_id) }
  end

  context 'scopes' do
    context 'apple devices' do
      it "includes push notification credential" do
        @push_notification_credential = create(:push_notification_credential, notification_service: 0)
        expect(PushNotificationCredential.apple_devices).to include(@push_notification_credential)
      end

      it "excludes push notification credential" do
        @push_notification_credential = create(:push_notification_credential, notification_service: 1)
        expect(PushNotificationCredential.apple_devices).to_not include(@push_notification_credential)
      end
    end

    context 'android devices' do
      it "includes push notification credential" do
        @push_notification_credential = create(:push_notification_credential, notification_service: 1)
        expect(PushNotificationCredential.android_devices).to include(@push_notification_credential)
      end

      it "excludes push notification credential" do
        @push_notification_credential = create(:push_notification_credential, notification_service: 0)
        expect(PushNotificationCredential.android_devices).to_not include(@push_notification_credential)
      end
    end
  end

end
