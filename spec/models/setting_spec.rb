require 'rails_helper'

RSpec.describe Setting, type: :model do

  context 'associations' do
    it { should belong_to(:mail_template) }
    it { should belong_to(:survey_category) }
  end

end
