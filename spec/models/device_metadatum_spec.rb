require 'rails_helper'

RSpec.describe DeviceMetadatum, type: :model do
  let!(:device_metadatum) { create(:device_metadatum) }

  context 'association' do
    it { should belong_to(:panelist) }
  end
end
