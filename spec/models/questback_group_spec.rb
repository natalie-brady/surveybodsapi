require 'rails_helper'

RSpec.describe QuestbackGroup, type: :model do

  let!(:questback_group) { create(:questback_group) }

  subject{ questback_group }

  context 'associations' do
    it { should have_many(:questback_group_users) }
    it { should have_many(:panelists).through(:questback_group_users) }
    it { should have_many(:quick_polls) }
    it { should have_many(:geofence_groups) }
  end

  context 'to_s' do
    it { expect(subject.to_s).to eql(subject.name) }
  end

end
