FactoryGirl.define do
  factory :geofence_log do
    panelist_id   { create(:panelist, questback_auth_code: false).id }
    geofence_id   { create(:geofence).id }
    logged_time   { Time.now.to_i.to_s }
  end
end
