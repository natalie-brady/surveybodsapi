FactoryGirl.define do
  factory :survey_category do
    name        { FFaker::Name.name }
    description { FFaker::Lorem.sentence }
    color       "#FFFFF"
  end
end
