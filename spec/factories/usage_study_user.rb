FactoryGirl.define do
  factory :usage_study_user do
    ip_client   '100.100.100.10'
    username    FFaker::Lorem.word
    active      true
    phone_type  1
  end
end

