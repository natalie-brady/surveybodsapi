FactoryGirl.define do
  factory :push_notification_credential do
    token                 { FFaker::Guid.guid }
    notification_service  { rand(2) }
    panelist_id           { create(:panelist, questback_auth_code: false).id }
  end
end
