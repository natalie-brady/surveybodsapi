FactoryGirl.define do
  factory :bart_action do
    bart_setting_id   { build(:bart_setting, process_bart_action: false, enqueue_bart_setting: false).id }
    search_term       FFaker::Lorem.word
    survey_url        FFaker::Lorem.word
    survey_parameters FFaker::Lorem.word
  end
end
