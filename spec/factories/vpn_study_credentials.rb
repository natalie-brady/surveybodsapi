FactoryGirl.define do
  factory :vpn_study_credential do
    panelist_id   { create(:panelist, questback_auth_code: false).id }
    status        1
    vpn_study_id  { create(:vpn_study).id }
    username      FFaker::Lorem.word
  end
end
