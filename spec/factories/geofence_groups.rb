FactoryGirl.define do
  factory :geofence_group do
    name          { FFaker::Name.name }
    description   { FFaker::Lorem.sentence }
    questback_group_id 12345
    geofence_type { ['entry', 'exit'].sample }
    dwell_time    nil
    message       { FFaker::Lorem.paragraph }
    action        { ["survey", "quick_poll"].sample }
    survey_id     { create(:survey).id }
    quick_poll_id { create(:quick_poll).id }
    survey_title  { FFaker::Name.name }
  end
end
