FactoryGirl.define do
  factory :bart_setting do
    vpn_study_id            { build(:vpn_study).id }
    start_activation_period Time.now
    end_activation_period   Time.now + 2.months
    frequency_checker       120
    exclusion_setting       240
    exclude_all             false

    transient do
      process_bart_action true
      enqueue_bart_setting true
    end

    after(:build) do |bart_setting, eval|
      unless eval.process_bart_action
        bart_setting.class.skip_callback(:save, :after, :process_bart_action)
      end
    end

    after(:build) do |bart_setting, eval|
      unless eval.enqueue_bart_setting
        bart_setting.class.skip_callback(:save, :after, :enqueue_bart_setting)
      end
    end
  end
end
