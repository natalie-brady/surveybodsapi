FactoryGirl.define do
  factory :geofence do
    name              { FFaker::Name.name }
    description       { FFaker::Lorem.sentence }
    latitude          { 51.0000012312 }
    longitude         { -123.12321312 }
    radius            50
    delay             5
    geofence_group_id { create(:geofence_group).id }
  end
end
