FactoryGirl.define do
  factory :quick_poll do
    title               { FFaker::Lorem.sentence }
    description         { FFaker::Lorem.paragraph }
    duration            { FFaker::Lorem.sentence }
    bonus_points        120
    quick_poll_type     { ['activity', 'geofence'].sample }
    questback_group_id  { build(:questback_group).id }
  end
end
