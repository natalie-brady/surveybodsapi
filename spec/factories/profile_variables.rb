FactoryGirl.define do
  factory :profile_variable do
    name        { FFaker::Name.name }
    description { FFaker::Lorem.sentence }
    value       { (FFaker::Time.date).to_s }
  end
end
