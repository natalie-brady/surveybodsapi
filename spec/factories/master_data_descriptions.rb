FactoryGirl.define do
  factory :master_data_description do
    varname     { FFaker::Lorem.word }
    label       { FFaker::Lorem.sentence }
    datatype    'int'
    categories  { {-77 => "-77", 1 => "1", 0 => "0"} }
  end
end
