FactoryGirl.define do
  factory :vpn_study do
    questback_group_id  { create(:questback_group).id }
    name                FFaker::Lorem.sentence
    description         FFaker::Lorem.paragraph
    last_start_time     FFaker::Time.date
    end_time            FFaker::Time.date
    bonus_points        "100"
    tickets             "1"
    active              true
    max_duration        0
  end
end

