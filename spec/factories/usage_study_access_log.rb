FactoryGirl.define do
  factory :usage_study_access_log do
    time_since_epoch      Time.zone.now.to_i
    time_response         683
    ip_client             "100.100.100.10"
    ip_server             "52.30.10.230"
    http_status_code      "200"
    http_reply_size       7466
    http_method           "CONNECT"
    http_url              "sb.researchbods-clients.co.uk:443"
    http_username         "-"
    http_mime_type        "-"
    squid_request_status  "TCP_TUNNEL"
    squid_hier_status     "HIER_DIRECT"
    http_useragent        "-"
  end
end

