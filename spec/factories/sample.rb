FactoryGirl.define do
  factory :sample do
    title     { FFaker::Lorem.word }
    sample_id 1
    survey_id { build(:survey).id }
  end
end
