FactoryGirl.define do
  factory :survey do
    survey_id           { rand(90000) + 1000 }
    title               { FFaker::Lorem.sentence }
    start_time          { FFaker::Time.date }
    end_time            { FFaker::Time.date }
    bonus_points        120
    duration            '2 minutes'
    active              { [true, false].sample }
    questback_status    { FFaker::Lorem.word }
    survey_category_id  { create(:survey_category).id}
  end
end
