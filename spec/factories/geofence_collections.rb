FactoryGirl.define do
  factory :geofence_collection do
    name { FFaker::Name.name }
    description { FFaker::Lorem.sentence }
  end
end
