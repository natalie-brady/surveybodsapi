FactoryGirl.define do
  factory :quick_poll_stat do
    completed_panelists_count       10
    questback_group_panelists_count 100
    responded_to                    true
  end
end
