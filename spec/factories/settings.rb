FactoryGirl.define do
  factory :setting do
    qb_hostname                   { FFaker::Name.name }
    qb_api_username               { FFaker::Name.name }
    qb_api_password               { FFaker::Name.name }
    auth_timeout_seconds          36000
    site_id                       567
    mail_template_id              456
    apn_cert                      "34542"
    apn_passphrase                "Surveyb0ds"
    survey_category_id            123
    questback_external_service_id 012
    gcm_key                       { FFaker::Lorem.sentence}
    android_minimum_version       1
    ios_minimum_version           1
    refer_promotion_id            "123123"
  end
end
