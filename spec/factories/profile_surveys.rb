FactoryGirl.define do
  factory :profile_survey do
    name      { FFaker::Name.name}
    survey_id { create(:survey).id }
  end
end
