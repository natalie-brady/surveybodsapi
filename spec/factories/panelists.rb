FactoryGirl.define do
  factory :panelist do
    access_token                { FFaker::Guid.guid }
    email                       { FFaker::Internet.email }
    last_accessed               { FFaker::Time.date }
    uid                         { rand(900000) + 10000 }
    activated                   { [true, false].sample }
    external_auth_code          { FFaker::Guid.guid }
    external_auth_panelist_code { FFaker::Guid.guid }

    transient do
      questback_auth_code true
    end

    after(:build) do |panelist, eval|
      unless eval.questback_auth_code
        panelist.class.skip_callback(:create, :after, :generate_external_auth_code)
      end
    end
  end
end
