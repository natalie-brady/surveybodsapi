FactoryGirl.define do
  factory :questback_group do
    group_id    { rand(90000) + 10000 }
    name        { FFaker::Name.name }
    description { FFaker::Lorem.sentence }
  end
end
