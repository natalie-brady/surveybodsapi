FactoryGirl.define do
  factory :faq do
    question  { FFaker::Lorem.word }
    answer    { FFaker::Lorem.sentence }
  end
end
