FactoryGirl.define do
  factory :bart_exclusion do
    bart_action_id  { build(:bart_action).id }
    panelist_id     { build(:panelist, generate_external_auth_code: false) }
  end
end
