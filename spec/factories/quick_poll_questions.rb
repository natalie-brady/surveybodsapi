FactoryGirl.define do
  factory :quick_poll_question do
    question_type   { ['single', 'multiple'].sample }
    question        { FFaker::Lorem.sentence }
    randomize_order { [true, false].sample }
    quick_poll_id   { build(:quick_poll).id }
  end
end
