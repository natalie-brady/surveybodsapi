FactoryGirl.define do
  factory :geofence_upload do
    name { FFaker::Name.name }
    latitude 50.10003123
    longitude -2.123123
    radius 50
    geofence_collection_id { create(:geofence_collection).id }
  end
end
