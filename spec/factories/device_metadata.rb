FactoryGirl.define do
  factory :device_metadatum do
    panelist_id           { create(:panelist, questback_auth_code: false).id }
    network_provider      { FFaker::Lorem.word }
    device_model          { FFaker::Lorem.word }
    device_manufacturer   { FFaker::Lorem.word }
  end
end
