#!/bin/bash

if [ $# -ne 3 ]
then
  echo "Error!"
  echo "Usage: $0 <lat> <lon> <limit>"
  exit 1
fi

source set_server.sh
echo "Server: ${SB_SERVER}"

TOKEN=`cat access_token`
echo "TOKEN: ${TOKEN}"

START_TIME=`date +%s`
RESPONSE=`curl -s -H "Content-Type: application/json" -H "Token: $TOKEN" -d "{\"geofence\":{\"latitude\":\"$1\", \"longitude\":\"$2\", \"limit\":\"$3\"}}" ${SB_SERVER}/api/geofences`
END_TIME=`date +%s`

echo -e "Response:"
echo $RESPONSE
echo $RESPONSE | python -m json.tool
echo

echo "Response Time (ms):"
echo "($END_TIME - $START_TIME) / 1000000" | bc

echo $TOKEN > access_token


