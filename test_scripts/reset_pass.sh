#!/bin/bash

if [ $# -ne 1 ]
then
  echo "Error!"
  echo "Usage: $0 <email>"
  exit 1
fi

source set_server.sh
echo "Server: ${SB_SERVER}"

START_TIME=`date +%s`
RESPONSE=`curl -s -H "Content-Type: application/json" -d "{\"auth\":{\"email\":\"$1\"}}" ${SB_SERVER}/api/auth/password_reset`
END_TIME=`date +%s`

echo -e "Response:"
echo $RESPONSE
echo $RESPONSE | python -m json.tool
echo

echo "Response Time (ms):"
echo "($END_TIME - $START_TIME) / 1000000" | bc
