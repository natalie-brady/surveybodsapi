#!/bin/bash

source set_server.sh
echo "Server: ${SB_SERVER}"

TOKEN=`cat access_token`

echo $TOKEN
echo ${SB_SERVER}
START_TIME=`date +%s`
RESPONSE=`curl -s -H "Content-Type: application/json" -H "Token: $TOKEN" ${SB_SERVER}/api/quick_polls`
END_TIME=`date +%s`

echo -e "Response:"
echo $RESPONSE
echo $RESPONSE | python -m json.tool
echo

echo "Response Time (ms):"
echo "($END_TIME - $START_TIME) / 1000000" | bc

