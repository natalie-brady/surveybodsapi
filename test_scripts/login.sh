#!/bin/bash

if [ $# -ne 2 ]
then
  echo "Error!"
  echo "Usage: $0 <username> <password>"
  exit 1
fi

source set_server.sh
echo "Server: ${SB_SERVER}"

START_TIME=`date +%s`
RESPONSE=`curl -s -H "Content-Type: application/json" -d "{\"auth\":{\"email\":\"$1\", \"password\":\"$2\"}}" ${SB_SERVER}/api/auth/login`
END_TIME=`date +%s`

echo -e "Response:"
echo $RESPONSE
echo $RESPONSE | python -m json.tool
echo


TOKEN=`echo $RESPONSE \
| grep -oE "access_token\":\".*\"" \
| grep -oE ":\".*\"" \
| grep -oE "[a-f0-9-]+"`

echo -e "AccessToken:\n$TOKEN\n"

echo "Response Time (ms):"
echo "($END_TIME - $START_TIME) / 1000000" | bc

echo $TOKEN > access_token
