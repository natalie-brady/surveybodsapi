#!/bin/bash

# if [ $# -ne 4 ]
if [ $# -ne 3 ]
then
  echo "Error!"
  # echo "Usage: $0 <quick_poll_id> <question_id> <answer_id> <answer>"
  echo "Usage: $0 <quick_poll_id> <question_id> <answer_id>"
  exit 1
fi

source set_server.sh
echo "Server: ${SB_SERVER}"

TOKEN=`cat access_token`

START_TIME=`date +%s`
# RESPONSE=`curl -s -H "Content-Type: application/json" -H "Token: $TOKEN" -d "{\"responses\":[{\"id\":$1, \"questions\":[{\"id\":$2, \"answers\":[{\"id\":$3, \"text\":\"$4\"}]}]}]}" ${SB_SERVER}/api/quick_polls`
RESPONSE=`curl -s -H "Content-Type: application/json" -H "Token: $TOKEN" -d "{\"responses\":[{\"id\":$1, \"questions\":[{\"id\":$2, \"answers\":[{\"id\":$3}]}]}]}" ${SB_SERVER}/api/quick_polls`
END_TIME=`date +%s`

echo -e "Response:"
echo $RESPONSE
echo $RESPONSE | python -m json.tool
echo

echo "Response Time (ms):"
echo "($END_TIME - $START_TIME) / 1000000" | bc

