# encoding: utf-8

##
# crontab
# * * * * * /bin/bash -l -c 'backup perform --trigger surveybods_backup'

# $ backup perform -t surveybods_backup [-c <path_to_configuration_file>]
#
# For more information about Backup's components, see the documentation at:
# http://meskyanichi.github.io/backup
#
Model.new(:surveybods_backup, 'SurveyBods Production Daily Backup') do

  database MySQL do |db|
    db.name     = "surveybods_production"
    db.username = "root"
    db.password = "7xaDzJtrEaY7Ycd8UVOs"
    db.host     = "localhost"
    db.port     = 3306
  end

  store_with S3 do |s3|
    s3.access_key_id     = "AKIAJYFIT4H3UMKPPGAA"
    s3.secret_access_key = "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"
    s3.bucket            = "surveybods-backup"
    s3.region            = "eu-west-1"
    s3.path              = "backups"
    s3.keep              = 7
  end

  store_with Local do |local|
    local.path       = "~/Backup/backups/"
    local.keep       = 5
  end

  compress_with Gzip

end
